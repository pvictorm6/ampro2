function ret = f2_vector(t,x,tp0,logObj,robotwalk)
ep      = robotwalk.ep;
a       = robotwalk.a_opt{robotwalk.dr,1};
p       = robotwalk.p;
nbasedof= robotwalk.nBaseDof;
ndof    = robotwalk.nDof;
nexdof  = nbasedof + ndof;
dx      = x(nexdof+1:end);

%%%Control Fields
M       = De_mat(x);
C       = Ce_mat(x);
G       = Ge_vec(x);
H       = C*dx + G;

Jsf     = J_sf(x);
Jdotsf  = Jdot_sf(x);
J       = Jsf;
Jdot    = Jdotsf;

Xi      = inv(J*inv(M)*J');
vf      = [dx; M \ ((J'*Xi*J*inv(M) - eye(nexdof))*H...
                -J'*Xi*Jdot*dx)];
B_IO    = [zeros(nbasedof, ndof); eye(ndof)];
gf      = [zeros(size(B_IO)); M \ (eye(nexdof) - ...
                J'*Xi*J*inv(M))*B_IO];

%%%Outputs
y_a1    = ya1_sca(x, p, a);
y_a2    = ya2_vec(x, p, a);
y_d1    = yd1_sca(x, p, a);
y_d2    = yd2_vec(x, p, a);

%%%Outputs
y1      = y_a1 - y_d1;
y2      = y_a2 - y_d2;
y_a     = [y_a1; y_a2];
y_d     = [y_d1; y_d2];

%%%Jacobians of Outputs
Dy_a1   = Dya1_mat(x, p, a);
Dy_d1   = Dyd1_mat(x, p, a);
Dy_a2   = Dya2_mat(x, p, a);
Dy_d2   = Dyd2_mat(x, p, a);
Dy_1    = Dy_a1 - Dy_d1;
Dy_2    = Dy_a2 - Dy_d2;

%%%Lie Derivatives
Lgy1    = Dy_1*gf;
Lgy2    = Dy_2*gf;
Lfy1    = Dy_1*vf;
Lfy2    = Dy_2*vf;

%%%Second Order Jacobians
DLfy_a2 = DLfya2_mat(x, p, a);
DLfy_d2 = DLfyd2_mat(x, p, a);
DLfy2   = DLfy_a2-DLfy_d2;

%%%Second Lie Derivatives
LfLfy2  = DLfy2*vf;
LgLfy2  = DLfy2*gf;

% Vector field
A   = [Lgy1; LgLfy2];
u   = -A \ ([0; LfLfy2]+[Lfy1; 2*ep*Lfy2]+ [ep*y1; ep^2*y2]);
ret = vf + gf * u;


%% log the data
tp  = t + tp0;
Pow = [0;0;0;u].*dx;
logObj.append(tp,tp0,x,robotwalk.dr,y_a,y_d,u,Pow);
end
