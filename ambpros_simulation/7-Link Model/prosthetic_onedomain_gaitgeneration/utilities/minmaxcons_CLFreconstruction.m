function [minsknee, minnsknee, maxtorso, maxfootroll, max_nsslope, velocityCond,...
    hcondmin_foot, hcondmin_toe, endZ, breakflag] = ...
    minmaxcons_CLFreconstruction(x_plus, tau, a, robotwalk, USERCHOICE)

    nexdof  = length(x_plus)/2;
    hmax    = USERCHOICE.MAX_FOOTHEIGHT;
    hmax_t  = USERCHOICE.MAX_TOEHEIGHT;
    p0      = deltaphip_sca(x_plus);
    
    N       = 10;
    deltat  = fix(tau*100)/(100*N);
    
    %% initialize the constriaints
    hcond_foot  = zeros(1, N);
    hcond_toe   = zeros(1, N);
    nsfoot_posx = zeros(1, N);
    nsfoot_posy = zeros(1, N);
    ya2current  = zeros(5, N);
    x_reconstruction = zeros(2*nexdof,N);    
                   
    x_current   = x_plus; 
    breakflag   = 0;
    
    %%
    for i = 1:N       
%         plot_gesture(x_current, 'r')
        [mu, Delta, exitflag] = min_norm_QP_lagrangeX(x_current, p0, a, robotwalk);
       
        if Delta > 100 || exitflag ~=1
            breakflag = 1;
            break;
        end
        
        %% compute the constraints            
        x_reconstruction(:,i) = x_current;
                     
        %%compute the outputs constraints
        ya2current(:,i) = ya2_vec(x_current, p0, a);
        
        %%compute the height and position constraint
        t               = deltat*(i-1);
        posf            = jpos_mat(x_current);
        toe_pos_y       = posf(2,end - 2);
        qheight         = quad_height(t, hmax, tau);
        hcond_foot(i)   = nsfoot_posy(i) - qheight;     
        
        qheight_T       = quad_height(t, hmax_t, tau);
        hcond_toe(i)    = toe_pos_y - qheight_T;  
        
        footpos         = footpos_mat(x_current);
        nsfoot_posx(i)  = footpos(1,end);
        nsfoot_posy(i)  = footpos(2,end); 
               
        %% get the next point
        %%%relative degree one output
        phip_current    = deltaphip_sca(x_current);
        vhip_current    = deltaphip_dot_mat(x_current)*x_current(nexdof+4:end);
        vhip_next       = vhip_current + deltat*mu(1);
        phip_next       = phip_current + deltat*vhip_current + deltat^2*mu(1)/2;
        
        %%%relatvie degree two outputs
        y2_current      = y2_vec(x_current, p0, a);
        y2dot_current   = Dy2_mat(x_current, p0, a)*[x_current(nexdof+1:end); zeros(nexdof, 1)];
        y2dot_next      = y2dot_current + deltat*mu(2:6);
        y2_next         = y2_current + deltat*y2dot_current + deltat^2*mu(2:6)/2;
        
        %%compute the next step desired outputs
        y2d_next        = ynosubd2_vec(phip_next, p0, a);
        y2ddot_next     = Dynosubd2_vec(phip_next, p0, a)';
                
        y1a_next        = vhip_next;
        y2a_next        = y2_next + y2d_next;
        y2adot_next     = y2dot_next + y2ddot_next;
       
        q_next          = Phi_mat()\[phip_next; y2a_next];
%         dq_next         = Phi_mat()\[y1a_next; y2adot_next];
        dq_next         = H_minus([0;0;0;q_next], a)\[y1a_next; y2dot_next];
        x_next          = [x_current(1:3);q_next;x_current(10:12);dq_next];
        
        x_current       = x_next;      
    end
    
    maxfootroll         = max(abs(ya2current(end, :)));
    maxtorso            = max(abs(ya2current(end -1,:)));       
    minsknee            = min(ya2current(1, :));
    minnsknee           = min(ya2current(2, :));
    max_nsslope         = max(abs(ya2current(3, :)));
    velocityCond        = max(max(abs(x_reconstruction(nexdof+1:end, :))));
    
    hcondmin_foot       = min(hcond_foot);  
    hcondmin_toe        = min(hcond_toe);
    endZ                = h_sca(x_reconstruction(:,end));   
    
end

function h = quad_height(t,hmax,Ttau)

a = -4*hmax/(Ttau^2);
b = 4*hmax/Ttau;

h = a*t^2 + b*t;
end
