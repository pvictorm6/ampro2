function impedParams = dynamic_estimation(t, q, dq, u) 
%% get the discrete dynamics
startpoint = 1;
dq      = dq(startpoint:end);
q       = q(startpoint:end);
u       = u(startpoint:end);

%% fitting the data
X_chosen        = [q, dq];
Y_chosen        = u;
u_chosen        = u;

errorbound      = 1e-6;
options         = optimset('Algorithm','interior-point','Display','final',...
                        'TolX',errorbound,'MaxIter', 1000,...
                        'MaxFunEvals',10000,'TolCon',errorbound);
nparameters     = 3;
njoints         = 1;
% ip_guess        = zeros(nparameters*njoints, 1);
ip_guess        = [ -22.8484   -1.2886   -0.2907];
ip_max          = 60;
lbmat           = [-ip_max; -ip_max*0.005; 0];
ubmat           = [ip_max; ip_max*0.005; 3];
ub              = ubmat(:);
lb              = lbmat(:);  

ip_opt          = fmincon(@(u)cost(u, nparameters, X_chosen, Y_chosen),...
                           ip_guess, [], [], [], [], lb, ub, ...
                          @(u)mycon(u, X_chosen, Y_chosen),options);
impedParams     = reshape(ip_opt, njoints, nparameters);
%%plot the results
% impedParams = [-5, 0.6, 1];
plot_estimatedImpedParams(impedParams*1, t, X_chosen, Y_chosen, u_chosen);

%% functions for fmincon
function ret = cost(coef, nparameters, Xdata, Ydata) 
    njoints         = size(Ydata, 2);
    impedparams     = reshape(coef, njoints, nparameters);
    ret  = 0;
    for j = 1: njoints;
        ret = ret + sum(1/2*((func_impedance(impedparams(j, :),...
                [Xdata(:, j), Xdata(:, j+njoints)]) - Ydata(:, j)).^2));
    end
end

function [c, ceq] = mycon(u, Xdata, Ydata)
%     qe  = abs(u(1)/u(2));
%     initial_con = abs(func_impedance(u, Xdata(1,:)) - Ydata(1));
%     c   = [ qe - 3];
%     c       = [initial_con - 0.1];
    c       = [];
    ceq     = [];
end
end