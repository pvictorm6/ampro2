function ret = func_impedance(coef, xdata)
    q       = xdata(:, 1);
    dq      = xdata(:, 2);
    
    if length(coef) == 3
        %% impedance function 1
        k       = coef(1);
        b       = coef(2);
        qe      = coef(3);
        ret     = k.*(q - qe) + b.*dq;
    elseif length(coef) == 4
    %% impedance function 2
        k1      = coef(1);
        k2      = coef(2);
        b       = coef(3);
        qe      = coef(4);
        ret     = k1.*(q - qe) + k2.*(q - qe).^3 + b.*dq;
    else
    %% impedance function 3 with torque shift
        k1      = coef(1);
        k2      = coef(2);
        b       = coef(3);
        qe      = coef(4);
        tor     = coef(5);
        ret     = k1.*(q - qe) + k2.*(q - qe).^3 + b.*dq + tor;        
    end
end