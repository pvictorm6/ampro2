function [a_t] = intermediatematrix(a_in,x_0,x_f,a_f,p)

ndof        = length(x_0)/2;
a_t         = zeros(size(a_f));
a_t(1,1)    = a_f(1,1);
a_t(:,[2 4 6]) = a_in;

%% Pick the parameterized time at the start and end of the intermediate step
tau_0   = (deltaphip_sca(x_0)-p)/a_f(1,1);
% tau_0=0;
tau_f   = (deltaphip_sca(x_f)-p)/a_f(1,1);

%% Computing hip velocities at the beginning and start of the intermediate step
xidot_0 = deltaphip_sca(x_0(ndof+1:end));
xidot_f = deltaphip_sca(x_f(ndof+1:end));

%% Compute the outputs from the states
ya_0    = ya2_vec(x_0);
dya_0   = Dya2_mat(x_0)*[x_0(ndof+1:end);zeros(ndof,1)]/xidot_0;
ya_f    = ya2_vec(x_f,p,a_f);
dya_f   = Dya2_mat(x_f,p,a_f)*[x_f(ndof+1:end);zeros(ndof,1)]/xidot_f;

%% Compute the parameters
for i=1:ndof-1

    % The canonical walking function can be written in the form A*v, where
    % A=LTM is the linear transformation matrix and v is the vector of
    % parameters [alpha_1 alpha_3 alpha_5 alpha_7]
     LTM_0=computeLTV(a_in(i+1,:),tau_0,a_f);
     LTM_f=computeLTV(a_in(i+1,:),tau_f,a_f);
     TMAT =[LTM_0;LTM_f];
     
     a_t(i+1,[1 3 5 7])=(TMAT\[ya_0(i);dya_0(i);ya_f(i);dya_f(i)])';
end
end

%% This computes the Linear tranformation vector which eventually is used to get the Linear Transformation Matrix
function ret=computeLTV(a_row,tau,a_f)
    ret=[exp(-a_row(2)*tau)*cos(a_row(1)*tau) ...
            exp(-a_row(2)*tau)*sin(a_row(1)*tau) ...
                    cos(a_row(3)*tau)+2*a_row(2)*a_row(3)*sin(a_row(3)*tau)/(a_row(1)^2+a_row(2)^2-a_row(3)^2) ...
                                                    1;
         (-a_row(2)*exp(-a_row(2)*tau)*cos(a_row(1)*tau)-a_row(1)*exp(-a_row(2)*tau)*sin(a_row(1)*tau))/a_f(1,1) ...
            (-a_row(2)*exp(-a_row(2)*tau)*sin(a_row(1)*tau)+a_row(1)*exp(-a_row(2)*tau)*cos(a_row(1)*tau))/a_f(1,1) ...
                (-a_row(3)*sin(a_row(3)*tau)+2*a_row(2)*(a_row(3))^2*cos(a_row(3)*tau)/(a_row(1)^2+a_row(2)^2-a_row(3)^2))/a_f(1,1) ...
                    0                                                                                                   ];
end
