iptables --table nat --append POSTROUTING --out-interface wlan0 -j MASQUERADE
iptables --append FORWARD --in-interface eth3 -j ACCEPT
iptables --append FORWARD --in-interface eth4 -j ACCEPT
echo 1 > /proc/sys/net/ipv4/ip_forward
