#ifndef NNETWORK
#define NNETWORK

//standard libraries
#include <math.h>
#include <ctime>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>

//custom includes
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Eigen>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <yaml_eigen_utilities/yaml_eigen_utilities.hpp>
#include <roslib_utilities/ros_package_utilities.hpp>

using namespace std; 
class neuralNetwork
{
//private members
private:
	//number of neurons
	int nInput, nHidden, nOutput;
    string modelname;
	
	//neurons
	Eigen::VectorXd inputNeurons;
	Eigen::VectorXd hiddenNeurons;
	Eigen::VectorXd outputNeurons;

    Eigen::VectorXd xOffset;
    Eigen::VectorXd gain;
    Eigen::VectorXd ymin;

	Eigen::VectorXd bInputHidden;
	Eigen::VectorXd bHiddenOutput;

	//weights
	Eigen::MatrixXd wInputHidden;
	Eigen::MatrixXd wHiddenOutput;
//public methods
//----------------------------------------------------------------------------------------------------------------
public:
    double prediction;
	//constructor
    neuralNetwork(string name) : modelname(name)
    {
//        //initialize the parameters
//        initParameters();

        cout << "Loading the neural network model of " << modelname << endl;

        //load the learned model
        loadWeights();
	}

	//feed data through network
    int predict(Eigen::VectorXd inputs)
	{
		//feed data into the network
		feedForward(inputs);

        //the first outputNeuron is always the standing mode. For code simplify purpose, 0 is for the standing mode, 1 is for the flat ground walking mode, -1 is for the stair climb mode
        if(nOutput == 2){
            if(outputNeurons(0) > outputNeurons(1)){
                prediction = 0;
            }else
            {
                prediction = 1;
            }
        }

        if(nOutput == 3){
            if((outputNeurons(0) > outputNeurons(1)) && (outputNeurons(0) > outputNeurons(2))){
                prediction = 0;
            }
            if((outputNeurons(1) > outputNeurons(0)) && (outputNeurons(1) > outputNeurons(2)))
            {
                prediction = 1;
            }
            if((outputNeurons(2) > outputNeurons(1)) && (outputNeurons(2) > outputNeurons(1)))
            {
                prediction = -1;
            }
        }
		//return results
		return prediction;
	}

//private methods
//----------------------------------------------------------------------------------------------------------------
private:
    void initParameters()
    {
        inputNeurons = Eigen::VectorXd::Zero(nInput, 1);

        hiddenNeurons = Eigen::VectorXd::Zero(nHidden, 1);

        outputNeurons = Eigen::VectorXd::Zero(nOutput, 1);

        xOffset = Eigen::VectorXd::Zero(nInput, 1);

        gain = Eigen::VectorXd::Zero(nInput, 1);

        ymin = Eigen::VectorXd::Zero(nInput, 1);

        wInputHidden = Eigen::MatrixXd::Zero(nHidden, nInput);

        bInputHidden = Eigen::VectorXd::Zero(nHidden, 1);

        bHiddenOutput = Eigen::VectorXd::Zero(nOutput, 1);

        wHiddenOutput = Eigen::MatrixXd::Zero(nOutput, nHidden);
    }

    void preprocess(Eigen::VectorXd inputs)
    {
        //preprocess  input neurons
        inputNeurons = (inputs- xOffset).cwiseProduct(gain) + ymin;
    }

	void loadWeights()
	{
        string datapath_model = "package://ambpros_trajectory/share/nnmodel_";
        datapath_model.append(modelname);
        datapath_model.append(".yaml");

        string filepath = roslib_utilities::resolve_local_url(datapath_model).string();
        YAML::Node node;
        yaml_utilities::yaml_read_file(filepath, node);

        node["ninput"] >> nInput;
        node["noutput"] >> nOutput;
        node["nhidden"] >> nHidden;

        initParameters();

        node["wInputHidden"] >> wInputHidden;
        node["wHiddenOutput"] >> wHiddenOutput;
        node["bInputHidden"] >> bInputHidden;
        node["bHiddenOutput"] >> bHiddenOutput;

        node["xOffset"] >> xOffset;
        node["gain"] >> gain;
        node["ymin"] >> ymin;
	}

	//feed input forward
	void feedForward(Eigen::VectorXd inputs)
	{
        preprocess(inputs);
		//Calculate Hidden Layer values - include bias neuron
		//--------------------------------------------------------------------------------------------------------
		for(int j=0; j < nHidden; j++)
		{
			//clear value
			hiddenNeurons(j) = 0;	

			//get weighted sum of inputs and bias neuron
			hiddenNeurons(j) = wInputHidden.row(j)*inputNeurons	+ bInputHidden(j);
			
			//set to result of sigmoid
			hiddenNeurons(j) = activationFunction( hiddenNeurons(j));			
		}
		
		//Calculating Output Layer values - include bias neuron
		//--------------------------------------------------------------------------------------------------------
		for(int k=0; k < nOutput; k++)
		{
			//clear value
			outputNeurons(k) = 0;

			//get weighted sum of inputs and bias neuron
            outputNeurons(k) = wHiddenOutput.row(k)*hiddenNeurons + bHiddenOutput(k);			
//			//set to result of sigmoid
//            outputNeurons(k) = activationFunction( outputNeurons(k));
		}

        // this method is borrowed from matlab. The detailed reason is not understood yet.
        double nmax    = max(outputNeurons(0), outputNeurons(1));
        outputNeurons  << outputNeurons(0) - nmax,
                          outputNeurons(1) - nmax;
        Eigen::VectorXd numer(2);
        numer << exp(outputNeurons(0)),
                 exp(outputNeurons(1));
        double denom = numer(0) + numer(1);
        if (denom==0)
            denom = 1;
        outputNeurons << numer(0)/denom,
                         numer(1)/denom;
	}

	//activation function
	inline double activationFunction( double x )
	{
		//sigmoid function
		// return 1/(1+exp(-x));
		return 2/(1+exp(-2*x))-1;
	}
};

#endif
