function ret = invkin_7links_flatground(a)

    typeofsolve = 2;

    if typeofsolve == 1    
        %%%%%%%%%%%%%%%%%%%%% Numeric inverse kinematics
        q(2) = yd_ns_knee(0,a);
        q(5) = yd_s_knee(0,a);
        q(3) = yd_ns_slope(0,a) + yd_torso(0,a) - 0.39883*q(2);
        q(1) = - 2*yd_ns_slope(0,a) - yd_ns_foot(0,a) -yd_torso(0,a) ...
               - 0.20234*q(2) + q(3);

        qtemp = zeros(2,1);
        options = optimset('Display','off','Algorithm','Levenberg-Marquardt');
        q46   = fsolve(@myfun,qtemp,options);
        q(4)  = q46(1);
        q(6)  = q46(2);
        ret   = q;
    elseif typeofsolve == 2
            %%%%If using the old version (no torso) use this:
            q    = zeros(6,1);
            q(2) = theta_a2(q,a);
            q(5) = theta_a5(q,a);
            q(3) = theta_a3(q,a);
            q(1) = theta_a1(q,a);
            q(4) = theta_a4(q,a);
            q(6) = theta_a6(q,a);
            
            ret  = q;
    end
    
    function ret = myfun(qtemp)
        ret1 = qtemp(1) + q(5) + qtemp(2) - yd_torso(0,a);
           q = [q(1);q(2);q(3);qtemp(1);q(5);qtemp(2)];
        ret2 = h_sca(q);        
%           rq = R(q);    
%         ret3 = h_sca(rq);
        ret  = [ret1;
                ret2;
%                 ret3
                ];
    end
end