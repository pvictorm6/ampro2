addpath('./dataforfigure');
% load('onestep_data_IO')
load('data')

% Figure properties
fig_width  = 500;
fig_height = 400;

% Distance between markers
mrkr_dist = 0.3;

% Specify marker and line properties
marker_style = {'-o','-d','-s','-p','-d','-s'};
marker_color = {'blue','red',[32 153 41]/255,[80 30 20]/255,'black','m'};
marker_size  = 6;
line_width   = 2;

%%for IO
% x_min = -0.82; 
% x_max =  1.32;
% y_min = -4.0;
% y_max =  4.2;

%%for PD
x_min = -1.0; 
x_max =  1.3;
y_min = -4.0;
y_max =  4.2;

x_lab = {'Angle (rad)'};
y_lab = {'Angular Velocity (rads/s)'};

% Interpreter and fontsizes
font_interpreter = 'LaTeX';
font_size_axis   = 20;
font_size_title  = 24;
font_size_lgnd   = 24;

% Legend labels
legend_labels = {'$\theta_{sa}$','$\theta_{sk}$','$\theta_{sh}$',...
    '$\theta_{nsh}$','$\theta_{nsk}$','$\theta_{nsa}$'};
% % % %  END USER INTERFACE % % % %
% % % % % % % % % % % % % % % % % % 

% % % Parse data
% q = cat(2,data{:});
q       = data;

ndof    = (size(q,1)-1)/2;

% % % Populate plot vectors, based on marker spacing (mrkr_dist)
for i = 1:ndof
%     p_vec{i} = pickPoints(q(i+1,:),q(i+ndof+1,:),mrkr_dist);
    p_vec{i} = parsePoint(q(i+1,:),q(i+ndof+1,:),mrkr_dist);
end

% % % Limit cycles figure
lc_handle = figure();
set(lc_handle, 'Position', [50 50 fig_width fig_height]);

% % % Limit cycles plot
for i = 1:ndof
    lc_plot(i) = plot([p_vec{i}(1,end),p_vec{i}(1,:)],[p_vec{i}(2,end),p_vec{i}(2,:)],...
                      marker_style{i}, 'color', marker_color{i}, 'MarkerSize', marker_size,...
                      'LineWidth', line_width);
    hold on
end

% % % Axis formatting
xlabel(x_lab{1},'Interpreter',font_interpreter,'FontSize',font_size_axis)
ylabel(y_lab{1},'Interpreter',font_interpreter,'FontSize',font_size_axis)
axis([x_min x_max y_min y_max])

% % % Axis positioning
x_range = abs(x_min)+abs(x_max);
y_range = abs(y_min)+abs(y_max);
set(get(gca,'XLabel'),'Position',[(x_min+x_max)/2   y_min-0.1*y_range/((fig_height-128)/(400-128))]);
set(get(gca,'YLabel'),'Position',[x_min-0.05*x_range/((fig_width-100)/400) (y_min+y_max)/2]);

% % % Plot positioning
apc = [65/fig_width  96/fig_height  (fig_width-100)/fig_width  (fig_height-128)/fig_height];
set(gca,'position',apc);

% % % Legend formatting and positioning
axis_handle = gca;

for i = 1:3
    % Position of legend marker
    lgnd_handle = legend(axis_handle, lc_plot(i),'',1,'Location','SouthOutside','Orientation','Horizontal');
    set(lgnd_handle,'position', [(i-1)*0.3+0.08 -(fig_height/2-35)/fig_height 0.2 1],'Box','off');
    axis_handle = axes('position',get(gca,'position'), 'visible','off');
    
    % Legend text
    text('Interpreter',font_interpreter,...
         'String',legend_labels{i},...
         'Position',[(i-1)*fig_width/3/(fig_width-40)+(65*(fig_width)/500-15)/fig_width -70.72/(fig_height-48)],...
         'FontSize', font_size_lgnd)
end

for i = 4:ndof
    % Position of legend marker
    lgnd_handle = legend(axis_handle, lc_plot(i),'',1,'Location','SouthOutside','Orientation','Horizontal');
    set(lgnd_handle,'position', [(i-4)*0.3+0.08 -(fig_height/2-15)/fig_height 0.2 1],'Box','off');
    axis_handle = axes('position',get(gca,'position'), 'visible','off');
    
    % Legend text
    text('Interpreter',font_interpreter,...
         'String',legend_labels{i},...
         'Position',[(i-4)*fig_width/3/(fig_width-40)+(65*(fig_width)/500-15)/fig_width -70.72/(fig_height-158)],...
         'FontSize', font_size_lgnd)
end
    
