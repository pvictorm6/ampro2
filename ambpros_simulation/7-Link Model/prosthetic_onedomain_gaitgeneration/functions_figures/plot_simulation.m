function plot_simulation(logObj, robotwalk)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    data        = cat(2,robotwalk.data{:});
    T           = data(1,:);
    Q           = data(2:end,:);
    nbasedof    = robotwalk.nBaseDof;
    ndof        = robotwalk.nDof;
    noutputs    = robotwalk.nOutputs;
    
    %% intialize the figure
    color_array     = distinguishable_colors(ndof + nbasedof); 
    LW              = 1;
    angle_labels    = cell(1, noutputs);
    torque_labels   = cell(1, noutputs);
    power_labels    = cell(1, noutputs);
    yact_labels     = cell(1, noutputs);
    ydes_labels     = cell(1, noutputs);
    for i = 1:noutputs
        angle_labels{i}  = ['q_{' num2str(i) '}'];
        torque_labels{i} = ['T_{' num2str(i) '}'];
        power_labels{i}  = ['P_{' num2str(i) '}'];
        yact_labels{i}   = ['y' num2str(i),'_{act}'];
        ydes_labels{i}   = ['y' num2str(i),'_{des}'];
    end

    %% plot the phase portrait and angle trajectory
    figure
    subplot(2, 1, 1);
    for i = 1:ndof
        plot(T, Q(nbasedof+i,:),...
            'Color',color_array(i,:), 'LineWidth', LW);
        hold on
    end
    legend(angle_labels, 'Location', 'EastOutside');
    subplot(2, 1, 2);
    for i = 1:ndof
        plot(Q(nbasedof+i,:), Q(2*nbasedof+ndof+i,:),...
            'Color',color_array(i,:), 'LineWidth', LW);
        hold on
    end
    legend(angle_labels, 'Location', 'EastOutside');

    %% plot the torque and power
    figure
    subplot(2,1,1);
    for i = 1:noutputs
        plot(logObj.tp, logObj.u(i,:), 'Color', color_array(i,:), 'LineWidth', LW)
        hold on
    end
    legend(torque_labels{1:end}, 'Location', 'EastOutside');
    
    subplot(2,1,2);
    for i = 1:noutputs
        plot(logObj.tp, logObj.Pow(nbasedof + i, :), 'Color', color_array(i,:), 'LineWidth', LW)
        hold on
    end
    legend(power_labels{1:end}, 'Location', 'EastOutside');

    %% plot the yactual VS ydesire 
    hf       = NaN(1, length(T));
    ht       = NaN(1, length(T));
    for i = 1:length(T)        
        pos     = jpos_mat(Q(:, i));
%         hf(i)   = h_nsf(Q(:, i));
        hf(i)   = pos(2, end);
        ht(i)   = pos(2, end -2);
    end

    figure
    for i = 1:noutputs
        plot(logObj.tp, logObj.y_a(i,:), 'Color', color_array(i,:),...
            'LineStyle','-', 'LineWidth', LW);
        hold on
    end
    
    for i = 1:noutputs
        plot(logObj.tp, logObj.y_d(i,:), 'Color', color_array(i,:),...
            'LineStyle','--', 'LineWidth', LW);
        hold on
    end
    plot(T, hf, 'Color', color_array(end, :), 'LineWidth', LW)
    hold on
    plot(T, ht, 'Color', color_array(end - 1, :), 'LineWidth', LW)
    title('ya vs. yd')
    xlabel('Time (s)');
    ylabel('Desired vs. Actual')
    legend('Location', 'EastOutside', ...
        {yact_labels{:}, ydes_labels{:}, 'footheight', 'toeheight'});
end