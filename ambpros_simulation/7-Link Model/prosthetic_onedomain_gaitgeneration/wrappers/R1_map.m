% degree of four

function [xr, dr] = R1_map(xe, d)
n       = length(xe)/2;
R       = fliplr(eye(n-3));
 
dqe     = xe(n+1:end);
D       = De_mat(xe);
Jnsf    = J_nsf(xe);
J       = Jnsf;

P       = [D, -J';
            J, zeros(size(J, 1))] ...
            \ [D * dqe; zeros(size(J, 1), 1)];

pos_pre = footpos_mat(xe);

px      = pos_pre(1,2);
pz      = pos_pre(2,2);

qr      = [px; 0; 0; R*xe(4:n)];
qdotr   = [0;0;0;R*P(4:n)];

xr      = [qr;qdotr];
dr      = mod(d, 2) + 1;
end


