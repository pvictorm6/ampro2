%% read rosbag data logged from experiment
% function read_logdata()
% temp    = load('walking_2014-03-12_18-04.bag')
% end

MOTOR_REDUCTION     = 80;
TORQUE_SENS         = 0.0621;

%%desired motor current and joint torque
ankCurrCommand      = rosbag.control_feedback.control_signal_ankle;
kneeCurrCommand     = rosbag.control_feedback.control_signal_knee;

ankTorqCommand      = rosbag.control_feedback.torque_desired_ankle;
kneeTorqCommand     = rosbag.control_feedback.torque_desired_knee;


%%actual motor current and joint torque
ankCurrFeedback     = rosbag.control_feedback.torque_actual_ankle/MOTOR_REDUCTION/TORQUE_SENS;
kneeCurrFeedback    = rosbag.control_feedback.torque_actual_knee/MOTOR_REDUCTION/TORQUE_SENS;

ankTorqFeedback     = rosbag.control_feedback.torque_actual_ankle;
kneeTorqFeedback    = rosbag.control_feedback.torque_actual_knee;

%%desired joint angle positions
ankPosReference     = rosbag.control_feedback.position_desired_ankle;
kneePosReference    = rosbag.control_feedback.position_desired_knee;

ankVelReference     = rosbag.control_feedback.velocity_desired_ankle;
kneeVelReference    = rosbag.control_feedback.velocity_desired_knee;

%%actual joint angle positions and velocities
ankPosFeedback      = rosbag.control_feedback.position_actual_ankle;
kneePosFeedback     = rosbag.control_feedback.position_actual_knee;

ankVelFeedback      = rosbag.control_feedback.velocity_actual_ankle;
kneeVelFeedback     = rosbag.control_feedback.velocity_actual_knee;

%%process the time
logtime_sec         = rosbag.control_feedback.t_mat;
dftime              = diff(logtime_sec);
logtime             = cumsum(dftime);

startPoint          = 1;
endPoint            = length(logtime);
plot_range          = startPoint:1:endPoint;



%%
figure(1); clf;
subplot(211)
plot(logtime(plot_range), ankTorqCommand(plot_range), 'b--', 'LineWidth', 2)
hold on
plot(logtime(plot_range), ankTorqFeedback(plot_range), 'r', 'LineWidth', 2)
title('Ankle joint torque plots')
legend('Desired ankle torque', 'Actual ankle torque')
subplot(212)
plot(logtime(plot_range), kneeTorqCommand(plot_range), 'b--', 'LineWidth', 2)
hold on
plot(logtime(plot_range), kneeTorqFeedback(plot_range), 'r', 'LineWidth', 2)
legend('Desired knee torque', 'Actual knee torque')
title('Knee joint torque plots')


figure(2); clf;
subplot(211)
plot(logtime(plot_range), ankCurrCommand(plot_range), 'b--', 'LineWidth', 2)
hold on
plot(logtime(plot_range), ankCurrFeedback(plot_range), 'r', 'LineWidth', 2)
legend('Desired ankle current', 'Actual ankle current')
title('Ankle motor current plots')
subplot(212)
plot(logtime(plot_range), kneeCurrCommand(plot_range), 'b--', 'LineWidth', 2)
hold on
plot(logtime(plot_range), kneeCurrFeedback(plot_range), 'r', 'LineWidth', 2)
legend('Desired knee current', 'Actual knee current')
title('Knee motor current plots')


figure(3); clf;
subplot(211)
plot(logtime(plot_range), ankPosReference(plot_range), 'b--', 'LineWidth', 2)
hold on
plot(logtime(plot_range), ankPosFeedback(plot_range), 'r', 'LineWidth', 2)
% hold on
% plot(logtime(plot_range), ankVelFeedback(plot_range), 'k')
legend('Desired ankle angle', 'Actual ankle angle')
title('Ankle angle Plots')
subplot(212)
plot(logtime(plot_range), kneePosReference(plot_range), 'b--', 'LineWidth', 2)
hold on
plot(logtime(plot_range), kneePosFeedback(plot_range), 'r', 'LineWidth', 2)
legend('Desired knee angle', 'Actual knee angle')
title('Knee angle plots')


figure(4); clf;
subplot(211)
plot(logtime(plot_range), ankVelReference(plot_range), 'b--', 'LineWidth', 2)
hold on
plot(logtime(plot_range), ankVelFeedback(plot_range), 'r', 'LineWidth', 2)
legend('Desired ankle velocity', 'Actual ankle velocity')
title('Ankle velocity Plots')
subplot(212)
plot(logtime(plot_range), kneeVelReference(plot_range), 'b--', 'LineWidth', 2)
hold on
plot(logtime(plot_range), kneeVelFeedback(plot_range), 'r', 'LineWidth', 2)
legend('Desired knee velocity', 'Actual knee velocity')
title('Knee velocity Plots')

%% plot the frequency  
loopfrequency       = 0.005; %s
actualfrequency     = mean(dftime);

figure(5); clf;
plot(dftime(plot_range))
hold on
plot(plot_range, loopfrequency, '--r', 'LineWidth', 3)
hold on
plot(plot_range, actualfrequency, '--m', 'LineWidth', 3)
legend('Execution Time', ['Desired Frequency: ', num2str(loopfrequency)],...
    ['Actual average Frequency: ', num2str(actualfrequency)])


