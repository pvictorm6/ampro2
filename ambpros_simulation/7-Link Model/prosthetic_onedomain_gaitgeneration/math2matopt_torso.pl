## Mathematica output file list:
#
@filenames = ("y_minus","y_plus","H_minus","H_plus","tau");
## Parse the mathematica output files and write them to disk in a format usable by MATLAB:
#
for ($matnum = 0; $matnum < scalar(@filenames); $matnum++) {
    $filename = @filenames[$matnum];
    open(DAT, "< ./buildopt_torso/$filename") || die("Could not open file: " . $filename);
    @raw_data = <DAT>;
    close(DAT);
    $mat_str = "function x@filenames[$matnum] = @filenames[$matnum](x, a)\n"; # remove p
    $mat_str .= "x@filenames[$matnum] = ";
     
    foreach $linevar (@raw_data) {
        $linevar =~ s/\},/\};/g;
        $linevar =~ s/\n/ ...\n/g;
        $linevar =~ s/Pi/pi/g;

	$linevar =~ s/Abs/abs/g;
	$linevar =~ s/Sign/sign/g;

        $linevar =~ s/Sec/sec/g;
        $linevar =~ s/Csc/csc/g;
        $linevar =~ s/Cot/cot/g;

        $linevar =~ s/ArcCos/acos/g;
        $linevar =~ s/Sqrt/sqrt/g;
        
        $linevar =~ s/Cos/cos/g;
        $linevar =~ s/Sin/sin/g;
        $linevar =~ s/Tan/tan/g;

	$linevar =~ s/E/exp(1)/g;
        
        $linevar =~ s/\[/\(/g;
        $linevar =~ s/\]/\)/g;
        $linevar =~ s/\{/\[/g;
        $linevar =~ s/\}/\]/g;
        
        $mat_str .= $linevar;
    }
    $mat_str .= ";";

    $newfilename = @filenames[$matnum].".m";
    open(DAT, "> ./buildopt_torso/$newfilename") || die "Can't open $newfilename : $!";
    print DAT $mat_str;
    close(DAT);
}

## Mathematica output file list:
#
@filenames = ("h_dot_minus");
## Parse the mathematica output files and write them to disk in a format usable by MATLAB:
#
for ($matnum = 0; $matnum < scalar(@filenames); $matnum++) {
    $filename = @filenames[$matnum];
    open(DAT, "< ./buildopt_torso/$filename") || die("Could not open file: " . $filename);
    @raw_data = <DAT>;
    close(DAT);
    $mat_str = "function x@filenames[$matnum] = @filenames[$matnum](x)\n"; # remove p
    $mat_str .= "x@filenames[$matnum] = ";
     
    foreach $linevar (@raw_data) {
        $linevar =~ s/\},/\};/g;
        $linevar =~ s/\n/ ...\n/g;
        $linevar =~ s/Pi/pi/g;

	$linevar =~ s/Abs/abs/g;
	$linevar =~ s/Sign/sign/g;

        $linevar =~ s/Sec/sec/g;
        $linevar =~ s/Csc/csc/g;
        $linevar =~ s/Cot/cot/g;

        $linevar =~ s/ArcCos/acos/g;
        $linevar =~ s/Sqrt/sqrt/g;
        
        $linevar =~ s/Cos/cos/g;
        $linevar =~ s/Sin/sin/g;
        $linevar =~ s/Tan/tan/g;

	$linevar =~ s/E/exp(1)/g;
        
        $linevar =~ s/\[/\(/g;
        $linevar =~ s/\]/\)/g;
        $linevar =~ s/\{/\[/g;
        $linevar =~ s/\}/\]/g;
        
        $mat_str .= $linevar;
    }
    $mat_str .= ";";

    $newfilename = @filenames[$matnum].".m";
    open(DAT, "> ./buildopt_torso/$newfilename") || die "Can't open $newfilename : $!";
    print DAT $mat_str;
    close(DAT);
}

## Mathematica output file list:
#
@filenames = ("theta_a1","theta_a2","theta_a3","theta_a4","theta_a5","theta_a6", "Phi_mat");

## Parse the mathematica output files and write them to disk in a format usable by MATLAB:
#
for ($matnum = 0; $matnum < scalar(@filenames); $matnum++) {
    $filename = @filenames[$matnum];
    open(DAT, "< ./buildopt_torso/$filename") || die("Could not open file: " . $filename);
    @raw_data = <DAT>;
    close(DAT);
    $mat_str = "function x@filenames[$matnum] = @filenames[$matnum](x,a)\n"; # remove p
    $mat_str .= "x@filenames[$matnum] = ";
     
    foreach $linevar (@raw_data) {
        $linevar =~ s/\},/\};/g;
        $linevar =~ s/\n/ ...\n/g;
        $linevar =~ s/Pi/pi/g;

	$linevar =~ s/Abs/abs/g;
	$linevar =~ s/Sign/sign/g;

        $linevar =~ s/Sec/sec/g;
        $linevar =~ s/Csc/csc/g;
        $linevar =~ s/Cot/cot/g;

        $linevar =~ s/ArcCos/acos/g;
        $linevar =~ s/Sqrt/sqrt/g;
        
        $linevar =~ s/Cos/cos/g;
        $linevar =~ s/Sin/sin/g;
        $linevar =~ s/Tan/tan/g;

	$linevar =~ s/E/exp(1)/g;
        
        $linevar =~ s/\[/\(/g;
        $linevar =~ s/\]/\)/g;
        $linevar =~ s/\{/\[/g;
        $linevar =~ s/\}/\]/g;
        
        $mat_str .= $linevar;
    }
    $mat_str .= ";";

    $newfilename = @filenames[$matnum].".m";
    open(DAT, "> ./buildopt_torso/$newfilename") || die "Can't open $newfilename : $!";
    print DAT $mat_str;
    close(DAT);
}

## Mathematica output file list:
#
@filenames = ("Phi1_inverse_mat","Phi1_dot_inverse_mat");
## Parse the mathematica output files and write them to disk in a format usable by MATLAB:
#
for ($matnum = 0; $matnum < scalar(@filenames); $matnum++) {
    $filename = @filenames[$matnum];
    open(DAT, "< ./buildopt_torso/$filename") || die("Could not open file: " . $filename);
    @raw_data = <DAT>;
    close(DAT);
    $mat_str = "function x@filenames[$matnum] = @filenames[$matnum](z,p,a)\n"; # remove p
    $mat_str .= "x@filenames[$matnum] = ";
     
    foreach $linevar (@raw_data) {
        $linevar =~ s/\},/\};/g;
        $linevar =~ s/\n/ ...\n/g;
        $linevar =~ s/Pi/pi/g;

	$linevar =~ s/Abs/abs/g;
	$linevar =~ s/Sign/sign/g;

        $linevar =~ s/Sec/sec/g;
        $linevar =~ s/Csc/csc/g;
        $linevar =~ s/Cot/cot/g;

        $linevar =~ s/ArcCos/acos/g;
        $linevar =~ s/Sqrt/sqrt/g;
        
        $linevar =~ s/Cos/cos/g;
        $linevar =~ s/Sin/sin/g;
        $linevar =~ s/Tan/tan/g;

	$linevar =~ s/E/exp(1)/g;
        
        $linevar =~ s/\[/\(/g;
        $linevar =~ s/\]/\)/g;
        $linevar =~ s/\{/\[/g;
        $linevar =~ s/\}/\]/g;
        
        $mat_str .= $linevar;
    }
    $mat_str .= ";";

    $newfilename = @filenames[$matnum].".m";
    open(DAT, "> ./buildopt_torso/$newfilename") || die "Can't open $newfilename : $!";
    print DAT $mat_str;
    close(DAT);
}