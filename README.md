# README #

This is the HUR Group AMPRO2 repository. The set of code is C++ and is a clone of the original AMPRO2 code, however, from now on, the code has been (on process) replaced by new hur group only code. 

### What is this repository for? ###

* Controller and drivers stacks for AMPRO2
* Utilities for programming AMPRO2
* Keep Track of the work made by each member of the project

### How do I get set up? ###

* You need ROS (Hydro and Indigo tested) and Ubuntu (12.04 and 14.10 tested)
* This repo depends on ros_imu, flexiforce adapter and amber_development_stack
* Other important dependencies: yaml-cpp, eigen.

### Contribution guidelines ###

* Writing tests when convenient (matlab and c++ code output comparison)
* Create a branch on your application and when reviewed by the maintainer, test it on AMPRO and add it to master
* Think about efficiency (although some codes are not efficient, they will be rewritten)

### Who do I talk to? ###

* Repo owner or admin (for now Victor Paredes)
* Dr. Pilwon Hur