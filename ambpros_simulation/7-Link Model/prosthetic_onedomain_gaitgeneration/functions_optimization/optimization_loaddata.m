function humandata = optimization_loaddata(robotwalk)
    %% load human experimental data
    humandata               = struct;
    temp = load('mean_hippos.mat');
    humandata.mean_hippos   = temp.mean_hippos';

    temp = load('mean_nsslope.mat');
    humandata.mean_nsslope  = temp.mean_nsslope';

    temp = load('mean_torso.mat');
    humandata.mean_torso    = temp.mean_torso';

    temp = load('mean_sknee.mat');
    humandata.mean_sknee    = temp.mean_sknee';

    temp = load('mean_nsknee.mat');
    humandata.mean_nsknee   = temp.mean_nsknee';

    temp = load('mean_nsfoot.mat');
    humandata.mean_time     = temp.mean_time';
    humandata.mean_nsfoot   = temp.mean_nsfoot';
    
    %% get the initial fit. First check whether the a_fit already exists.
    % If not, run the get_fitinitial to get the intial fit of all the outputs.
    % Note that, the get_fitinitial will only run once and will save the a_fit.
    if(exist('human_data/a_fit.mat','file'))
        temp            = load('human_data/a_fit.mat');
        if size(temp.a_fit,2) == robotwalk.nParameter
            humandata.a_fit = temp.a_fit;
        else
            humandata.a_fit = get_afit(humandata, robotwalk);
        end
    else
        humandata.a_fit = get_afit(humandata, robotwalk);
    end
end