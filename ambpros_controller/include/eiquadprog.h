#ifndef _EIGEN_QUADSOLVE_HPP_
#define _EIGEN_QUADSOLVE_HPP_

#include <eigen3/Eigen/Cholesky>
#include <eigen3/Eigen/Dense>

//TODO Refactor into a class, storing the variables in that class so stuff doesn't have to be reallocated as much.

namespace Eigen {

  /* solve_quadprog2 is used when the Cholesky decomposition of the G matrix is precomputed */
  double solve_quadprog2(LLT<MatrixXd,Lower> &chol,  double c1, VectorXd & g0,  
			 const MatrixXd & CE, const VectorXd & ce0,  
			 const MatrixXd & CI, const VectorXd & ci0, 
			 VectorXd& x, double eqTolerance = 10.0 * std::numeric_limits<double>::epsilon());

  /* solve_quadprog is used for on-demand QP solving */
  double solve_quadprog(MatrixXd & G,  VectorXd & g0,  
			       const MatrixXd & CE, const VectorXd & ce0,  
			       const MatrixXd & CI, const VectorXd & ci0, 
			       VectorXd& x, double eqTolerance = 10.0 * std::numeric_limits<double>::epsilon());

}

#endif
