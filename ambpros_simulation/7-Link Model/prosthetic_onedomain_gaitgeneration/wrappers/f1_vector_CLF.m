function ret = f1_vector_CLF(t,x,tp0,logObj,robotwalk)
ep      = robotwalk.ep;
a       = robotwalk.a_opt{robotwalk.dr,1};
p       = robotwalk.p;
nbasedof= robotwalk.nBaseDof;
ndof    = robotwalk.nDof;
nexdof  = nbasedof + ndof;
dx      = x(nexdof+1:end);

%%%Control Fields
M       = De_mat(x);
C       = Ce_mat(x);
G       = Ge_vec(x);
He      = C*dx + G;

Jsf     = J_sf(x);
Jdotsf  = Jdot_sf(x);
J       = Jsf;
Jdot    = Jdotsf;

invM    = inv(M);
Xi      = inv(J*invM*J');
vf      = [dx; M \(-He)];
B_IO    = [zeros(nbasedof, ndof); eye(ndof)];
gf      = [zeros(size(B_IO)); M \ (eye(nexdof) - ...
                J'*Xi*J*invM)*B_IO];

%%%Outputs
y_a1    = ya1_sca(x, p, a);
y_a2    = ya2_vec(x, p, a);
y_d1    = yd1_sca(x, p,  a);
y_d2    = yd2_vec(x, p,  a);

%%%Outputs
y1      = y_a1 - y_d1;
y2      = y_a2 - y_d2;
y_a     = [y_a1; y_a2];
y_d     = [y_d1; y_d2];

%%%Jacobians of Outputs
Dy_a1   = Dya1_mat(x, p, a);
Dy_d1   = Dyd1_mat(x, p, a);
Dy_a2   = Dya2_mat(x, p, a);
Dy_d2   = Dyd2_mat(x, p, a);
Dy_1    = Dy_a1 - Dy_d1;
Dy_2    = Dy_a2 - Dy_d2;

%%%Lie Derivatives
Lgy1    = Dy_1*gf;
Lgy2    = Dy_2*gf;
Lfy1    = Dy_1*vf;
Lfy2    = Dy_2*vf;

%%%Second Order Jacobians
DLfy_a2 = DLfya2_mat(x, p, a);
DLfy_d2 = DLfyd2_mat(x, p, a);
DLfy2   = DLfy_a2-DLfy_d2;

%%%Second Lie Derivatives
LfLfy2  = DLfy2*vf;
LgLfy2  = DLfy2*gf;

% Vector field
% A   = [Lgy1; LgLfy2];
% u   = -A \ ([0; LfLfy2]+[Lfy1; 2*ep*Lfy2]+ [ep*y1; ep^2*y2]);
% ret = vf + gf * u;
%% CLF computation
nCon        = size(J, 1);
Bbar        = [B_IO, J'];
gbar_f      = [zeros(size(Bbar)); M \ Bbar];

Lgbar_y1    = Dy_1 * gbar_f;
Lgbar_Lf_y2 = DLfy2 * gbar_f;
Lgbar_yx1   = [zeros(nCon, nexdof), J] * gbar_f;

A_decouple  = [Lgbar_y1; Lgbar_Lf_y2; Lgbar_yx1];

Lfyx1       = [zeros(nCon, nexdof), J] * vf;
Lf_mat      = [Lfy1; LfLfy2; Lfyx1];

no1         = robotwalk.nR1Dof;
no2         = robotwalk.nR2Dof;
nox1        = nCon;

%% compute the the outputs from state variables
F_mat       = robotwalk.F;
G_mat       = robotwalk.G;
Pe_mat      = robotwalk.Pe;
C3_num      = robotwalk.C;
eta1        = y1;
eta2        = [y2; Lfy2];
eta_vect    = [eta1; eta2];
Ve_num      = eta_vect' * Pe_mat *eta_vect;
LfVe_num    = eta_vect' * (F_mat' * Pe_mat + Pe_mat * F_mat) * eta_vect;
Psi0        = LfVe_num + (C3_num / ep) * Ve_num;
Psi1        = (2*eta_vect' * Pe_mat * G_mat)';

% Framed in terms of torque, not mu
nOutputs    = no1 + no2;
nXOutputs   = nOutputs + nox1;
nUbar       = nXOutputs;
nMu         = 1 + nUbar;
H           = zeros(nMu);

H(1, 1)     = robotwalk.Penalty;

H(2:end, 2:end) = (A_decouple' * A_decouple);

f           = [0; 2 * A_decouple' * Lf_mat];

%% Set up the constraints
%%% Dynamic Constraints
AuJ         = J*invM*Bbar;
buJ         = -Jdot*dx + J*invM*He;

%%% CLF Constraints
A1          = [-1, Psi1' * A_decouple(1:nOutputs,:)];
b1          = -Psi0 - Psi1' * Lf_mat(1:nOutputs,:);

%%% Torque Conditions
u_max       = robotwalk.Umax;
%upper bound on torque
A2          = [zeros(nOutputs, 1), eye(nOutputs), zeros(nOutputs, nCon)];
b2          = u_max*ones(nOutputs, 1);
%lower bound on torque
A3          = [zeros(nOutputs, 1), -eye(nOutputs), zeros(nOutputs, nCon)];
b3          = u_max*ones(nOutputs, 1);

%%% ZMP Constraints
lh          = 5937/100000;
lt          = 891/6250;
A4          = [zeros(1, nOutputs + 2), -lt, 1;
                zeros(1, nOutputs + 2), -lh, -1];
b4          = zeros(2, 1);

%%% Friction Constraints
% nLCon       = size(AuJ, 1);
% Fric_mu     = 0.2;
% ILCon       = -eye(nLCon);
% ILCon(1,1)  = -1;
% ILCon(1,2)  = -Fric_mu;
% ILCon(2,1)  = 1;
% ILCon(2,2)  = -Fric_mu;
% AF          = [zeros(nLCon, 1 + nOutputs), ILCon];
% bF          = zeros(nLCon, 1);
AF = [];
bF = [];

%%%Add all the contraints together
Aeq         = [zeros(nCon, 1), AuJ];
beq         = buJ;

A           = [A1;
            A2;
            A3;
            A4
            AF;
            ];
        
b           = [b1;
            b2;
            b3;
            b4
            bF;
            ];

% %% Check whether H is positive definite
% if (min(eig(H)) <= 0)
%     fprintf('QP is not Positive-Definite !\n')
%     keyboard
% end

opts = optimset('Display', 'off', 'Algorithm', 'interior-point-convex');
[U, fval, exitflag, output, lambda] = ...
        quadprog(2 * H, f, A, b, Aeq, beq, [], [], [], opts);

u       = U(2:end);

ret     = vf + gbar_f * u;
%% log the data
tp  = t + tp0;
Pow = [0;0;0;u(1:6)].*dx;
logObj.append(tp,tp0,x,robotwalk.dr,y_a,y_d,u,Pow);
end
