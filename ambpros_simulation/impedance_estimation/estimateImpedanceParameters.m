% function estimateImpedanceParameters()
%% load the dataa
% close all;   clc;
addpath('exp_data','functions_fit','functions_impedance')
% folder  = 'expdata/';
% file    = '0030-ts';
% data    = dlmread(strcat(folder, file));

%%actual joint angle positions and velocities, torques
ankTorqFeedback     = rosbag.control_feedback.torque_actual_ankle;
kneeTorqFeedback    = rosbag.control_feedback.torque_actual_knee;

ankPosFeedback      = rosbag.control_feedback.position_actual_ankle;
kneePosFeedback     = rosbag.control_feedback.position_actual_knee;

ankVelFeedback      = rosbag.control_feedback.velocity_actual_ankle;
kneeVelFeedback     = rosbag.control_feedback.velocity_actual_knee;

%%desired joint angle positions, velocities and torques
ankTorqCommand      = rosbag.control_feedback.torque_desired_ankle;
kneeTorqCommand     = rosbag.control_feedback.torque_desired_knee;

ankPosReference     = rosbag.control_feedback.position_desired_ankle;
kneePosReference    = rosbag.control_feedback.position_desired_knee;

ankVelReference     = rosbag.control_feedback.velocity_desired_ankle;
kneeVelReference    = rosbag.control_feedback.velocity_desired_knee;

%%process the time
logtime_sec         = rosbag.control_feedback.t_mat;
dftime              = diff(logtime_sec);
logtime             = cumsum(dftime);

%% plot data
ndof    = 2;
start   = 1;
finish  = size(logtime, 1);
qa      = zeros(finish- start + 1, ndof);
qd      = zeros(finish- start + 1, ndof);
dqa     = zeros(finish- start + 1, ndof);
dqd     = zeros(finish- start + 1, ndof);
tord    = zeros(finish- start + 1, ndof);
tora    = zeros(finish- start + 1, ndof);

t       = logtime(start:finish);

qa(:, 1)    = ankPosFeedback(start:finish);
qd(:, 1)    = ankPosReference(start:finish);
dqa(:, 1)   = ankVelFeedback(start:finish);
dqd(:, 1)   = ankVelReference(start:finish);
tora(:, 1)  = ankTorqFeedback(start:finish);
tord(:, 1)  = ankTorqCommand(start:finish); %% ankle data
qa(:, 2)    = kneePosFeedback(start:finish);
qd(:, 2)    = kneePosReference(start:finish);
dqa(:, 2)   = kneeVelFeedback(start:finish);
dqd(:, 2)   = kneeVelReference(start:finish);
tora(:, 2)  = kneeTorqFeedback(start:finish);
tord(:, 2)  = kneeTorqCommand(start:finish); %% knee data

legend_joint    = {'ankle', 'knee'};
legend_term     = {'{pos}', '{vel}', '{tor}'};
legend_ad       = {'{a}', '{d}'};


for i = 1:1 %ndof
    figure(i)
    subplot(311)
    plot(t, qa(:, i), 'r', t, qd(:, i), 'b');
    legend({[legend_joint{i},'_', legend_term{1},'^',legend_ad{1}], [legend_joint{i},'_', legend_term{1},'^',legend_ad{2}]})

    subplot(312)
    plot(t, dqa(:, i), 'r', t, dqd(:, i)*0, 'b');
    legend({[legend_joint{i},'_', legend_term{2},'^',legend_ad{1}], [legend_joint{i},'_', legend_term{2},'^',legend_ad{2}]})

    subplot(313)
    plot(t, tora(:, i), 'r', t, tord(:, i), 'b');
    legend({[legend_joint{i},'_', legend_term{3},'^',legend_ad{1}], [legend_joint{i},'_', legend_term{3},'^',legend_ad{2}]})
end

%% Impedance Estimation
%% break point initialization
% DELTAT          = 0.005;
% SHIFT           = -floor(21.58/DELTAT);
% startTime       = 44.44;
% stanceTime      = 44.94;
% swingTime       = 45.03;
% endTime         = 45.23;
% indexStart      = floor(startTime/DELTAT) + SHIFT;  
% indexStance     = floor(stanceTime/DELTAT) + SHIFT;
% indexSwing      = floor(swingTime/DELTAT) + SHIFT;  
% indexEnd        = floor(endTime/DELTAT) + SHIFT;
indexStart      = 15953;
indexStance     = 15987;
indexSwing      = 16041;
indexEnd        = 16112;
range_onestep   = indexStart:indexEnd;
range_stance1   = indexStart:indexStance;
range_stance2   = indexStance + 1: indexEnd;
range_swing1    = indexStart: indexSwing;
range_swing2    = indexSwing + 1: indexEnd;

range_stance1onestep    = 1: (indexStance - indexStart + 1);
range_stance2onestep    = (indexStance - indexStart + 2) : (indexEnd - indexStart + 1);
range_swing1onestep     = 1: (indexSwing - indexStart + 1);
range_swing2onestep     = (indexSwing - indexStart + 2) : (indexEnd - indexStart + 1);

trange          = t(range_onestep);

%% smooth the velocity and torque data
Fwin            = 'hamming';
WidthWin        = 10;
dqa_smooth      = zeros(indexEnd - indexStart + 1, ndof);
tora_smooth     = zeros(indexEnd - indexStart + 1, ndof);
tord_smooth     = zeros(indexEnd - indexStart + 1, ndof);
% dqa_smooth      = zeros(finish - start + 1, ndof);`
% tor_smooth      = zeros(finish - start + 1, ndof);
for i = 1:ndof
    tora_smooth(:, i) = ndnanfilter(tora(indexStart:indexEnd, i), Fwin, WidthWin*1);
    tord_smooth(:, i) = ndnanfilter(tord(indexStart:indexEnd, i), Fwin, WidthWin*1);
    dqa_smooth(:, i)  = ndnanfilter(dqa(indexStart:indexEnd, i), Fwin, WidthWin*1);

    
    switch i
        case 1
            name = 'LA';
        case 2
            name = 'LK';
        case 3
            name = 'LH';
        case 4
            name = 'RH';
        case 5
            name = 'RK';
        case 6
            name = 'RA';
    end
    figure(i + 40);  %%plot the smooth data
    subplot(211)
    plot(trange, qa(range_onestep, i), 'b', trange, dqa(range_onestep, i),...
                'r', trange, dqa_smooth(:, i), 'k')
    legend('position', 'velocity', 'smooth velocity')
    title(name)
    subplot(212)
    plot(trange, tord_smooth(:, i), 'r', trange, tora_smooth(:, i),'k')
    legend('desired torque', 'actual torque')
end

%% estimate the impedance parameters
%%%%Using smoothed velocity data%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
estimation_stance   = @dynamic_estimation_stance_imped;
estimation_swing    = @dynamic_estimation_swing_imped;
%%estimate the impedance parameters for domain1 of stance phase   
ImpedParams.stance1 = estimation_stance(t(range_stance1), qa(range_stance1, :), ...
                    dqa_smooth(range_stance1onestep, :), tora_smooth(range_stance1onestep, :));
gcf; title('dynamic estimation of domain1 of stance phase')
%%ImpedParams.stance1

%%estimate the impedance parameters for domain2 of stance phase
ImpedParams.stance2 = estimation_stance(t(range_stance2), qa(range_stance2, :), ...
                    dqa_smooth(range_stance2onestep, :), tora_smooth(range_stance2onestep, :));
gcf; title('dynamic estimation of domain2 of stance phase')
ImpedParams.stance1
ImpedParams.stance2
%%
%%estimate the impedance parameters for domain1 of swing phase   
ImpedParams.swing1  = estimation_swing(t(range_swing1), qa(range_swing1, :), ...
                    dqa_smooth(range_swing1onestep, :), tora_smooth(range_swing1onestep, :));
gcf; title('dynamic estimation of domain1 of swing phase')

%%estimate the impedance parameters for domain2 of swing phase
ImpedParams.swing2  = estimation_swing(t(range_swing2), qa(range_swing2, :), ...
                    dqa_smooth(range_swing2onestep, :), tora_smooth(range_swing2onestep, :));
gcf; title('dynamic estimation of domain2  of swing phase')

ImpedParams.swing1
ImpedParams.swing2
%%%%Using smoothed velocity data%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%Using Non-smooth velocity data%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% estimation_stance   = @dynamic_estimation_stance_imped;
% estimation_swing    = @dynamic_estimation_swing_imped;
% %%estimate the impedance parameters for domain1 of stance phase   
% ImpedParams.stance1 = estimation_stance(t(range_stance1), qa(range_stance1, :), ...
%                         dqa(range_stance1, :), tor_smooth(range_stance1onestep, :));
% gcf; title('dynamic estimation of domain1 of stance phase')
% % ImpedParams.stance1
% 
% %%estimate the impedance parameters for domain2 of stance phase
% ImpedParams.stance2 = estimation_stance(t(range_stance2), qa(range_stance2, :), ...
%                         dqa(range_stance2, :), tor_smooth(range_stance2onestep, :));
% gcf; title('dynamic estimation of domain2 of stance phase')
% ImpedParams.stance1
% ImpedParams.stance2
% %%
% %%estimate the impedance parameters for domain1 of swing phase   
% ImpedParams.swing1  = estimation_swing(t(range_swing1), qa(range_swing1, :), ...
%                     dqa(range_swing1, :), tor_smooth(range_swing1onestep, :));
% gcf; title('dynamic estimation of domain1 of swing phase')
% 
% %%estimate the impedance parameters for domain2 of swing phase
% ImpedParams.swing2  = estimation_swing(t(range_swing2), qa(range_swing2, :), ...
%                     dqa(range_swing2, :), tor_smooth(range_swing2onestep, :));
% gcf; title('dynamic estimation of domain2  of swing phase')
% ImpedParams.swing1
% ImpedParams.swing2


%% new format
ImpedParams.k1  = [ImpedParams.stance1(:, 1)', ImpedParams.swing1(:, 1)'];
ImpedParams.k2  = [ImpedParams.stance2(:, 1)', ImpedParams.swing2(:, 1)'];
ImpedParams.b1  = [ImpedParams.stance1(:, 2)', ImpedParams.swing1(:, 2)'];
ImpedParams.b2  = [ImpedParams.stance2(:, 2)', ImpedParams.swing2(:, 2)'];
ImpedParams.qe1 = [ImpedParams.stance1(:, 3)', ImpedParams.swing1(:, 3)'];
ImpedParams.qe2 = [ImpedParams.stance2(:, 3)', ImpedParams.swing2(:, 3)'];

ImpedParams.tau_stance  = (indexStance - indexStart)/(indexEnd - indexStart)
ImpedParams.tau_swing   = (indexSwing - indexStart)/(indexEnd - indexStart)
ImpedParams.k1
ImpedParams.k2
ImpedParams.b1
ImpedParams.b2
ImpedParams.qe1
ImpedParams.qe2

















