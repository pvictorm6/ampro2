% function estimateImpedanceParameters()
%% load the dataa
% close all;   clc;
addpath('exp_data','functions_fit','functions_impedance')

%% actual joint angle positions and velocities, torques
ankTorqFeedback     = rosbag.control_feedback.torque_actual_ankle;
kneeTorqFeedback    = rosbag.control_feedback.torque_actual_knee;

ankPosFeedback      = rosbag.control_feedback.position_actual_ankle;
kneePosFeedback     = rosbag.control_feedback.position_actual_knee;

ankVelFeedback      = rosbag.control_feedback.velocity_actual_ankle;
kneeVelFeedback     = rosbag.control_feedback.velocity_actual_knee;

%%desired joint angle positions, velocities and torques
ankTorqCommand      = rosbag.control_feedback.torque_desired_ankle;
kneeTorqCommand     = rosbag.control_feedback.torque_desired_knee;

ankPosReference     = rosbag.control_feedback.position_desired_ankle;
kneePosReference    = rosbag.control_feedback.position_desired_knee;

ankVelReference     = rosbag.control_feedback.velocity_desired_ankle;
kneeVelReference    = rosbag.control_feedback.velocity_desired_knee;

%%process the time
logtime_sec         = rosbag.control_feedback.t_mat;
dftime              = diff(logtime_sec);
logtime             = cumsum(dftime);

%% plot data
ndof    = 2;
start   = 1;
finish  = size(logtime, 1);
qa      = zeros(finish- start + 1, ndof);
qd      = zeros(finish- start + 1, ndof);
dqa     = zeros(finish- start + 1, ndof);
dqd     = zeros(finish- start + 1, ndof);
tord    = zeros(finish- start + 1, ndof);
tora    = zeros(finish- start + 1, ndof);

t       = logtime(start:finish);

qa(:, 1)    = ankPosFeedback(start:finish);
qd(:, 1)    = ankPosReference(start:finish);
dqa(:, 1)   = ankVelFeedback(start:finish);
dqd(:, 1)   = ankVelReference(start:finish);
tora(:, 1)  = ankTorqFeedback(start:finish);
tord(:, 1)  = ankTorqCommand(start:finish); %% ankle data
qa(:, 2)    = kneePosFeedback(start:finish);
qd(:, 2)    = kneePosReference(start:finish);
dqa(:, 2)   = kneeVelFeedback(start:finish);
dqd(:, 2)   = kneeVelReference(start:finish);
tora(:, 2)  = kneeTorqFeedback(start:finish);
tord(:, 2)  = kneeTorqCommand(start:finish); %% knee data

legend_joint    = {'ankle', 'knee'};
legend_term     = {'{pos}', '{vel}', '{tor}'};
legend_ad       = {'{a}', '{d}'};


for i = 1:1 %ndof
    figure(i)
    subplot(311)
    plot(t, qa(:, i), 'r', t, qd(:, i), 'b');
    legend({[legend_joint{i},'_', legend_term{1},'^',legend_ad{1}], [legend_joint{i},'_', legend_term{1},'^',legend_ad{2}]})

    subplot(312)
    plot(t, dqa(:, i), 'r', t, dqd(:, i)*1, 'b');
    legend({[legend_joint{i},'_', legend_term{2},'^',legend_ad{1}], [legend_joint{i},'_', legend_term{2},'^',legend_ad{2}]})

    subplot(313)
    plot(t, tora(:, i), 'r', t, tord(:, i), 'b');
    legend({[legend_joint{i},'_', legend_term{3},'^',legend_ad{1}], [legend_joint{i},'_', legend_term{3},'^',legend_ad{2}]})
end

%% Impedance Estimation
%% stance phase
%%break point initialization for stance phase
indexStartStance= 11953;
indexStance1    = 12987;
indexStance2    = 13041;
indexEndStance  = 13112;

%%stance phase
range_stance_onestep   = indexStartStance:indexEndStance;
range_stance1           = indexStartStance:indexStance1;
range_stance2           = indexStance1 + 1:indexStance2;
range_stance3           = indexStance2 + 1: indexEndStance;
range_stance1onestep    = 1: (indexStance1 - indexStartStance + 1);
range_stance2onestep    = (indexStance1 - indexStartStance + 2) : (indexStance2 - indexStartStance + 1);
range_stance3onestep    = (indexStance2 - indexStartStance + 2) : (indexEndStance - indexStartStance + 1);
trange_stance           = t(range_stance_onestep);

%%smooth the velocity and torque data for stance phase
Fwin            = 'hamming';
WidthWin        = 10;
dqa_smooth      = zeros(length(range_stance_onestep), ndof);
tora_smooth     = zeros(length(range_stance_onestep), ndof);
tord_smooth     = zeros(length(range_stance_onestep), ndof);
% dqa_smooth      = zeros(finish - start + 1, ndof);`
% tor_smooth      = zeros(finish - start + 1, ndof);
for i = 1:1
    tora_smooth(:, i) = ndnanfilter(tora(range_stance_onestep, i), Fwin, WidthWin*1);
    tord_smooth(:, i) = ndnanfilter(tord(range_stance_onestep, i), Fwin, WidthWin*1);
    dqa_smooth(:, i)  = ndnanfilter(dqa(range_stance_onestep, i), Fwin, WidthWin*1);

    figure(i + 40);  %%plot the smooth data
    subplot(211)
    plot(trange_stance, qa(range_stance_onestep, i), 'b', trange_stance, dqa(range_stance_onestep, i),...
                'r', trange_stance, dqa_smooth(:, i), 'k')
    legend('position', 'velocity', 'smooth velocity')
    title(legend_joint{i})
    subplot(212)
    plot(trange_stance, tord_smooth(:, i), 'r', trange_stance, tora_smooth(:, i),'k')
    legend('desired torque', 'actual torque smoothed')
    title(legend_joint{i})
end

%% swing phase
%%break point initialization for swing phase
indexStartSwing = 11953;
indexSwing1     = 12987;
indexSwing2     = 13041;
indexEndSwing   = 13112;

range_swing_onestep    = indexStartSwing:indexEndSwing;
range_swing1           = indexStartSwing:indexSwing1;
range_swing2           = indexSwing1 + 1:indexSwing2;
range_swing3           = indexSwing2 + 1: indexEndSwing;
range_swing1onestep    = 1: (indexSwing1 - indexStartSwing + 1);
range_swing2onestep    = (indexSwing1 - indexStartSwing + 2) : (indexSwing2 - indexStartSwing + 1);
range_swing3onestep    = (indexSwing2 - indexStartSwing + 2) : (indexEndSwing - indexStartSwing + 1);
trange_swing           = t(range_swing_onestep);

%%smooth the velocity and torque data for swing phase
Fwin            = 'hamming';
WidthWin        = 10;
dqa_smooth      = zeros(length(range_swing_onestep), ndof);
tora_smooth     = zeros(length(range_swing_onestep), ndof);
tord_smooth     = zeros(length(range_swing_onestep), ndof);
% dqa_smooth      = zeros(finish - start + 1, ndof);`
% tor_smooth      = zeros(finish - start + 1, ndof);
for i = 1:1
    tora_smooth(:, i) = ndnanfilter(tora(range_swing_onestep, i), Fwin, WidthWin*1);
    tord_smooth(:, i) = ndnanfilter(tord(range_swing_onestep, i), Fwin, WidthWin*1);
    dqa_smooth(:, i)  = ndnanfilter(dqa(range_swing_onestep, i), Fwin, WidthWin*1);

    figure(i + 40);  %%plot the smooth data
    subplot(211)
    plot(trange_swing, qa(range_swing_onestep, i), 'b', trange_swing, dqa(range_swing_onestep, i),...
                'r', trange_swing, dqa_smooth(:, i), 'k')
    legend('position', 'velocity', 'smooth velocity')
    title(legend_joint{i})
    subplot(212)
    plot(trange_swing, tord_smooth(:, i), 'r', trange_swing, tora_smooth(:, i),'k')
    legend('desired torque', 'actual torque smoothed')
    title(legend_joint{i})
end

%% estimate the impedance parameters
%%%%Using smoothed velocity data%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
estimation_stance   = @dynamic_estimation_stance_imped;
estimation_swing    = @dynamic_estimation_swing_imped;
%%estimate the impedance parameters for domain1 of stance phase   
ImpedParams.stance1 = estimation_stance(t(range_stance1), qa(range_stance1, :), ...
                    dqa_smooth(range_stance1onestep, :), tora_smooth(range_stance1onestep, :));
gcf; title('dynamic estimation of domain1 of stance phase')
%%ImpedParams.stance1

%%estimate the impedance parameters for domain2 of stance phase
ImpedParams.stance2 = estimation_stance(t(range_stance2), qa(range_stance2, :), ...
                    dqa_smooth(range_stance2onestep, :), tora_smooth(range_stance2onestep, :));
gcf; title('dynamic estimation of domain2 of stance phase')

%%estimate the impedance parameters for domain2 of stance phase
ImpedParams.stance3 = estimation_stance(t(range_stance3), qa(range_stance3, :), ...
                    dqa_smooth(range_stance3onestep, :), tora_smooth(range_stance3onestep, :));
gcf; title('dynamic estimation of domain3 of stance phase')

ImpedParams.stance1
ImpedParams.stance2
ImpedParams.stance3
%%
%%estimate the impedance parameters for domain1 of swing phase   
ImpedParams.swing1  = estimation_swing(t(range_swing1), qa(range_swing1, :), ...
                    dqa_smooth(range_swing1onestep, :), tora_smooth(range_swing1onestep, :));
gcf; title('dynamic estimation of domain1 of swing phase')

%%estimate the impedance parameters for domain2 of swing phase
ImpedParams.swing2  = estimation_swing(t(range_swing2), qa(range_swing2, :), ...
                    dqa_smooth(range_swing2onestep, :), tora_smooth(range_swing2onestep, :));
gcf; title('dynamic estimation of domain2  of swing phase')

%%estimate the impedance parameters for domain3 of swing phase
ImpedParams.swing3  = estimation_swing(t(range_swing3), qa(range_swing3, :), ...
                    dqa_smooth(range_swing3onestep, :), tora_smooth(range_swing3onestep, :));
gcf; title('dynamic estimation of domain2  of swing phase')

ImpedParams.swing1
ImpedParams.swing2
ImpedParams.swing3
%%%%Using smoothed velocity data%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%Using Non-smooth velocity data%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% estimation_stance   = @dynamic_estimation_stance_imped;
% estimation_swing    = @dynamic_estimation_swing_imped;
% %%estimate the impedance parameters for domain1 of stance phase   
% ImpedParams.stance1 = estimation_stance(t(range_stance1), qa(range_stance1, :), ...
%                         dqa(range_stance1, :), tor_smooth(range_stance1onestep, :));
% gcf; title('dynamic estimation of domain1 of stance phase')
% % ImpedParams.stance1
% 
% %%estimate the impedance parameters for domain2 of stance phase
% ImpedParams.stance2 = estimation_stance(t(range_stance2), qa(range_stance2, :), ...
%                         dqa(range_stance2, :), tor_smooth(range_stance2onestep, :));
% gcf; title('dynamic estimation of domain2 of stance phase')
% ImpedParams.stance1
% ImpedParams.stance2
% %%
% %%estimate the impedance parameters for domain1 of swing phase   
% ImpedParams.swing1  = estimation_swing(t(range_swing1), qa(range_swing1, :), ...
%                     dqa(range_swing1, :), tor_smooth(range_swing1onestep, :));
% gcf; title('dynamic estimation of domain1 of swing phase')
% 
% %%estimate the impedance parameters for domain2 of swing phase
% ImpedParams.swing2  = estimation_swing(t(range_swing2), qa(range_swing2, :), ...
%                     dqa(range_swing2, :), tor_smooth(range_swing2onestep, :));
% gcf; title('dynamic estimation of domain2  of swing phase')
% ImpedParams.swing1
% ImpedParams.swing2


%% new format
ImpedParams.stance = [ImpedParams.stance1(1,:); ImpedParams.stance2(1,:); ImpedParams.stance3(1,:);...
                      ImpedParams.stance1(2,:); ImpedParams.stance2(2,:); ImpedParams.stance3(2,:)];
ImpedParams.swing  = [ImpedParams.swing1(1,:); ImpedParams.swing2(1,:); ImpedParams.swing3(1,:);...
                      ImpedParams.swing1(2,:); ImpedParams.swing2(2,:); ImpedParams.swing3(2,:)];
                  
ImpedParams.stance
ImpedParams.swing

addpath('./yaml');
yaml_init();
dir = './tmp';
yaml_write_file([dir, '/impedparameters.yaml'], ImpedParams);

% 
% ImpedParams.tau_stance  = (indexStance - indexStart)/(indexEnd - indexStart)
% ImpedParams.tau_swing   = (indexSwing - indexStart)/(indexEnd - indexStart)
% ImpedParams.k1
% ImpedParams.k2
% ImpedParams.b1
% ImpedParams.b2
% ImpedParams.qe1
% ImpedParams.qe2

















