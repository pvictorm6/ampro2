function plotfcn(x,a,t1,t2)



a1 = a(2:7,1:5);
a2 = a(9:end,:);

size1 = size(a1,2);
size2 = size(a2,1);
time1 = linspace(0,t1,size(x,2));
time2 = linspace(0,t2,100);

% % plot over the first domain
    for j = 1:length(time1)
            
            ya1(:,j) = ya2_vec(x(:,j),0,a1);
            
    end

% plot over the second domain
for i = 1:size2
    for j = 1:length(time2)
            
            yd2(i,j) = canonicalFunction(time2(j),a2(i,:));
            
    end
end

    figure(1);
plot(time1,ya1(1,:),'r',time1,ya1(2,:),'b',time1,ya1(3,:),'k',time1,ya1(4,:),'g',...
    time1,ya1(5,:),'y',time1,ya1(6,:),'c')
hold on
plot(time1(end)+time2,yd2(1,:),'r',time1(end)+time2,yd2(2,:),'b',time1(end)+time2,yd2(3,:),'k',...
    time1(end)+time2,yd2(4,:),'g',time1(end)+time2,yd2(5,:),'y',time1(end)+time2,yd2(6,:),'c')


end