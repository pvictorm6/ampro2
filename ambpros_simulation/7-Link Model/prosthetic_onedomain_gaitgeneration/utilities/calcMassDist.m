function ret = calcMassDist(weight)
    %Coefficients of Mass Distribution from Winter's Book
    m_foot = 0.0145*weight;
    m_calf = 0.0465*weight;
    m_thigh = 0.1*weight;
    %Torso weight is the remaining body weight
    m_torso = weight - 2*(m_foot+m_calf+m_thigh);


    %Convert from pounds to kg
%     lb_to_kg = 0.45359237;
    lb_to_kg    = 1;

    ret.foot    = m_foot*lb_to_kg;
    ret.calf    = m_calf*lb_to_kg;
    ret.thigh   = m_thigh*lb_to_kg; 
    ret.torso   = m_torso*lb_to_kg;

end