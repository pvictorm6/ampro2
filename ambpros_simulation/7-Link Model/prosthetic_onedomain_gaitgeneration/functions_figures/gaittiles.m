function gaittiles(robotwalk)
    data1       = robotwalk.data{1};
    data2       = robotwalk.data{2};
    data3       = robotwalk.data{3};
    
    ndof        = 9;
    njoints     = length(jpos_mat_figure(data1(2:ndof+1,1)));
    left_range  = [1:floor(njoints/2)+2];
    right_range = [floor(njoints/2)+2:njoints];
   
    k1          = size(data1, 2);
    k2          = size(data2, 2);
    k3          = size(data3, 2);
    LW          = 1;
    h           = figure; clf;
    %%set the position of axis
    set(h,'position',[100 100 1800 200]);
    axis([-0.5 9 -0.1 1.2])
    axis manual
    
    xterrain = linspace(-0.5,9,1000);
    zterrain = linspace(0,-0.2,1000);
    h2 = area(xterrain, 0*zterrain,-0.2);
    set(h2,'FaceColor',[0 150 50]/255)
    %%clear x,y ticks
    axopts = {'XTick', [], 'YTick', []}; 
    axopts2={};
    set(gca, axopts{:}, axopts2{:});

    node1   = 8;  
    inc1    = 0.7;
    node2   = 8;  
    inc2    = 0;
    node3   = 14;  
    inc3    = 0;

    px  = 0.4; 
    py  = 0;
    
    
    %% plot domain 1
    xp1 = zeros(k1,njoints);
    yp1 = zeros(k1,13);
    for i = 1:k1
        
        pos     = jpos_mat_figure(data1(2:ndof+1,i))+...
                  [px*ones(1,njoints); py*ones(1,njoints)];
        xp1(i,:) = pos(1,:);
        yp1(i,:) = pos(2,:);
    end
    
    hold on
    h11 = plot(xp1(1,left_range)+inc1,yp1(1,left_range),'k','LineWidth',LW);
    set(gca, axopts{:}, axopts2{:});
    hold on
    h12 = plot(xp1(1,right_range)+inc1,yp1(1,right_range),'r','LineWidth',LW);
    set(gca, axopts{:}, axopts2{:});
    hold on
    inc1 = inc1 + 0.7;

    for i = 1:k1
        if(mod(i,node1)==0)
            hold on
            h13 = plot(xp1(i,left_range)+inc1,yp1(i,left_range),'k','LineWidth',LW);
            set(gca, axopts{:}, axopts2{:});
            hold on
            h14 = plot(xp1(i,right_range)+inc1,yp1(i,right_range),'r','LineWidth',LW);
            set(gca, axopts{:}, axopts2{:});
            hold on
            inc1 = inc1 + 0.7;
        end
    end

    
    %% plot domain 2
    xp2 = zeros(k2,njoints);
    yp2 = zeros(k2,13);
    for i = 1:k1
        
        pos     = jpos_mat_figure(data2(2:ndof+1,i))+...
                  [px*ones(1,njoints); py*ones(1,njoints)];
        xp2(i,:) = pos(1,:);
        yp2(i,:) = pos(2,:);
    end

    for i = 1:k2
        if(mod(i,node2)==0)
            hold on
            h21 = plot(xp2(i,left_range)+inc2 + inc1,yp2(i,left_range),'k','LineWidth',LW);
            set(gca, axopts{:}, axopts2{:});
            hold on
            h22 = plot(xp2(i,right_range)+inc2 + inc1,yp2(i,right_range),'r','LineWidth',LW);
            set(gca, axopts{:}, axopts2{:});
            hold on
            inc2 = inc2 + 0.7;
        end
    end

    %% plot domain 3
    xp3 = zeros(k3,njoints);
    yp3 = zeros(k3,13);
    for i = 1:k3
        
        pos     = jpos_mat_figure(data3(2:ndof+1,i))+...
                  [px*ones(1,njoints); py*ones(1,njoints)];
        xp3(i,:) = pos(1,:);
        yp3(i,:) = pos(2,:);
    end

    for i = 1:k3
        if(mod(i,node3)==0)
            hold on
            h31 = plot(xp3(i,left_range)+inc3 + inc2 + inc1,yp3(i,left_range),'k','LineWidth',LW);
            set(gca, axopts{:}, axopts2{:});
            hold on
            h32 = plot(xp3(i,right_range)+inc3 + inc2 + inc1,yp3(i,right_range),'r','LineWidth',LW);
            set(gca, axopts{:}, axopts2{:});
            hold on
            inc3 = inc3 + 0.7;
        end
    end
    %%plot the last step tiles
    hold on
    h33 = plot(xp3(end,left_range)+inc3 + inc2 + inc1,yp3(end,left_range),'k','LineWidth',LW);
    set(gca, axopts{:}, axopts2{:});
    hold on
    h34 = plot(xp3(end,right_range)+inc3 + inc2 + inc1,yp3(end,right_range),'r','LineWidth',LW);
    set(gca, axopts{:}, axopts2{:});
    hold on



%     set(h1,'MarkerSize',4,'LineWidth',2)
%     set(h2,'MarkerSize',4,'LineWidth',2)
%     set(h3,'MarkerSize',4,'LineWidth',2)
%     set(h4,'MarkerSize',4,'LineWidth',2)
%     set(h5,'MarkerSize',4,'LineWidth',2)
%     set(h6,'MarkerSize',4,'LineWidth',2)
end