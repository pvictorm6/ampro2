%% fit the forward hip position with linear pattern
function ret = fit_hippos(t,hip_pos)

    ret = polyfit(t,hip_pos,1);
 
end