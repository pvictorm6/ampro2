function ret = f1_vector_under(t,x, a, ep,gamma)
global tp tp0 yact ydes p U Pow u h kneemin qdotmax condnum umax Voltage
global typeofactuation

typeofcontrol=2; %1 indicates input output linearisation and 2 indicates PD control based on thetad
%%%%%Choose the type of actuation
%%% 1 = full actuation
%%% 2 = under actuation
% typeofactuation = 2;

ndof = length(x)/2;
dx = x(ndof+1:end);

M    = D_mat(x);
C    = C_mat(x);
G    = G_vec(x);

%%%Control Fields

if typeofactuation == 1
    
    vf    = [dx; M \ (-C*dx - G)];
    B_IO  = eye(ndof);
    gf    = [zeros(size(B_IO)); M \ B_IO];
    
elseif typeofactuation == 2
    
    vf    = [dx; M \ (-C*dx - G)];
    B_IO  = eye(ndof);
    B_IO(1,1)  = 0;
    gf    = [zeros(size(B_IO)); M \ B_IO];
    
end

%%%Outputs
y_a1     = ya1_sca(x, p, a);
y_a2     = ya2_vec(x, p, a);
y_d1     = yd1_sca(x, p,  a);
y_d2     = yd2_vec(x, p,  a);

%%%Outputs
y1 = y_a1 - y_d1;
y2 = y_a2 - y_d2;


y_a = [y_a1; y_a2];
y_d = [y_d1; y_d2];

%%%Jacobians of Outputs
Dy_a1    = Dya1_mat(x, p, a);
Dy_d1    = Dyd1_mat(x, p, a);
Dy_a2    = Dya2_mat(x, p, a);
Dy_d2    = Dyd2_mat(x, p, a);

Dy_1 = Dy_a1 - Dy_d1;
Dy_2 = Dy_a2 - Dy_d2;

%%%Lie Derivatives
Lgy1 = Dy_1*gf;
Lgy2 = Dy_2*gf;
Lfy1 = Dy_1*vf;
Lfy2 = Dy_2*vf;

% Dy_a1*gf;
% gamma = gamma_sca(x);
% Dgamma = Dgamma_mat(x);
% Dgamma*gf

%%%Second Order Jacobians

DLfy_a2  = DLfya2_mat(x, p, a);
DLfy_d2  = DLfyd2_mat(x, p, a);

DLfy2 = DLfy_a2-DLfy_d2;

%%%Second Lie Derivatives

LfLfy2 = DLfy2*vf;
LgLfy2 = DLfy2*gf;

tp = [tp, t+tp0];
yact = [yact, y_a];
ydes = [ydes, y_d];
height = h_sca(x);
h = [h, height];
kneemincalc = min(x(2,:),x(5,:));
kneemin = min(kneemin,kneemincalc);
qdotmaxcalc = max(max(abs(dx)));
qdotmax = max(qdotmax,qdotmaxcalc);

% Vector field

if typeofactuation == 1
    
    A = [Lgy1; LgLfy2];
    
    test_nan = max(max(isnan(A)));
    test_inf = max(max(isinf(A)));
    test = max(test_nan,test_inf);
    
    if test == 1
        condnum = 1e10
    else
        condnum = max(condnum,cond(A));
    end
    
    u = -A \ ([0; LfLfy2]+[Lfy1; 2*ep*Lfy2]+ [ep*y1; ep^2*y2]);
    umax = max(umax,max(abs(u)));
    
elseif typeofactuation == 2
    
    A = LgLfy2;
    
    test_nan = max(max(isnan(A)));
    test_inf = max(max(isinf(A)));
    test = max(test_nan,test_inf);
    
    if test == 1
        condnum = 1e10
    else
        condnum = max(condnum,cond(A));
    end
    
    u = -A \ (LfLfy2+ 2*ep*Lfy2 + ep^2*y2);
    umax = max(umax,max(abs(u)));
    
end


Pow = [Pow, u.*dx];
if typeofcontrol==1
    ret=vf+gf*u;
else 
    Vdo=-ep*y2;
    Kphi=3.344;Kmg=3.3465;Ra=6;
    Tauin=Kphi*([0;Vdo(2);Vdo(4);Vdo(1);Vdo(3)]-Kmg*x(ndof+1:end))/Ra;
 
      ret = vf + gf *Tauin;
    
U = [U, Tauin];
Voltage=[Voltage,Vdo];
end
end
