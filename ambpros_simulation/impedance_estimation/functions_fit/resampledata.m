function ave_data = resampledata(N,data)
        D = length(data);
        datapad = [repmat(data(1), 1, D), data', repmat(data(end), 1, D)];
        datatemp = resample(datapad,N,D); % Now resample it
        ave_data = datatemp(N+1:end-N);
        ave_data(1) = data(1);
        ave_data(end) = data(end);

end