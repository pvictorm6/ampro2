 function a_fit = get_afit(humandata, robotwalk)
    a_fit = zeros(robotwalk.nOutputs,robotwalk.nParameter);
    
    %% parse the data
    mean_time   = humandata.mean_time;
    mean_hippos = humandata.mean_hippos;
    mean_sknee  = humandata.mean_sknee;
    mean_nsknee = humandata.mean_nsknee;
    mean_nsslope= humandata.mean_nsslope;
    mean_torso  = humandata.mean_torso;
    mean_nsfoot = humandata.mean_nsfoot;
    
    %% fit the data
    % fit the hip
    a_fit(1,1:2) = fit_hippos(mean_time,mean_hippos);
    
    %fit the stance knee
    a_fit(2,:) =...
        fit_r2outputs(mean_time,mean_sknee,OutputsType.SKNEE, robotwalk);
    
    %fit non stance knee 
    a_fit(3,:) =...
        fit_r2outputs(mean_time,mean_nsknee,OutputsType.NSKNEE, robotwalk);
    
    %fit the ns_slope
    a_fit(4,:) =...
        fit_r2outputs(mean_time,mean_nsslope,OutputsType.NSSLOPE,robotwalk);
    
    %fit the torso
    a_fit(5,:) =...
        fit_r2outputs(mean_time,mean_torso,OutputsType.TORSO, robotwalk);
    
    %fit the ns_foot angle
    a_fit(6,:) =...
        fit_r2outputs(mean_time,mean_nsfoot,OutputsType.NSFOOT, robotwalk);
    
    %% plot the fit data
    optimization_plotoutputs(humandata,a_fit,a_fit);
    
    %% save a_fit
    cd('./human_data')
    save('a_fit','a_fit')
    disp('a_fit has been successfully saved !')
    cd('../')
end