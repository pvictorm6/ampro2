#ifndef CONTROL_UTILITIES_GAIN_SCHEDULE_HPP_
#define CONTROL_UTILITIES_GAIN_SCHEDULE_HPP_

namespace gait_schedule
{

// Recude the effect of the velocity jump at the beginning of each step by increasing the P gain and decreasing
// D gain right after the impact. An exponential decay function is used

inline double gain_schedule(double init_gain, double final_gain, double t, double decay_rate)
{
    return final_gain+ exp(-t*decay_rate)*(init_gain-final_gain);
}

}

#endif
