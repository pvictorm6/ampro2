addpath('./build_torso','./buildopt_torso', './functions_figures', 'classdef')
addpath('./wrappers')
addpath('./utilities');

robotwalk   = amberwalk_configuration;

ep      = robotwalk.ep;
a       = robotwalk.a_opt{1};
a       = a_expand(a);
ndof    = robotwalk.nDof;
nbasedof= robotwalk.nBaseDof;
nexdof  = nbasedof + ndof;

v_d     = a(1, 1);

x_plus  = robotwalk.ic{1};
x_minus = robotwalk.x_opt{1};
ic      = x_plus;
%> @note Using variables from cpp_save

p_minus = deltaphip_sca(x_minus);

p0      = deltaphip_sca(ic);

p_plus  = deltaphip_sca(x_plus);


zm      = p_minus;
zp      = p_plus;
z0      = p0;

% zs = linspace(zp, zm);
% count = length(zs);
t0      = (z0 - zp) / a(1, 1);
tf      = (zm - zp) / a(1, 1);
ts      = linspace(t0, tf);
count   = length(ts);

qs      = zeros(ndof, count);
dqs     = zeros(ndof, count);
yd2s    = zeros(ndof - 1, count);
dyd2s   = zeros(ndof - 1, count);
ps      = zeros(1, count);
dps     = zeros(1, count);

%%
for i = 1:count
    t = ts(i);
    
    dq_plus = x_plus(nexdof + 1:end);
    dp_plus = deltaphip_dot_mat(x_plus) * dq_plus(4:end);
    z1      = v_d * t + p_plus + 1 / ep * (1 - exp(-ep * t)) * (dp_plus - v_d);
    dz1     = v_d + exp(-ep * t) * (dp_plus - v_d);
    
    yd2     = yt_d2(t, p_plus, a);
    dyd2    = dyt_d2(t, p_plus, a);
    q       = Phi_mat() \ [z1; yd2];
    dq      = Phi_mat() \ [dz1; dyd2];
    
    ps(i)   = z1;
    dps(i)  = dz1;
    
    qs(:, i) = q;
    dqs(:, i) = dq;
    yd2s(:, i) = yd2;
    dyd2s(:, i) = dyd2;
end

%% Ensure final position corresponds to fixed point
xf      = [0; 0 ;0;  qs(:, end); 0; 0; 0; dqs(:, end)];
xf_plus = R1_map(xf, 1);
diff    = xf - x_minus;

err     = max(abs(diff));
tol     = 1e-10;
if err > tol
    [xf_plus, x_plus]
    err
    warning('Condition is not on HZD surface!');
end

z = [ps; dps];
%% Generate test file
test = struct();
test.in = struct();
test.in.ts = ts;
test.in.Phi = inv(Phi_mat());
test.in.p_plus = p_plus;
test.out = struct();
test.out.z = z';
test.out.qs = qs';
test.out.dqs = dqs';
test.out.yd2s = yd2s';
test.out.dyd2s = dyd2s';

addpath('./utilities/yaml');
yaml_init();
dir = '~/catkin_ws/src/amber_prosthetic/ambpros_trajectory/share';
yaml_write_file([dir, '/test_hzd_recon.yaml'], test);

%% HZD Reconstruction
figure(20);
clf();

subplot(2, 1, 1);
plot(ts, qs);
legend({'sa', 'sk', 'sh', 'nsh', 'nsk','nsa'});

subplot(2, 1, 2);
plot(ts, dqs);
title('velocity');

%% Integration Consistency Test
% ps_int = cumtrapz(ts, dps) + repmat(ps(1), 1, count);

figure(21);
% Integration velocities (w.r.t. time) and compare against computed position
qs_int = cumtrapz(ts, dqs, 2) + repmat(qs(:, 1), 1, count);
clf();
hold('on');
plot(ts, qs, '-', 'LineWidth', 2);
plot(ts, qs_int, ':');
diff = (qs - qs_int);
err = max(abs(diff(:)));
tol = 1e-3;
if err > tol
    warning('Position and velocity are inconsistent');
end
plot(ts, diff, '--');

%% Position of the hip
figure(22);
clf();
subplot(2, 1, 1);
plot(ts, ps);
ylabel('p');
subplot(2, 1, 2);
plot(ts, dps);
ylabel('dp');

% %% Left / Right, Checking for Contiuity
% % Don't care about stance ankle... Right?
% RL = [0  1  0  0  0; % Left Knee
%       0  0  1  0  0; % Left Hip
%       0  0  0  1  0; % Right Hip
%       0  0  0  0  1;]; % Right Knee
% 
% RR = [0  0  0  0  1; % Left Knee
%       0  0  0  1  0; % Left Hip
%       0  0  1  0  0; % Right Hip
%       0  1  0  0  0;]; % Rignt Knee
% 
% qLs = RL * qs;
% dqLs = RL * dqs;
% qRs = RR * qs;
% dqRs = RR * dqs;
% 
% qLRs = [qLs, qRs];
% dqLRs = [dqLs, dqRs];
% tLRs = [ts, tf + ts];
% 
% figure(23);
% clf();
% subplot(2, 1, 1);
% plot(tLRs, qLRs);
% legend({'lk', 'lh', 'rh', 'rk'});
% ylabel('Position');
% 
% subplot(2, 1, 2);
% plot(tLRs, dqLRs);
% legend({'lk', 'lh', 'rh', 'rk'});
% ylabel('Position');

%% Aniplot
do_aniplot = true;
if do_aniplot
    figure(2);
    clf;
    ramp = plot([-1 10], [0 0], 'b:');
    hold on;
    legs = plot([0 0], 'k');
    hold off;
    axis([-1 5 -1 1.2]);%

    px = 0;
    py = 0;

    for i = 1:count
        % position
        q = qs(:, i);
        pos = jpos_mat([0;0;0;q]);
        phipz = pos(1,3);
        set(legs, 'XData', pos(1,:), 'YData', pos(2,:), ...
            'erasemode', 'normal');
        axis equal;
        hold on;
        drawnow;
    end
end
