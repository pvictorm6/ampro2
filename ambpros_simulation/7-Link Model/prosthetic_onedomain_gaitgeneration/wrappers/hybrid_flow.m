%% data = hybrid_flow(qi, di, num_execs)
%
%  Description:
%   Simulate flow of a hybrid system.
%
%  Operational Description:
%   Defines a state variable that keeps track of the domain in which the
%   system is operating, i.e. the discrete state.  Defines the guards,
%   reset maps and constraints for each discrete state.  Constraints are
%   stored in the form of A' * lambda.  Defines a data structure:  The
%   integral_curves member contains the solution to the different equation
%   for the interval listed in the hybrid_interval member.  Stores the 
%   discrete state variable 'current_state' as a member in the 'robot'
%   struct.  Then loops for 'num_execs' executions.  Each time through the
%   loop, stores the guards, reset maps and constraints associated with the
%   discrete state in robot.system and passes this to the hybrid_step()
%   function, which evaluates the ODE until the system hits a guard.
%   hybrid_step() returns the results of integration along with the next
%   discrete state of the system.  Then returns to the beginning of the
%   loop.
%
%  Arguments:
%   qi        - Initial condition of the continuous state.
%   di        - The initial discrete state.
%   num_execs - Specify the number of executions of the hybrid system.
%
%  Return Values:
%   data      - Struct with fields 'hybrid_interval' and 'integral_curves'
%               which specify the solutions to the various discrete states
%               and the time intervals over which these solutions are
%               valid.
function robotwalk = hybrid_flow(logObj,robotwalk)
    %% Check if the number of executions is valid.
    num_execs = robotwalk.nSteps;
    if round(num_execs) ~= num_execs || num_execs < 1
        ME = MException('HybridSystem:InvalidExecutionNumber', ...
            'Number of executions must be a positive integer.');
        throw(ME);
    end
    
    tp0  = 0;
    %% Go through num_execs executions.  There is one reset per execution
    for i = 1 : num_execs

        %% Do one execution, passing the associated guards, maps, and
        % constraints.  qi is the initial condition.  robot.system contains the
        % guards, maps and constraints for the particular domain.
        execution    = hybrid_step(tp0,logObj,robotwalk);
       %% Save the data from the execution, adding the end time of the
        % previous execution to the times returned.  Also construct a two-
        % element vector containing the interval over which this execution
        % takes place
        T                    = tp0+execution.time;
        data.time            = T;
        data.integral_curves = execution.integral_curve;
        robotwalk.data{i,1}  = [data.time;data.integral_curves];
       
        robotwalk.dr                 = execution.next_di;
        robotwalk.ic{robotwalk.dr,1} = execution.next_qi;
%        robotwalk.p                  = deltaphip_sca(robotwalk.ic{robotwalk.dr,1});
      %% Save the total time elapsed.  Also save the position of the system
       % as the initial condition for the next step.
        tp0 = tp0 + execution.time(end);
    end
end


