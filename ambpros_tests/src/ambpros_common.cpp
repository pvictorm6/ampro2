#include "ros/ros.h"
#include "ambpros_common.hpp"
#include <yaml_eigen_utilities/yaml_eigen_utilities.hpp>
#include <ambpros_trajectory/ambpros_trajectory.hpp>

using namespace std; 
using namespace Eigen;

AmbprosCommon::AmbprosCommon()
{
  ros::param::get("/ambpros/tests/control_method", this->control_method);
  ros::param::get("/ambpros/tests/track_mode", this->track_mode);
  ros::param::get("/ambpros/tests/number_drivers", this->number_drivers);

  control_type_flag = 3; /* torque */
  can_rate = 1000;
  PI_MATH  = 3.1415926;
  MOTOR_REDUCTION = 160;
  TORQUE_SENS = 0.0229;  /*0.0621 */
  init_flag = 1;
  driver_ID[0] = 127;
  driver_ID[1] = 126;
  running = false;

  init_pos = VectorXd::Zero(number_drivers);
  control_signal = VectorXd::Zero(number_drivers);
  pos_ref = VectorXd::Zero(number_drivers);
  vel_ref = VectorXd::Zero(number_drivers);
  tor_ref = VectorXd::Zero(number_drivers);

  pos_feed = VectorXd::Zero(number_drivers);
  vel_feed = VectorXd::Zero(number_drivers);
  tor_feed = VectorXd::Zero(number_drivers);

  for(int i = 0; i < number_drivers; i++){
    control_output[i] = 0;
  }

  for(int i = 0; i < 6; i++){
    feedback_container[i] =0;
  }
}

/*Make sure the joint angle readings will be in the range of [-180, 180]*/
double AmbprosCommon::anglelimiter(float angle)
{
  double limited_angle = fmod(angle, 360);

  if(limited_angle > 180){
   limited_angle = 180 - limited_angle;
  }else if(limited_angle < -180){
    limited_angle = -180 - limited_angle;
  }
  return limited_angle;
}

void AmbprosCommon::feedback_setup()
{
    for(int i=0; i < number_drivers; i++)
    {
        pos_feed(i) = anglelimiter(feedback_container[3*i+0]) * PI_MATH / 180.0; //conver from degree to rad
        vel_feed(i) = feedback_container[3*i+1] * PI_MATH / 180.0; //rad/s
        tor_feed(i) = feedback_container[3*i+2]*TORQUE_SENS*MOTOR_REDUCTION;  // convert from acutal motor side current to acutal joint side torque
    }
    // reverse ankle
    pos_feed(0) = -pos_feed(0);
    vel_feed(0) = -vel_feed(0);
    tor_feed(0) = -tor_feed(0);
    //     // reverse ankle
    // pos_feed(1) = -pos_feed(1);
    // vel_feed(1) = -vel_feed(1);
    // tor_feed(1) = -tor_feed(1);
}

void AmbprosCommon::command_setup(AmbprosControl &AmbprosController)
{
    /* Hard Bounds for joint side torque and motor side current */
    for(int i = 0; i < number_drivers; i++){
      control_signal(i) = (control_signal(i) > AmbprosController.TORQUE_BOUND ? AmbprosController.TORQUE_BOUND : control_signal(i));
      control_signal(i) = (control_signal(i) < -AmbprosController.TORQUE_BOUND ? -AmbprosController.TORQUE_BOUND : control_signal(i));

      control_output[i] = (control_signal(i) / MOTOR_REDUCTION / TORQUE_SENS);  //convert the joint torque coomand to the motor current command
      control_output[i] = (control_output[i] > AmbprosController.CURRENT_BOUND ? AmbprosController.CURRENT_BOUND : control_output[i]);
      control_output[i] = (control_output[i] < -AmbprosController.CURRENT_BOUND ? -AmbprosController.CURRENT_BOUND : control_output[i]);
    }
    // reverse ankle command
    control_signal(0) = -control_signal(0);
    control_output[0] = -control_output[0];
    // control_signal(1) = -control_signal(1);
    // control_output[1] = -control_output[1];
}

void AmbprosCommon::imudata_setup(VectorXd &imu_data, ambcap_pros &imu){
    //IMU positions
    imu_data.head(4) << imu.hLte[2],  // left thigh link which is used to get the left foot angle
                        -imu.hRst[2],  // right knee joint//quat = w,x,y,z
                        -imu.hRfs[2],  // right ankle joint
                        imu.hRfe[2];  // right foot link * not used
    //IMU velocities
    Eigen::VectorXd  vel_links(4);
    vel_links << imu.Lt_vel[1],  // left thigh link
            /*Here, the coordinate is set as: forward (the walking direction) is x, rightside is y and downward is z.*/
                -imu.Rt_vel[1], // right thigh link
                -imu.Rs_vel[1], // right shank link
                imu.Rf_vel[1]*0.0; // right foot link. Set it to zero since we are assuming flat foot walking

    imu_data.tail(4) << vel_links(0),  // left thigh link ** not used
                        vel_links(1) - vel_links(2), // right knee joint  =  right thigh link - right shank link//gyr = x,y,z
                        vel_links(2) - vel_links(3), // right ankle joint =  right shank link - right foot link
                        vel_links(3) - 0.0; // right foot = right foot link - right foot link
}

void AmbprosCommon::message_setup(ambpros_can_driver::control_feedback &feedback_msg, VectorXd &contactforce, VectorXd &imu_data, VectorXd &imped_torque, double tau, bool ishumanswing)
{    // Only send motor command if running flag has been called by server!

    feedback_msg.action_type = 1;
    // set up the ankle
    feedback_msg.torque_actual_ankle    = tor_feed(0);
    feedback_msg.control_signal_ankle   = control_output[0];
    feedback_msg.torque_desired_ankle   = control_signal(0);
    feedback_msg.position_desired_ankle = pos_ref(0);
    feedback_msg.position_actual_ankle  = pos_feed(0);
    feedback_msg.velocity_actual_ankle  = vel_feed(0);
    feedback_msg.velocity_desired_ankle = vel_ref(0);

    // set up the knee
    feedback_msg.torque_actual_knee    = tor_feed(1);
    feedback_msg.control_signal_knee   = control_output[1];
    feedback_msg.torque_desired_knee   = control_signal(1);
    feedback_msg.position_desired_knee = pos_ref(1);
    feedback_msg.position_actual_knee  = pos_feed(1);
    feedback_msg.velocity_actual_knee  = vel_feed(1);
    feedback_msg.velocity_desired_knee = vel_ref(1);

    feedback_msg.tau = tau;
    feedback_msg.ishumanswing       = ishumanswing;

    // set up the contact forces
    feedback_msg.force_toe          = contactforce(0);
    feedback_msg.force_heel         = contactforce(1);

    feedback_msg.torque_imped_ankle = imped_torque(0);
    feedback_msg.torque_imped_knee  = imped_torque(1);

    // set up the imu data
    feedback_msg.imu_position_ankle = imu_data(2);
    feedback_msg.imu_position_knee  = imu_data(1);
    feedback_msg.imu_velocity_ankle = imu_data(6);
    feedback_msg.imu_velocity_knee  = imu_data(5);
}

void AmbprosCommon::print_status(AmbprosControl &AmbprosController, TrajectoryGen &AmbprosTraj)
{
    string controller;
    switch(control_method){
        case 0:
        {
            controller = "PD";
            break;
        }
        case 1:{
            controller = "MIQP";
            break;
        }
        case 2:{
            controller = "Imped";
            break;
        }
        case 3:{
            controller = "SwingImp";
            break;
        }
        default:
        {
            cout << "The chosen control method is not available !!" << endl;
        }
    }

    //control parameters status
    string useimpedance;
    if(AmbprosController.isimpedance)
        useimpedance = "Yes";
    else
        useimpedance = "No";

    //trajectory parameters status
    string usestatebase;
    if(AmbprosTraj.isTimebased)
        usestatebase = "No";
    else
        usestatebase = "Yes";

    string useforcesensor;
    if(AmbprosTraj.isForcesensor)
        useforcesensor = "is used.";
    else
        useforcesensor = "is not used.";

    cout << "Controller is: "<< controller<<". With Impedance: " << useimpedance << ". Tracking is statebased: "<< usestatebase << endl;
    cout << "Algorithm runs at " << AmbprosController.frequency << " Hz." << endl;
    cout << "Swing P gain: " << AmbprosController.pGainSwing << ". Swing D gain: " << AmbprosController.dGainSwing << endl;
    cout << "Stance P gain: " << AmbprosController.pGainStance << ". Swing D gain: " << AmbprosController.dGainStance << endl;
    cout << "Force sensor " << useforcesensor << endl;

}

void AmbprosCommon::get_feedback_fake()
{
    double track_lag = 0;
    for(int i=0; i < number_drivers; i++)
    {
        feedback_container[3*i+0] = anglelimiter(pos_ref(i) + track_lag)* 180 / PI_MATH; //conver from degree to rad
        feedback_container[3*i+1] = (vel_ref(i) + track_lag)* 180 / PI_MATH; //rad/s
        feedback_container[3*i+2] = (tor_ref(i) + track_lag*10)/(TORQUE_SENS*MOTOR_REDUCTION);  // convert from acutal motor side current to acutal joint side torque
    }

}

void AmbprosCommon::set_reference_fake()
{
//    pos_feed = pos_ref;
//    vel_feed = vel_feed;
//    tor_feed = tor_ref;
}

void AmbprosCommon::cushion_timer(int nsec){
    int i;
    for (i=0; i<nsec; i++)
    {
        sleep(1);
        cout << "Time Remaining To Take Off: " << nsec - i  - 1 << endl;
    }
}

void AmbprosCommon::setup_servs(ros::NodeHandle &nh) {
    running_serv_ =  nh.advertiseService("start_ambpros", &AmbprosCommon::running_callback, this);
}

bool AmbprosCommon::running_callback(std_srvs::Empty::Request  &req,
                      std_srvs::Empty::Response &resp) {
    if ( !running ) {
        ROS_INFO("Starting ambpros controllers");
        running = true;
    } else {
        ROS_INFO("Stopping ambpros controllers");
        running = false;
    }
    return true;
}
