%%calc the human data for specific model
load('humandata.mat');
load('models_human.mat');

lc  = modelParameters.Lc;
lt  = modelParameters.Lt;

datalength = 200;
mean_hippos = zeros(1, datalength);
mean_nsslope= zeros(1, datalength);
for i = 1:200
    mean_hippos(1,i) = -(lc+lt)*mean_sankle_angle(i) - lt*mean_sknee_angle(i);
    mean_nsslope(1,i)= -mean_sankle_angle(i) - mean_sknee_angle(i) - ...
        mean_ship_angle(i) + mean_nship_angle(i) + lc/(lc+lt)*mean_nsknee_angle(i);    
end

mean_hippos = mean_hippos - mean_hippos(1);
save mean_hippos mean_hippos
save mean_nsslope mean_nsslope