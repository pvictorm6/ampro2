classdef OutputsType < handle
    enumeration
        SKNEE
        NSKNEE
        NSSLOPE
        TORSO   
        HIPANG  %%hip angle which is \theta_ship - \theta_nship
        SFOOT
        NSFOOT
        HIPPOS
    end
end