%%get the initial guess for fitting

function ret = get_initial(output, robotwalk)
MOTIONTYPE  = robotwalk.motiontype;

if eq(MOTIONTYPE, MotionType.FLATGROUND)
%% initial guess for flatground outptus. Note that, all the initial guesses
%% are for version 1 canoncial function.
    switch output
        case OutputsType.SKNEE
            ret = [-0.1243   10.4883    0.1010    1.9458    0.2179];
        case OutputsType.NSKNEE
            ret = [ -0.4098  -10.0422    0.1223   -0.4122    0.6554];
        case OutputsType.NSSLOPE
            ret = [ -0.0401    7.5452   -0.1907   -1.5505   -0.2827];
        case OutputsType.HIPANG
            ret = [ 0.7676    6.2066    0.7770    1.8497   -0.2671];
        case OutputsType.NSFOOT
            ret = [ 0.4024   10.2720   -0.0422   -0.9500   -0.4753];
        case OutputsType.SFOOT
            ret = [0.4172    0.0000   -3.4003    7.8350   -0.0468];
        case OutputsType.TORSO
            ret = [-0.0156  -10.5060   -0.0093    1.4857    0.1641];
        otherwise
            disp('The input output name is not defined yet');
    end
    if robotwalk.nParameter == 7
        ret = [ret,10,0];
    end
else
%% initial guess for stairclimbing outputs. Note that, all the initial guesses
%% are for version 1 extended canoncial function.
    switch output
        case OutputsType.SKNEE
            ret = [-1.5923    4.9587   -0.8949    2.1147    0.0154   19.1867    0.5831];
        case OutputsType.NSKNEE
            ret = [ 0.5657    5.7575    0.8990    4.3497    0.0155   13.8179    0.3900];
        case OutputsType.NSSLOPE
            ret = [-0.7725    7.1861   -0.0623    1.7299    0.0964   15.5704    1.0069];
            %%needs to be changed. 08/28/2012
        case OutputsType.HIPANG
            ret = [-0.7725    7.1861   -0.0623    1.7299    0.0964   15.5704    1.0069];
        case OutputsType.NSFOOT
            ret = [ 0.7800   10.6421    0.0001   -0.0001   -0.7792   10.6510    0.0010];
            %%needs to be changed. 08/28/2012
        case OutputsType.SFOOT
            ret = [ 0.7800   10.6421    0.0001   -0.0001   -0.7792   10.6510    0.0010];
            %%needs to be changed. 08/28/2012
        case OutputsType.TORSO
            ret = [-0.1859    0.0476   -0.1507    0.4567    0.0249   13.4195   -0.1412];
        otherwise
            disp('The input output name is not defined yet');
    end   
end  
end