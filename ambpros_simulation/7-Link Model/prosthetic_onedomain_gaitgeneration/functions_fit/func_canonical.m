function ret = func_canonical(t, coef)
    %% version 1. the very first version used in the code. it's better to 
    %% use version 1 to be complimentary with other code
    ret = (coef(1).*cos(coef(2).*t)+coef(3).*sin(coef(2).*t))./...
           (exp(coef(4).*t))+coef(5);
       
%     %% version 2. the later version which is inheritted from stair climbing
%     ret = (coef(2).*cos(coef(3).*t)+coef(4).*sin(coef(3).*t))./...
%            (exp(coef(1).*t))+coef(5);
end