/*
 * EigenUtil.h
 *
 *  Created on: Oct 14, 2012
 *      Author: eacousineau
 */

#ifndef AMBER_EIGENUTIL_H_
#define AMBER_EIGENUTIL_H_

#include <vector>
#include <map>

#define AMBER_USE_RBDL

//TODO Redo with RBDL's plugins
//#include <rbdl/rbdl_math.h>

// Hope this doesn't conflict with RBDL

//#include <include/Error.h>

#ifndef eigen_assert
	#define eigen_assert(x) assert_ex(x)
#endif

//#include "Eigen/Dense"

namespace Eigen
{
	// Spatial Vector Algebra
	typedef Matrix<double, 6, 1> Vector6d;
	typedef Matrix<double, 6, 6> Matrix6d;

	typedef Matrix<double, 1, 1> Matrix1d;
	typedef Matrix1d Vector1d;
}

/**
 * Fix problem compiling under Matlab with alignment
 * http://eigen.tuxfamily.org/dox-devel/TopicUnalignedArrayAssert.html
 * This problem is normally encountered in RigidBody.h, with vector's of fixed-size matrices. Occurs when initializing them in the constructor.
 * Make a template for these types?
 *
 * For structures / classes with fixed-size Eigen matrices DIRECTLY IN THEM (not wrapped with STL containers, etc)
 * Place
 *		EIGEN_MAKE_ALIGNED_OPERATOR_NEW
 * in the public part of the struct / class, to make sure it allocates on 128-bit boundary for SSE
 * See http://eigen.tuxfamily.org/dox-devel/TopicStructHavingEigenMembers.html
 *
 * Does this work if the structure is on the stack, in a function call??? Think it is handled with that aligned() keyword
 * But how does aligned(16) correspond to 128-bit alignment for SSE?
 *
 * Guess this doesn't have to done with structs compiled normally, but oh well
 */
#ifdef AMBER_EIGEN_ALIGN

	#include <Eigen/StdVector>

#ifndef AMBER_USE_RBDL
	EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Vector3d);
	EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Matrix3d);
	EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Vector6d);
	EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Matrix6d);
#endif // AMBER_USE_RBDL

	EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Vector4d);
	EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Matrix4d);
	EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Matrix1d);


	namespace std
	{
			template<typename K, int rows, int cols>
		class map<K, Eigen::Matrix<double, rows, cols> >
			: public map<K, Eigen::Matrix<double, rows, cols>, std::less<K>, Eigen::aligned_allocator<std::pair<const K, Eigen::Matrix<double, rows, cols> > > >
		{ };
	}

#endif

#ifndef AMBER_CROSS_LIBRARY
namespace Amber
{

/** See \ref Util.h for info on why these are in Amber and not Amber::Core */

using Eigen::MatrixXd;
using Eigen::VectorXd;

using Eigen::Vector1d;
using Eigen::Matrix1d;

// Basic
using Eigen::Vector3d;
using Eigen::Matrix3d;
// Homogeneous
using Eigen::Vector4d;
using Eigen::Matrix4d;

using Eigen::Vector6d;
using Eigen::Matrix6d;

	namespace Core
	{

inline Matrix1d matrixScalar(double x)
{
	Matrix1d A;
	A(0, 0) = x;
	return A;
}

// Same as A \ B (mldivide) in Matlab
inline MatrixXd leftDivide(const MatrixXd &A, const MatrixXd &B)
{
	return A.householderQr().solve(B);
}

// Same as A / B (mrdivide) in Matlab
inline MatrixXd rightDivide(const MatrixXd &A, const MatrixXd &B)
{
	// Inefficient, but meh
	return leftDivide(B.transpose(), A.transpose()).transpose();
}

	}
}
#endif //AMBER_HEADER_ONLY

#endif /* EIGENUTIL_H_ */
