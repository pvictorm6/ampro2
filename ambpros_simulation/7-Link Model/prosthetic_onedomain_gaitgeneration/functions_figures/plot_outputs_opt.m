%Hip position constraint for 4 subjects
function plot_outputs_opt

addpath('./dataforfigure')

temp        = load('yact.mat');
outputs_act = temp.yact_best;
temp        = load('ydes.mat');
outputs_des = temp.ydes_best;
temp        = load('time.mat');
time        = temp.time;

legendarray_act = {'\delta v_{hip}^a','\theta_{sk}^a','\theta_{nsk}^a',...
                    '\delta m_{nsl}^a','\theta_{tor}^a','\theta_{nsf}^a'};
legendarray_des = {'\delta v_{hip}^d','\theta_{sk}^d','\theta_{nsk}^d',...
                    '\delta m_{nsl}^d','\theta_{tor}^d','\theta_{nsf}^d'};
colorarray  = {'blue','red',[32 153 41]/255,[80 30 20]/255,'black','m'};

% mark_list_act = {'-','-','-','-','-','-'};
% mark_list_des = {'-o','-o','-o','-o','-o','-o'};

sampleRate  = 1000;
range       = 1400:2000;

time        = resampledata(sampleRate,time(range));
time        = time - time(1); %%offset the time to start from zero

[~, nOutputs]   = size(outputs_act);
y_act           = zeros(nOutputs, sampleRate);  
y_des           = zeros(nOutputs, sampleRate);  
%reduce number of data points for readability
for i = 1: nOutputs
    y_act(i,:) = resampledata(sampleRate, outputs_act(range,i));  
    y_des(i,:) = resampledata(sampleRate, outputs_des(range,i));  
end

ha = zeros(1,nOutputs);
hd = zeros(1,nOutputs);
%%plot the figures
for i = 1  %1:nOutputs
    h               = figure(i);
    mark_size       = 8;

    set(h, 'position', [100 100 1*500 1*350]);

    ha(i) = plot(time,y_act(i,:),'color',colorarray{1});
    hold on
    hd(i) = plot(time,y_des(i,:),'color',colorarray{2});
  
    %%set the marker size and line width
    set(ha(i),'MarkerSize',mark_size,'LineWidth',2);
    set(hd(i),'MarkerSize',mark_size,'LineWidth',2);
    
    xlbound = 0;
    xubound = time(end) + 0.01;
    ylbound = min(min([y_act(i,:);y_des(i,:)])) + 25;
    yubound = max(max([y_act(i,:);y_des(i,:)])) - 2 ;
    axis([xlbound xubound ylbound yubound])
    
    %Font Size
    FS       = 22;

    %Top offset
    top_offset = 0.80;

    xlabel('Scaled Time','Interpreter','LaTex','FontSize',FS);
    ylabel('Angle (rad)','Interpreter','LaTex','FontSize',FS);


    set(gca,'FontSize',FS);
    apc = [0.200000000000000   0.2800000000000   0.775000000000000   0.690000000000000];
    set(gca,'position',apc);

    ah1 = gca;
    l = legend(ah1,ha(i),legendarray_act{i},'Location','SouthOutside',...
              'Box','off','FontSize',FS,'Interpreter','LaTeX');
    p = get(l,'position');
    set(l, 'position', [0.1 p(2)- top_offset .3 p(4)+.06],'Box','off');


    ah2=axes('position',get(gca,'position'), 'visible','off');
    set(ah2,'FontSize',FS);
    l = legend(ah2,hd(i),legendarray_des{i},'Location','SouthOutside',...
        'Box','off','FontSize',FS,'Interpreter','LaTeX');
    p = get(l,'position');
    set(l, 'position', [0.65 p(2)-top_offset .35 p(4)+.06],'Box','off');
end

end