function y=pacre2(x,a)
addpath('./build_torso')
global p

%Control Gain
ep = 10;

%%%compute q5 based on the guard. Since we are solving the sinusoidal
%%%functions, there are multiple solutions. We have to check which one is
%%%correct by repluging it back to the guard equation.
%%%In our situation, we solve \theta_5, which is quite straight forward,
%%%from the guard equation(the non-stance foot height). Therefore, we know 
%%%which solution is right. 
h1 = @(x1,x2,x3,x4)(-1).*acos(cos(x1)+(113/95).*(cos(x1+ ...
                x2)+(-1).*cos(x1+x2+x3+(-1).*x4)))+ x1 + x2+ x3-x4;

% h2= @(x1,x2,x3,x4)(+1).*acos(cos(x1)+(113/95).*(cos(x1+ ...
%                 x2)+(-1).*cos(x1+x2+x3+(-1).*x4)))+ x1 + x2+ x3-x4;;
% 
% xtest=[x(1:4);h1(x(1),x(2),x(3),x(4))];
% if abs(h_sca(xtest))<=0.01
%     x1=h1;
% end
% xtest=[x(1:4);h2(x(1),x(2),x(3),x(4))];
% if abs(h_sca(xtest))<=0.01
%     x1=h2;
% end

x       = [x(1:4);h1(x(1),x(2),x(3),x(4));x(5:end)];
ic      = resetFunc(x);

%%%compute the hip position and hip velocity
p       = deltaphip_sca(ic);


%%%take one step
sol     = ode45(@(t,x) My_f1_vector(t,x,a,ep), [0 4], ic, ...
        odeset('Events', @(t,x)eventfcn(t,x,a), 'MaxStep', 1e-2));
data    = sol.y;
        

y=data([1:4,6:end],end);

