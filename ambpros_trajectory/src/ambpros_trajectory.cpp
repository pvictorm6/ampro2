#include <iostream>
#include <stdlib.h>
#include <cmath>
#include <gtest/gtest.h>
#include "ros/ros.h"

#include <ambpros_trajectory/ambpros_trajectory.hpp>
#include <ambpros_trajectory/geometry_reconstruction.hpp>
#include <control_utilities/limits.hpp>

static bool switchTaubase = true;
static string _preDomain = "standing";
static int forwardIndex = 0;
static int predictScore = 0;
static int predictScore_walk=0;
static int predictScore_stair=0;
static double TauMax_Optimized = 0;
static double _TimeParameter = 0;
static double ANKLE_POSBOUND_LOW = -0.55;
static double ANKLE_POSBOUND_HIGH = 0.5;
static double ANKLE_VELBOUND_LOW = -5;
static double ANKLE_VELBOUND_HIGH = 5;
static double ANKLE_POS_RATELIMITER = 0.1;
static double ANKLE_VEL_RATELIMITER = 1;

static double KNEE_POSBOUND_LOW = 0;
static double KNEE_POSBOUND_HIGH = 1.1;
static double KNEE_VELBOUND_LOW = -5;
static double KNEE_VELBOUND_HIGH = 5;
static double KNEE_POS_RATELIMITER = 0.1;
static double KNEE_VEL_RATELIMITER = 1;

#define PI_MATH 3.1415926
using namespace common;
using boost::filesystem::path;
using namespace std;
using namespace Eigen;

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

TrajectoryGen::TrajectoryGen(){
    /* initialization all the variable */    
    ros::param::get("/ambpros/tests/track_mode", this->track_mode);
    ros::param::get("/ambpros/control/e", this->ep);
    ros::param::get("/ambpros/trajectory/nFullOutputs", this->nFullOutputs);
    ros::param::get("/ambpros/trajectory/nParameters", this->nParameters);
    ros::param::get("/ambpros/trajectory/calflength", this->calflength);
    ros::param::get("/ambpros/trajectory/thighlength", this->thighlength);
    ros::param::get("/ambpros/trajectory/anklelength", this->anklelength);
    ros::param::get("/ambpros/trajectory/slowrate", this->slowrate);
    ros::param::get("/ambpros/trajectory/ndomains", this->ndomains);
    ros::param::get("/ambpros/trajectory/initialdomain", this->initialdomain);
    ros::param::get("/ambpros/trajectory/tauIncrement", this->tauIncrement);

    ros::param::get("/ambpros/trajectory/isTimebased", this->isTimebased);
    ros::param::get("/ambpros/trajectory/isODEapproximation", this->isODEapproximation);
    ros::param::get("/ambpros/trajectory/isVelocityadaptable", this->isVelocityadaptable);
    ros::param::get("/ambpros/trajectory/isForcesensor", this->isForcesensor);
    ros::param::get("/ambpros/trajectory/isGeometry", this->isGeometry);

    // initialize the common variable
    nFullR2Outputs  = nFullOutputs - 1;
    TimeParameter   = 0;

    legState        = 1;
    phaseIndex      = 1;
    isHumanSwing    = true;
    isSwaped        = false;

    forceThreshold  = 150;
    if(isHumanSwing){
        heelcontact = true;
        toecontact  = true;
    }else{
        heelcontact = false;
        toecontact  = false;
    }
    pauseimpedance  = false;

   // initialize the domain specific variable
    domains.push_back(DomainPtr(new Domain("standing")));
    domains.push_back(DomainPtr(new Domain("levelwalk")));
    domains.push_back(DomainPtr(new Domain("stairclimb")));
    domains[0]->loaddata();
    domains[1]->loaddata();
    domains[2]->loaddata();

    isStanding = (initialdomain==0);
    isWalking = (initialdomain==1);
    isStairclimb = (initialdomain==2);
    currDomain = domains[initialdomain]->getDomainData();
    setDomainData(currDomain);
    cout << "Current domain is " << currDomain.name << endl;

    // initialize the neural network model
    nnmodels.push_back(nnPtr(new neuralNetwork("stand2walk")));
    nnmodels.push_back(nnPtr(new neuralNetwork("walk2stand")));
    nnmodels.push_back(nnPtr(new neuralNetwork("stair2stand")));
    nnmodels.push_back(nnPtr(new neuralNetwork("stand2walkstair")));
    nnmodels.push_back(nnPtr(new neuralNetwork("walk2stand_human")));
    nnmodels.push_back(nnPtr(new neuralNetwork("stair2stand_human")));
    gmm_models = GmmPtr(new gmm_classifier());

    connecting_spline = SplinePtr(new SplineSupport(3, nFullR2Outputs, 0.01, 0, 0.2));
    connecting_spline->addInequalityConstraint(3);

    isUsingSpline = false;
    isSplineBuilt = false;
}

// set the domain data from the struct DomainData
void TrajectoryGen::setDomainData(DomainData domaindata)
{
    a = domaindata.a;
    invPhi = domaindata.invPhi;
    desiredVhip = slowrate*domaindata.a(0, 0);
    initialPhip = domaindata.a(0, 1);
     //for one-domain flat foot walking
    phip_end = domaindata.a(0, 2);
    initialVhip = domaindata.a(0, 3);

    impedswitch_stance = domaindata.impedswitch_stance;
    impedswitch_swing = domaindata.impedswitch_swing;

    if(domaindata.name.compare("standing") != 0)
    {
         Tau_max = (phip_end - initialPhip)/desiredVhip;
         TauMax_Optimized= Tau_max;
    }else
    {
         Tau_max = 0;
         TauMax_Optimized= Tau_max;
    }
}

//time based phip and vhip
double TrajectoryGen::compute_phip(double t)
{
    return initialPhip+t*desiredVhip + ((1-exp(-t*ep))*initialVhip+(-1+exp(-t*ep))*desiredVhip)/ep;
}

double TrajectoryGen::compute_vhip(double t)
{
    return desiredVhip+ exp(-t*ep)*(initialVhip-desiredVhip);
}

//state based phip and vhip
double TrajectoryGen::compute_phip(VectorXd &qState)
{
    double phip;
    phip = -qState(0)*(anklelength + calflength + thighlength) - qState(1)*(calflength + thighlength) - qState(2)*thighlength;
    //phip = control_utilities::clamp(phip, initialPhip, phip_end);
    return phip;
}

double TrajectoryGen::compute_vhip(VectorXd &dqState)
{
    double vhip;
    vhip = -dqState(0)*(anklelength + calflength + thighlength) - dqState(1)*(calflength + thighlength) - dqState(2)*thighlength;
    //vhip = control_utilities::clamp(vhip, -a(0,0), 1.5*a(0,0));
    return vhip;
}
//ydesire and dydesire
void TrajectoryGen::compute_y2d(VectorXd &y2d,  double t)
{
    double a1, a2, a3, a4, a5, a6, a7;
    if (nParameters==7)
    {
        for(int j=1; j<nFullOutputs; j++)
        {
        a1 = a(j, 0);  a2 = a(j,1);   a3 = a(j,2);
        a4 = a(j, 3);  a5 = a(j,4);   a6 = a(j,5);   a7 = a(j,6);

         y2d(j-1) = exp(-a4*t) * ( a1*cos(a2*t) + a3 * sin(a2*t) )
                    + a5 * cos( a6*t )
                    + (2 * a4 * a5 * a6) / ( pow(a4,2) + pow(a2,2) - pow(a6,2) ) * sin( a6*t )
                    + a7;
        }
    }else
    {
        for(int j=1; j<nFullOutputs; j++)
        {   
            a1 = a(j, 0);  a2 = a(j,1);   a3 = a(j,2);
            a4 = a(j, 3);  a5 = a(j,4);
            y2d(j-1) = exp(-a4*t) * (a1*cos(a2*t) + a3 * sin(a2*t)) + a5;
        }

    }
}

void TrajectoryGen::compute_y2dDot(VectorXd &y2dDot, double vhip, double t)
{
    // double t = TimeParameter;
    double a1, a2, a3, a4, a5, a6, a7;
    if (nParameters==7){
        for(int j=1; j<nFullOutputs; j++)
        {
            a1 = a(j, 0);  a2 = a(j,1);   a3 = a(j,2);
            a4 = a(j, 3);  a5 = a(j,4);   a6 = a(j,5);   a7 = a(j,6);

            y2dDot(j-1) =  exp(-a4*t) * ( a2*a3*cos(a2*t) - a1*a2*sin(a2*t) )
                            - a4 * exp(-a4*t) * (a1*cos(a2*t) + a3*sin(a2*t))
                            - a5 * a6 * sin(a6*t)
                            + (2 * a4 * a5 * pow(a6,2) *cos(a6*t)) / ( pow(a2,2) + pow(a4,2) - pow(a6,2) );
            if(!isTimebased)
            {
                y2dDot[j-1] *= ( vhip / a(0,0) );
            }
        }
    }else
    {
        for(int j=1; j<nFullOutputs; j++)
        {   
            a1 = a(j, 0);  a2 = a(j,1);   a3 = a(j,2);
            a4 = a(j, 3);  a5 = a(j,4);
            y2dDot(j-1) = exp(-a4*t) *( a2 * a3*cos(a2*t)- a1* a2*sin(a2*t) ) - a4 * exp(-a4*t) *
                       (a1 * cos(a2*t) + a3 * sin(a2*t));
            if(!isTimebased){
                y2dDot[j-1] *= ( vhip / a(0,0) );  //noted
            }
        }
    }
}

//sinusoidal ydesire
void TrajectoryGen::compute_sine(double &y2, double &y2d)
{
    y2  = sin(2*PI_MATH/Tau_max*TimeParameter);
    y2d = 2*PI_MATH/Tau_max*cos(2*PI_MATH/Tau_max*TimeParameter);
}

//time based tau
double TrajectoryGen::updateTimeParameters(double freq)
{
    if(isSwaped){
        _TimeParameter = 0;
    }else
    {
        _TimeParameter += 1.0/freq;
        _TimeParameter = control_utilities::clamp(_TimeParameter, 0, Tau_max);
    }
    return _TimeParameter;
}

//Get the tau by using the IMU sensor of the feedback joint angles
double TrajectoryGen::updateTimeParameters(VectorXd &qstance)
{
    double phip_curr;
    phip_curr = compute_phip(qstance);
    _TimeParameter = (phip_curr - initialPhip) / desiredVhip;
    // add clamp to tau
    _TimeParameter = control_utilities::clamp(_TimeParameter, 0, Tau_max);
    return tauIncrement + _TimeParameter;
}

void TrajectoryGen::checkStandToWalk(VectorXd &dataset, double phip_curr)
{
    int status_predict = nnmodels[0]->predict(dataset);
    int StandToWalkThreshold = 20;

    if(phip_curr > 0.05 ){
        predictScore +=status_predict;
    }
    else{
        predictScore = 0;
    }
    // cout << "STAND TO WALK:: Current Prediction Score " << predictScore << endl;
    //     cout << "STAND TO WALK:: Hip position " << phip_curr << endl;

    if(predictScore > StandToWalkThreshold && isStanding)
    {
        isStanding = false;
        isWalking = true;
        predictScore = 0;
        _preDomain = "standing";
        ROS_WARN("SWITCH TO WALK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");

        if(isWalking){
            currDomain = domains[1]->getDomainData();
            setDomainData(currDomain);
            _preDomain = "levelwalk";
            isHumanSwing = true;
            //isUsingSpline = true;
            //connecting_spline->tSwitch = TimeParameter;
            cout << "Current domain is " << currDomain.name << endl;
        }
        if(isStairclimb){
            currDomain = domains[2]->getDomainData();
            setDomainData(currDomain);
            _preDomain = "stairclimb";
            isHumanSwing = true;
            cout << "Current domain is " << currDomain.name << endl;
        }
    }
}

void TrajectoryGen::checkStandToWalkStair(VectorXd &dataset, double phip_curr)
{
    int status_predict = nnmodels[3]->predict(dataset);
    int StandToMoveThreshold = 24;
    int WalkThreshold   = 40;
    int StairThreshold  = 40;

    if(phip_curr > 0.05 ){
        predictScore +=status_predict;
        if(status_predict > 0)
            predictScore_walk+=1;
        if(status_predict < 0)
            predictScore_stair+=1;
    }
    else{
        predictScore = 0;
        predictScore_walk=0;
        predictScore_stair=0;
    }
//    cout << "Current Prediction Score ALL:" << predictScore << " Stair:  " <<predictScore_stair << " Walk:"<< predictScore_walk<< endl;

    if(abs(predictScore) > StandToMoveThreshold && isStanding && (predictScore_walk> WalkThreshold || predictScore_stair>StairThreshold))
    {
        isStanding = false;
        if(predictScore > StandToMoveThreshold && predictScore_walk > WalkThreshold)
        {
            isWalking = true;
            isStairclimb = false;
        }

        if(predictScore < -StandToMoveThreshold && predictScore_stair > StairThreshold)
        {
            isWalking = false;
            isStairclimb = true;
        }

        _preDomain = "standing";

        if(isWalking){
            currDomain = domains[1]->getDomainData();
            setDomainData(currDomain);
            _preDomain = "levelwalk";
            isHumanSwing = true;
            predictScore_walk = 0;
            predictScore_stair = 0;
            predictScore = 0;
            ROS_WARN("SWITCH TO WALK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");
            cout << "Current domain is " << currDomain.name << endl;
        }
        if(isStairclimb){
            currDomain = domains[2]->getDomainData();
            setDomainData(currDomain);
            _preDomain = "stairclimb";
            isHumanSwing = true;
            predictScore_walk = 0;
            predictScore_stair = 0;
            predictScore = 0;
            ROS_WARN("SWITCH TO STAIRCLIMB!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");
            cout << "Current domain is " << currDomain.name << endl;
        }
    }
}


void TrajectoryGen::checkWalkToStand(VectorXd &dataset, double phip_curr)
{
    int status_predict = nnmodels[1]->predict(dataset);
    int WalkToStandThreshold = 50;
    if(phip_curr < 0.05)
    {
        predictScore +=1-status_predict*1;
    }
    else{
        predictScore = 0;
    }
//    cout << "WALK TO STAND:: Current Prediction Score " << predictScore << endl;

    if(predictScore > WalkToStandThreshold && isWalking){
        isStanding = true;
        isWalking = false;
        isStairclimb = false;
        predictScore = 0;
        currDomain = domains[0]->getDomainData();
        setDomainData(currDomain);
        _preDomain = "levelwalk";
        cout << "Current domain is " << currDomain.name << endl;
        ROS_WARN("SWITCH TO STAND!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");
    }
}

void TrajectoryGen::checkStairToStand(VectorXd &dataset, double phip_curr)
{
    int status_predict = nnmodels[2]->predict(dataset);
    int StairToStandThreshold = 60;
    if(phip_curr < 0.05)
    {
        predictScore +=1-status_predict*1;
    }
    else{
        predictScore = 0;
    }
//    ROS_WARN("STANDING DETECTED");
//    cout << "STAIR TO STAND:: Current Prediction Score " << predictScore << endl;

    if(predictScore > StairToStandThreshold && isStairclimb){
        isStanding = true;
        isWalking = false;
        isStairclimb = false;
        predictScore = 0;
        currDomain = domains[0]->getDomainData();
        setDomainData(currDomain);
        _preDomain = "stairclimb";
        cout << "Current domain is " << currDomain.name << endl;
        ROS_WARN("SWITCH TO STAND!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");
    }
}

void TrajectoryGen::checkWalkToStand_imu(VectorXd &dataset, double phip_curr)
{
    int status_predict = nnmodels[4]->predict(dataset);
    int WalkToStandThreshold = 50;
    if(phip_curr < 0.05)
    {
        predictScore +=1-status_predict*1;
    }
    else{
        predictScore = 0;
    }
//    cout << "WALK TO STAND:: Current Prediction Score " << predictScore << endl;

    if(predictScore > WalkToStandThreshold && isWalking){
        isStanding = true;
        isWalking = false;
        isStairclimb = false;
        predictScore = 0;
        currDomain = domains[0]->getDomainData();
        setDomainData(currDomain);
        _preDomain = "levelwalk";
        cout << "Current domain is " << currDomain.name << endl;
        ROS_WARN("SWITCH TO STAND!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");
    }
}

void TrajectoryGen::checkStairToStand_imu(VectorXd &dataset, double phip_curr)
{
    int status_predict = nnmodels[5]->predict(dataset);
    int StairToStandThreshold = 60;
    if(phip_curr < 0.05)
    {
        predictScore +=1-status_predict*1;
    }
    else{
        predictScore = 0;
    }
//    ROS_WARN("STANDING DETECTED");
//    cout << "STAIR TO STAND:: Current Prediction Score " << predictScore << endl;

    if(predictScore > StairToStandThreshold && isStairclimb){
        isStanding = true;
        isWalking = false;
        isStairclimb = false;
        predictScore = 0;
        currDomain = domains[0]->getDomainData();
        setDomainData(currDomain);
        _preDomain = "stairclimb";
        cout << "Current domain is " << currDomain.name << endl;
        ROS_WARN("SWITCH TO STAND!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");
    }
}

void TrajectoryGen::checkSwitchEvent()
{
    if(TimeParameter >= Tau_max && forwardIndex > 20){
        isHumanSwing  = !isHumanSwing;
        isSwaped      = true;
        forwardIndex  = 0;
    }else
    {
        isHumanSwing  = isHumanSwing;
        isSwaped      = false;
        forwardIndex += 1;
    }
}

//Decide whether the prosthetic device is stance leg or swing leg: THIS part of code will be finished when the force sensor is ready */
void TrajectoryGen::checkSwitchEvent(VectorXd &contactForce)
{
    // check the contact condition of the heel and toe
    if(contactForce(0) > forceThreshold)
        heelcontact = true;
    else
        heelcontact = false;

    if(contactForce(1) > forceThreshold)
        toecontact  = true;
    else
        toecontact  = false;

    // check standing or not
    if(isStanding){
        isHumanSwing = true;
        isSwaped = false;
    }else{
        if(isHumanSwing){
            if(!heelcontact && !toecontact)
            {
                pauseimpedance = true;
            }else
            {
                pauseimpedance = false;
            }

            if((!heelcontact && !toecontact) && (forwardIndex > 100) && (TimeParameter >= 0.8*Tau_max)){
                isHumanSwing = !isHumanSwing;
                isSwaped     = true;
                forwardIndex = 0;
            }else{
                isHumanSwing = isHumanSwing;
                isSwaped     = false;
                forwardIndex += 1;
            }
        }else
        {
            pauseimpedance = false;
            if((heelcontact || toecontact || (TimeParameter >= Tau_max)) && (forwardIndex > 100) && (TimeParameter >= 0.3*Tau_max)){
                isHumanSwing = !isHumanSwing;
                isSwaped     = true;
                forwardIndex = 0;
            }else{
                isHumanSwing = isHumanSwing;
                isSwaped     = false;
                forwardIndex += 1;
            }
        }
    }
}

/**
 * @brief TrajectoryGen::CheckStandToNewDomain
 * @param dataset
 * @param phip_curr
 *
 * Checks a Vector with probabilities on each possible domain, depending on the current domain, using
 * this data, it chooses the most probable domain based on gaussian mixture models.
 */
void TrajectoryGen::CheckStandToNewDomain(Eigen::VectorXd &dataset, double phip_curr)
{
  static double local_time = 0;
  static double old_phip = 0;
  static bool isInClassificationMode = false;
  static ResultGmm prediction_result;
  static int local_domain = 0;

  std::cout<<"phip_curr: "<<phip_curr<<std::endl;
  if(phip_curr < 0.05 && old_phip >= 0.05)
    {
      gmm_models->reset_probabilities();
      if(old_phip >= 0.05)
        {
          old_phip = 0;
        }
    }

  if(local_time > 3)
    {
      gmm_models->reset_probabilities();
      local_time = 0;
      ROS_WARN("TIMEOUT:      ---------------------Standing---------------------");
    }

  std::cout<<"TimeParameter: "<<TimeParameter<<std::endl;
  if(TimeParameter < 0.4)
    {
      gmm_models->accumulate_points(dataset);
      isInClassificationMode = true;
      local_time += 1.0/200.0;
    }else{
      if(isInClassificationMode)
        {
          local_time = 0;
          // Just entering one-time per normal cycle (at least that user goes backwards), flag in next lines
          prediction_result = gmm_models->pop_gmm_results();
          local_domain = prediction_result.winner_mode;

          /* Just checking Standing2Walking and Standing */

          if(prediction_result.NormalizedProb(1) > prediction_result.NormalizedProb(0) * 2)
            {
            ROS_WARN("---------------------Stand2Walking---------------------");
/*              isStanding = false;
            isWalking = true;
            predictScore = 0;
            _preDomain = "standing";
            currDomain = domains[1]->getDomainData();
            setDomainData(currDomain);
            isHumanSwing = true;
            cout << "Current domain is " << currDomain.name << endl;
            ROS_WARN("SWITCH TO WALK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1"); */
            }else{
              gmm_models->reset_probabilities();
            }

          isInClassificationMode = false;
        }
    }

  old_phip = phip_curr;
}

/**
 * @brief TrajectoryGen::CheckWalkToNewDomain
 * @param dataset
 * @param phip_curr
 * Checks the relevant data from the probability vector obtained, it compares the values to Walk2(new-behavior) against
 * Walking (itself).
 */
void TrajectoryGen::CheckWalkToNewDomain(Eigen::VectorXd &dataset, double phip_curr)
{
  static bool isInClassificationMode = false;
  static ResultGmm prediction_result;
  static double local_time = 0;


  if(local_time > 3)
    {
      gmm_models->reset_probabilities();
      local_time = 0;
      ROS_WARN("TIMEOUT:      ---------------------Standing---------------------");
      /*              isStanding = false;
                  isWalking = true;
                  predictScore = 0;
                  _preDomain = "levelwalk";
                  currDomain = domains[0]->getDomainData();
                  setDomainData(currDomain);
                  isHumanSwing = true;
                  cout << "Current domain is " << currDomain.name << endl;
                  ROS_WARN("SWITCH TO WALK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1"); */
    }

  if(TimeParameter < 0.4)
    {
      gmm_models->accumulate_points(dataset);
      local_time += 1.0 / 200.0;
      isInClassificationMode = true;
    }else{
      if(isInClassificationMode)
        {
          prediction_result = gmm_models->pop_gmm_results();
          isInClassificationMode = false;
          local_time = 0;

          if(prediction_result.NormalizedProb(2) > 2 * prediction_result.NormalizedProb(3))
            {
              ROS_WARN("---------------------Walking2Standing---------------------");
  /*              isStanding = false;
              isWalking = true;
              predictScore = 0;
              _preDomain = "levelwalk";
              currDomain = domains[0]->getDomainData();
              setDomainData(currDomain);
              isHumanSwing = true;
              cout << "Current domain is " << currDomain.name << endl;
              ROS_WARN("SWITCH TO WALK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1"); */
            }
        }else{ // Keep Walking
          gmm_models->reset_probabilities();
        }
    }
}

//*Decide the current sub phase based on the joint angles*/
void TrajectoryGen::updatePhaseStatus(int &phaseIndex, VectorXd &qStates, double _TimeParameters)
{
    double _prephaseIndex = phaseIndex;
    if(_prephaseIndex==1 && legState == 1 && qStates(1) > 0.2 && _TimeParameters > 0.3)
    {
        phaseIndex = _prephaseIndex + 1;
    }
    if(_prephaseIndex==2 && isSwaped)
    {
        phaseIndex = _prephaseIndex + 1;
    }
    if(_prephaseIndex==3 && legState == -1 && qStates(1) > 0.3 && _TimeParameters > 0.3)
    {
        phaseIndex = _prephaseIndex + 1;
    }
    if(_prephaseIndex==4 && isSwaped)
    {
        phaseIndex = 1;
    }
}


//*Decide the current sub phase based on the time*/
void TrajectoryGen::updatePhaseStatus(){
    double phip_current = compute_phip(TimeParameter);
    switch(ndomains){
        case 1:
        {
            phaseIndex = 1;
            break;
        }
        case 2:
        {
            if(isHumanSwing)
            {
                phip_domain2 = 0.6162*(phip_end - initialPhip) + initialPhip;  // these two values are for temporary test
            }else{
                phip_domain2 = 0.8295*(phip_end - initialPhip) + initialPhip;
            }

            if(phip_current >= initialPhip && phip_current <= phip_domain2)
                phaseIndex = 1;
            if(phip_current > phip_domain2 && phip_current <= phip_end)
                phaseIndex = 2;
            if(phip_current > phip_end + 0.01)
                cout << "Phip exceeds the designed end hip position" << endl;
            break;
        }
        case 3:
        {
            if(phip_current >= initialPhip && phip_current <= phip_domain2)
                phaseIndex = 1;
            if(phip_current > phip_domain2 && phip_current <= phip_domain3)
                phaseIndex = 2;
            if(phip_current > phip_domain3 && phip_current <= phip_end)
                phaseIndex = 3;
            if(phip_current > phip_end + 0.001)
                cout << "Phip exceeds the designed end hip position" << endl;
            break;
        }
        default:{
            cout << "Input number of domains is not valid !!" << endl;
        }
    }

}


void TrajectoryGen::updateImpedPhase(){
    double tau_switch;
    if(isHumanSwing)
    {
        tau_switch = impedswitch_stance;  // these two values are for temporary test
    }else{
        tau_switch = impedswitch_swing;
    }

    if(TimeParameter >= 0 && TimeParameter <= tau_switch)
        phaseIndex = 1;
    if(TimeParameter > tau_switch && TimeParameter <= Tau_max);
        phaseIndex = 2;
}

//*Genearte the reference trajectory */
void TrajectoryGen::calcReference(VectorXd &pos_ref, VectorXd &vel_ref, VectorXd &pos_feed, VectorXd &vel_feed, VectorXd &contactForces, VectorXd &imu_angle, double freq)
{
    if(currDomain.name.compare("standing")==0){
         calcStandReference(pos_ref, vel_ref, pos_feed, vel_feed, contactForces, imu_angle);
    }else{
        if(isTimebased)
        {
            calcTimebasedReference(pos_ref, vel_ref, contactForces, freq);
        }else if(!isUsingSpline)
        {
            calcStatebasedReference(pos_ref, vel_ref, pos_feed, vel_feed, contactForces, imu_angle, freq);
        }else
          {
            calcSplineReference(pos_ref, vel_ref, pos_feed, vel_feed, contactForces, imu_angle, freq);
          }
    }

    
}


void TrajectoryGen::calcStandReference(VectorXd &pos_ref, VectorXd &vel_ref, VectorXd &pos_feed, VectorXd &vel_feed, VectorXd &contactForces, VectorXd &imu_data){
    pos_ref << 0, 0;
    vel_ref << 0, 0;
    VectorXd dataset = VectorXd::Zero(8);

    dataset <<  pos_feed,
                imu_data.segment(1, 2),
                vel_feed,
                imu_data.segment(5, 2);
    VectorXd qstance = VectorXd::Zero(3);;
    qstance << 0,
               pos_feed;
    double phip_curr = compute_phip(qstance);

    // checkStandToWalk(dataset, phip_curr);

   //checkStandToWalkStair(dataset, phip_curr);

    checkSwitchEvent(contactForces);
}


void TrajectoryGen::calcTimebasedReference(VectorXd &pos_ref, VectorXd &vel_ref, VectorXd &contactForces, double freq)
{
    VectorXd _pos_ref = pos_ref;
    VectorXd _vel_ref = vel_ref;

    if(isForcesensor)
        checkSwitchEvent(contactForces);
    else
        checkSwitchEvent();
    TimeParameter = updateTimeParameters(freq);
    updateImpedPhase();
    switch(track_mode){
        case 0:   /* setting the reference velocity to zero to track the constants */
        {
            pos_ref(0) = 0;
            pos_ref(1) = 0;
            vel_ref(0) = 0;
            vel_ref(1) = 0;
            break;
        }
        case 1:   /* Sinusoidal time-based tracking for test purpose */
        {
            double y2temp;
            double y2Dottemp;
            compute_sine(y2temp, y2Dottemp);
            /* Extract Ankle Behavior */
            pos_ref(0)  = y2temp;
            vel_ref(0) = y2Dottemp;

            pos_ref(1)  = y2temp;
            vel_ref(1) = y2Dottemp;
            break;
        }
       case 2:  /* Canonical state-based tracking with PHZD reconstruction */
        {
            updatePhaseStatus();
            VectorXd xref  = VectorXd::Zero(2*nFullOutputs);

            calcQrefstate(xref);

            // testing code
//            double phip_curr = 0;
//            VectorXd dataset = VectorXd::Zero(8);;
//            VectorXd imu_data = VectorXd::Zero(8);;
//            dataset <<  pos_ref,
//                        imu_data.segment(1, 2),
//                        vel_ref,
//                        imu_data.segment(5, 2);
//            checkWalkToStand(dataset, phip_curr);

            reference_setup(pos_ref, vel_ref, xref);
            break;
        }
        default:
        {
            cout << "The track mode you choose is not correct " << track_mode << " is not belong to {0, 1, 2}" << endl;
            cout << "References are set to 0" << endl;
            pos_ref(0) = 0;
            pos_ref(1) = 0;
            vel_ref(0) = 0;
            vel_ref(1) = 0;
            break;
        }
    }

    reference_clamp(pos_ref, vel_ref, _pos_ref, _vel_ref);
}


void TrajectoryGen::calcStatebasedReference(VectorXd &pos_ref, VectorXd &vel_ref, VectorXd &pos_feed, VectorXd &vel_feed, VectorXd &contactForces, VectorXd &imu_data, double freq)
{
    // save the previous feedback data
    VectorXd _pos_ref = pos_ref;
    VectorXd _vel_ref = vel_ref;

    //construct the state variable
    VectorXd qstate  = VectorXd::Zero(6);
    VectorXd dqstate = VectorXd::Zero(6);
    VectorXd qstance = VectorXd::Zero(3);
    VectorXd dqstance = VectorXd::Zero(3);

    VectorXd xref  = VectorXd::Zero(2*nFullOutputs);
    /* Code Logic:  1. get the current state; 2. extract the stance state based on the current configuration; 3. check whether the state is on the guard or not;
                    4. if yes, update the initial hip position and recompute the stance state; 5, if no, keep going to the next cycle */
    calcQfeedstate(qstate, dqstate, pos_feed, vel_feed, imu_data);

    calcStancestate(qstance, dqstance, qstate, dqstate);

     double phip_curr;
    // update whether the stance leg is switched or not
    if(isForcesensor){
        // check whether the user is intent to stand only when the prosthetic is in stance phase
        phip_curr = compute_phip(qstance);
        if(isHumanSwing){            
            VectorXd dataset = VectorXd::Zero(8);;
            dataset <<  pos_feed,
                        imu_data.segment(1, 2),
                        vel_feed,
                        imu_data.segment(5, 2);
            if(isWalking){
                // checkWalkToStand(dataset, phip_curr);
            }
            if(isStairclimb){
                // checkStairToStand(dataset, phip_curr);
            }
        }
//        else{
//            VectorXd dataset_imu = VectorXd::Zero(4);;
//            dataset_imu <<  imu_data.segment(1, 2),
//                            imu_data.segment(5, 2);
//            if(isWalking){
//                checkWalkToStand_imu(dataset_imu, phip_curr);
//            }
//            if(isStairclimb){
//                checkStairToStand_imu(dataset_imu, phip_curr);
//            }
//        }
        checkSwitchEvent(contactForces);
    }
    else{
        checkSwitchEvent();
    }

    if(isSwaped){
        // update the stance state since the state reaches the guard
        calcStancestate(qstance, dqstance, qstate, dqstate);
        if(isHumanSwing)
            Tau_max = 1*TauMax_Optimized;
        else
            Tau_max = TauMax_Optimized;

        if(isVelocityadaptable && isODEapproximation){
            // Update the initial hip position, tau_max and desired hip velocity when switch happens
            updateInitialPhip(qstance);
            Tau_max = (phip_end - initialPhip)/desiredVhip;
            updateInitialVhip(dqstance);
        }

        switchTaubase = true;
    }

    if(isHumanSwing){
        TimeParameter = updateTimeParameters(qstance);
    }else{
        double timebaseTimeParameter = updateTimeParameters(freq);
        double statebaseTimeParameter;
        phip_curr = compute_phip(qstance);
        statebaseTimeParameter = (phip_curr - initialPhip) / desiredVhip;
        // add clamp to tau
        statebaseTimeParameter = control_utilities::clamp(statebaseTimeParameter, 0, Tau_max);


        // time based based on the parameter difference
        double time_diff_threshold = 0.04;
        if((abs(timebaseTimeParameter - statebaseTimeParameter) > time_diff_threshold) && switchTaubase){
            TimeParameter = timebaseTimeParameter;
        }else{
            TimeParameter = statebaseTimeParameter;
            switchTaubase = false;
        }

        // TimeParameter = updateTimeParameters(qstance);

        // let the last portion of swing phase finish with time based control
        if(TimeParameter > 0.9*Tau_max)
        {
            TimeParameter += 1.0/freq;
            TimeParameter = control_utilities::clamp(TimeParameter, 0, Tau_max);
        }
    }

    updateImpedPhase();

    if(isODEapproximation){
        // assume the system is on the HZD surface
        calcQrefstate(xref);
    }else{
        calcQrefstate(xref, qstance, dqstance);
    }

    reference_setup(pos_ref, vel_ref, xref);

    reference_clamp(pos_ref, vel_ref, _pos_ref, _vel_ref);
}

void TrajectoryGen::calcQrefstate(VectorXd &xref){
    /* right now, we are only doing state based tracking for prosthetic stance phase. Still, since there is no foot contact sensor, we use state-base tracking combined with time-base switch */
    VectorXd z = VectorXd::Zero(2);
    VectorXd y2dtemp = VectorXd::Zero(nFullR2Outputs);
    VectorXd y2dDottemp = VectorXd::Zero(nFullR2Outputs);

    z << compute_phip(TimeParameter),
         compute_vhip(TimeParameter);

    double scaleTau;
    scaleTau = TimeParameter*desiredVhip/a(0,0);

    compute_y2d(y2dtemp, scaleTau);
    compute_y2dDot(y2dDottemp, z(1), scaleTau);

    phzd_reconstruction(xref, z, y2dtemp, y2dDottemp);
}

void TrajectoryGen::calcQrefstate(VectorXd &xref, VectorXd &qstance, VectorXd &dqstance){
    VectorXd z = VectorXd::Zero(2);
    VectorXd y2dtemp = VectorXd::Zero(nFullR2Outputs);
    VectorXd y2dDottemp = VectorXd::Zero(nFullR2Outputs);

    z << compute_phip(qstance),
         compute_vhip(dqstance);

    // rate limit z(0) and z(1)
    VectorXd z_memory = z;
    z(0) = control_utilities::clamp(z(0), initialPhip, phip_end);
    z(1) = control_utilities::clamp(z(1), 0, 1*desiredVhip);
    if(z(0)!=z_memory(0)){
        z(1) = 0;
    }
    double scaleTau;
    scaleTau = TimeParameter*desiredVhip/a(0,0);
    compute_y2d(y2dtemp, scaleTau);
    compute_y2dDot(y2dDottemp, z(1), scaleTau);

    phzd_reconstruction(xref, z, y2dtemp, y2dDottemp);
}

void TrajectoryGen::phzd_reconstruction(VectorXd &xd, VectorXd &z, VectorXd &y2d, VectorXd &y2dDot)
{
        VectorXd y_pos = VectorXd::Zero(nFullOutputs);
        VectorXd y_vel = VectorXd::Zero(nFullOutputs);
        y_pos(0) = z(0);
        y_vel(0) = z(1);

        y_pos.segment(1, nFullR2Outputs) = y2d;
        y_vel.segment(1, nFullR2Outputs) = y2dDot;

        xd.head(nFullOutputs) = invPhi*y_pos;
        xd.tail(nFullOutputs) = invPhi*y_vel;
}

void TrajectoryGen::calcQfeedstate(VectorXd &qstate, VectorXd &dqstate, VectorXd &pos_feed, VectorXd &vel_feed, VectorXd &imu_data){
    double ampros_foot_angle = imu_data(0) - pos_feed(0) - pos_feed(1); // get the prosthetic foot angle and velocity w.r.t the ground
    double ampros_foot_vel   = imu_data(4) - vel_feed(0) - vel_feed(1);

    qstate <<   ampros_foot_angle*0,
                pos_feed,
                imu_data.segment(1,2),
                0;
    dqstate <<  ampros_foot_vel*0,
                vel_feed,
                imu_data.segment(5,2),
                0;
}

void TrajectoryGen::calcStancestate(VectorXd &qstance, VectorXd &dqstance, VectorXd &qstate, VectorXd &dqstate){
    if(isHumanSwing)
    {
        qstance = qstate.head(3);
        dqstance = dqstate.head(3);
    }else
    {
        qstance <<  qstate(5),
                    qstate(4),
                    qstate(3);
        dqstance << dqstate(5),
                    dqstate(4),
                    dqstate(3);
    }
}

void TrajectoryGen::updateInitialPhip(VectorXd &qstance)
{
    double phip_temp = compute_phip(qstance);
    initialPhip = control_utilities::clamp(phip_temp, initialPhip, phip_end);  //a(0, 1) is the optimized initial hip position
}

void TrajectoryGen::updateInitialVhip(VectorXd &dqstance)
{
    double vhip_temp = compute_vhip(dqstance);
    initialVhip = control_utilities::clamp(vhip_temp, 0, desiredVhip); // a(0,0) is the optimized desired hip velocity
}

void TrajectoryGen::reference_setup(VectorXd &pos_ref, VectorXd &vel_ref, VectorXd &xd){
    int sanklejoint, skneejoint, nsanklejoint, nskneejoint;

    if(nParameters==7){
        sanklejoint = 2;
        skneejoint = 3;
        nsanklejoint = 7;
        nskneejoint = 6;
    }
    else{
        sanklejoint = 1;
        skneejoint  = 2;
        nsanklejoint= 6;
        nskneejoint = 5;
    }

    if(isHumanSwing)
    {
        pos_ref(0) = xd(sanklejoint-1);
        vel_ref(0) = xd(sanklejoint-1 + nFullOutputs);
        pos_ref(1) = xd(skneejoint-1);
        vel_ref(1) = xd(skneejoint-1 + nFullOutputs);
    }else
    {
        pos_ref(0) =  xd(nsanklejoint - 1);
        vel_ref(0) =  xd(nsanklejoint - 1 + nFullOutputs);
        pos_ref(1) =  xd(nskneejoint - 1);
        vel_ref(1) =  xd(nskneejoint - 1 + nFullOutputs);
    }
}

void TrajectoryGen::reference_clamp(VectorXd &pos_ref, VectorXd &vel_ref, VectorXd &_pos_ref, VectorXd &_vel_ref){

//    /* apple rate limiter to both position terms and velocity terms */
//    pos_ref(0) = control_utilities::rate_limit(_pos_ref(0), pos_ref(0), 1, ANKLE_POS_RATELIMITER);
//    pos_ref(1) = control_utilities::rate_limit(_pos_ref(1), pos_ref(1), 1, KNEE_POS_RATELIMITER);

//    vel_ref(0) = control_utilities::rate_limit(_vel_ref(0), vel_ref(0), 1, ANKLE_VEL_RATELIMITER);
//    vel_ref(1) = control_utilities::rate_limit(_vel_ref(1), vel_ref(1), 1, KNEE_VEL_RATELIMITER);
    /*
       constrain the movement of both the ankle and the knee
       ankle:   position -0.35 ~~ 0.5 radians -25 ~ 30
                velocity -10 ~~ 10 radians/s
       knee:    position 0 ~~ 1.1 radians 0 ~ 65
                velocity -10 ~~ 10 radians/s
    */
    VectorXd _pos_ref_clamp;
    _pos_ref_clamp = pos_ref;
    pos_ref(0) = control_utilities::clamp(pos_ref(0), ANKLE_POSBOUND_LOW, ANKLE_POSBOUND_HIGH);
    pos_ref(1) = control_utilities::clamp(pos_ref(1), KNEE_POSBOUND_LOW, KNEE_POSBOUND_HIGH);
    if(pos_ref(0)!=_pos_ref_clamp(0)){           //if the position is clamped, set the desired velocity to 0
        vel_ref(0) = 0;
    }else
    {
        vel_ref(0) = control_utilities::clamp(vel_ref(0), ANKLE_VELBOUND_LOW, ANKLE_VELBOUND_HIGH);
    }

    if(pos_ref(1)!=_pos_ref_clamp(1)){
        vel_ref(1) = 0;
    }else
    {
        vel_ref(1) = control_utilities::clamp(vel_ref(1), KNEE_VELBOUND_LOW, KNEE_VELBOUND_HIGH);
    }
}

/* Spline Functions */
void TrajectoryGen::BuildConnectingSpline(Eigen::VectorXd &pos_ref, VectorXd &vel_ref, VectorXd &pos_feed, VectorXd &vel_feed, VectorXd &contactForces, VectorXd &imu_angle, double freq)
{
  VectorXd P1 = VectorXd::Zero(nFullR2Outputs);
  VectorXd P2 = VectorXd::Zero(nFullR2Outputs);
  VectorXd DP1 = VectorXd::Zero(nFullR2Outputs);
  VectorXd DP2 = VectorXd::Zero(nFullR2Outputs);
  VectorXd DDP1 = VectorXd::Zero(nFullR2Outputs);
  VectorXd DDP2 =  VectorXd::Zero(nFullR2Outputs);
  VectorXd y2d = VectorXd::Zero(nFullR2Outputs);
  VectorXd y2dDot = VectorXd::Zero(nFullR2Outputs);

  //construct the state variable
  VectorXd qstate  = VectorXd::Zero(6);
  VectorXd dqstate = VectorXd::Zero(6);
  VectorXd qstance = VectorXd::Zero(3);
  VectorXd dqstance = VectorXd::Zero(3);

  double local_time_parameter;

  calcQfeedstate(qstate, dqstate, pos_feed, vel_feed, imu_angle);

  calcStancestate(qstance, dqstance, qstate, dqstate);

  TimeParameter = updateTimeParameters(qstance);
  local_time_parameter = TimeParameter;

  compute_y2d(y2d, local_time_parameter);
  compute_y2dDot(y2dDot,compute_vhip(dqstance), local_time_parameter);

  //phzd_reconstruction(xref,z,y2d,y2dDot);

  P1 = y2d;
  DP1 = y2dDot;
  DDP1 = VectorXd::Zero(nFullR2Outputs);

  compute_y2d(y2d, local_time_parameter + connecting_spline->tConvergence);
  compute_y2dDot(y2dDot,this->a(0,0),local_time_parameter + connecting_spline->tConvergence);

  //phzd_reconstruction(xref,z,y2d,y2dDot);

  P2 = y2d;
  DP2 = y2dDot;
  DDP2 = VectorXd::Zero(nFullR2Outputs);

  connecting_spline->setSwitichingTime(local_time_parameter);
  connecting_spline->addBoundaryConditions(P1,P2,DP1,DP2,DDP1,DDP2);
  connecting_spline->solveSplines();
}

void TrajectoryGen::calcSplineReference(VectorXd &pos_ref, VectorXd &vel_ref, VectorXd &pos_feed, VectorXd &vel_feed, VectorXd &contactForces, VectorXd &imu_angle, double freq)
{
  VectorXd y2temp = VectorXd::Zero(nFullR2Outputs);
  VectorXd y2dotTemp = VectorXd::Zero(nFullR2Outputs);
  VectorXd xref  = VectorXd::Zero(2*nFullOutputs);
  VectorXd qstate  = VectorXd::Zero(6);
  VectorXd dqstate = VectorXd::Zero(6);
  VectorXd qstance = VectorXd::Zero(3);
  VectorXd dqstance = VectorXd::Zero(3);

  VectorXd z = VectorXd::Zero(2);

  if(!isSplineBuilt)
    {
    BuildConnectingSpline(pos_ref,vel_ref,pos_feed,vel_feed,contactForces,imu_angle,freq);
    isSplineBuilt = true;
    }else{
      calcQfeedstate(qstate, dqstate, pos_feed, vel_feed, imu_angle);

      calcStancestate(qstance, dqstance, qstate, dqstate);

      TimeParameter = updateTimeParameters(qstance);
    }



  if(connecting_spline->tConvergence + connecting_spline->tSwitch >= TimeParameter)
    {
      y2temp << connecting_spline->computeOutput(TimeParameter - connecting_spline->tSwitch);
      y2dotTemp << connecting_spline->computeDotOutput(TimeParameter - connecting_spline->tSwitch);

      calcQfeedstate(qstate, dqstate, pos_feed, vel_feed, imu_angle);

      calcStancestate(qstance, dqstance, qstate, dqstate);

      z << compute_phip(qstance),
           compute_vhip(dqstance);

      phzd_reconstruction(xref,z,y2temp,y2dotTemp);

      reference_setup(pos_ref, vel_ref, xref);

      reference_clamp(pos_ref, vel_ref, qstate, dqstate);
      cout<<"pos ref: "<<pos_ref<<endl;
    }else{
      isUsingSpline = false;
      isSplineBuilt = false;
    }


}
