function impedParams = dynamic_estimation_stance_imped(t, q, dq, u) 
%% get the discrete dynamics
startpoint      = 1;
q               = q(startpoint:end, :);
dq              = dq(startpoint:end, :);
u               = u(startpoint:end, :);
t               = t(startpoint:end, :);

chosen_joints   = [1, 2];
X_chosen        = [q(:, chosen_joints), dq(:, chosen_joints)];
Y_chosen        = u(:, chosen_joints);
u_chosen        = u(:, chosen_joints);

errorbound      = 1e-12;
options         = optimset('Algorithm','interior-point','Display','final',...
                        'TolX',errorbound,'MaxIter', 1000,...
                        'MaxFunEvals',10000,'TolCon',errorbound);
nparameters     = 3;
njoints         = length(chosen_joints);
ip_guess        = zeros(njoints, nparameters);
% ip_guess        = [ -11.7563   -0.4716   -0.2275
%                     -59.9398   -1.0946    0.1710];
ImpedMax        = 20;
lbmat           = -ImpedMax*10*ones(njoints, nparameters);
ubmat           = ImpedMax*10*ones(njoints, nparameters);
ubmat(:, 1)     = 0;
lbmat(:, 2)     = -8*ones(njoints, 1);
ubmat(:, 2)     = 8*ones(njoints, 1);
lbmat(:, end)   = -2*ones(njoints, 1);
ubmat(:, end)   = 2*ones(njoints, 1);
%%ankle angle constraints
lbmat(1, end)   = -0.5;
ubmat(1, end)   = 0;
%%knee angle constraints
lbmat(2, end)   = 0.1;
ubmat(2, end)   = 0.6;

initialguess    = ip_guess(:);
ub              = ubmat(:);
lb              = lbmat(:);  

ip_opt          = fmincon(@(u)cost(u, nparameters, X_chosen, Y_chosen),...
                           initialguess, [], [], [], [], lb, ub, ...
                          @(u)mycon(u, X_chosen, Y_chosen),options);
impedParams     = reshape(ip_opt, njoints, nparameters);
%%plot the results
plot_estimatedImpedParams(impedParams, t, X_chosen, Y_chosen, u_chosen);

%% functions for fmincon
function ret = cost(coef, nparameters, Xdata, Ydata) 
    njoints         = size(Ydata, 2);
    impedparams     = reshape(coef, njoints, nparameters);
    ret  = 0;
    for j = 1: njoints;
        ret = ret + sum(1/2*((func_impedance(impedparams(j, :),...
                [Xdata(:, j), Xdata(:, j+njoints)]) - Ydata(:, j)).^2));
    end
end

function [c, ceq] = mycon(coef, Xdata, Ydata)

%     initial_con2 = abs(func_impedance(u(2,:), Xdata(1,[2, 6])) - Ydata(1,2));
%     c   = [ qe - 3];
%     c       = [initial_con2 - 1];
    c       = [];
    ceq     = [];
end
end