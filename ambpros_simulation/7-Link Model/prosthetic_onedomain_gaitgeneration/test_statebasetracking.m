addpath('./build_torso','./buildopt_torso', './functions_figures', 'classdef')
addpath('./wrappers')
addpath('./utilities');

%%
robotwalk   = amberwalk_configuration;

ep      = robotwalk.ep;
a       = robotwalk.a_opt{1};
a       = a_expand(a);
ndof    = robotwalk.nDof;
nbasedof= robotwalk.nBaseDof;
nexdof  = nbasedof + ndof;

v_d     = 0.8*a(1, 1);

x_plus  = robotwalk.ic{1};
x_minus = robotwalk.x_opt{1};
ic      = x_plus;
%> @note Using variables from cpp_save

phipend     = deltaphip_sca(x_minus);
phipinitial = deltaphip_sca(x_plus);
vhipinitial = deltaphip_dot_mat(x_plus) * x_plus(nbasedof+nexdof + 1:end);

zm      = phipend;
zp      = phipinitial;
z0      = phipinitial;

t0      = (z0 - zp) /v_d;
tf      = (zm - zp) /v_d;

% count   = length(ts);
count   = 40;

qa      = zeros(ndof, count);
dqa     = zeros(ndof, count);
qd      = zeros(ndof, count);
dqd     = zeros(ndof, count);
yd2s    = zeros(ndof-1, count);
dyd2s   = zeros(ndof-1, count);

ts      = zeros(1, count);
ps      = zeros(1, count);
dps     = zeros(1, count);
phip    = zeros(1, count);
ptau    = zeros(1, count);

qa(:, 1)=x_plus(nbasedof+1:nexdof);
dqa(:,1)=x_plus(nbasedof+nexdof+1:end);

qd(:, 1)=x_plus(nbasedof+1:nexdof);
dqd(:,1)=x_plus(nbasedof+nexdof+1:end);

isODEapproximation = 1;
switchIndex = count-1;

Tincrease   = 0;
forwardIndex = 0;
for i = 1:count-1 
    
    qa_cur  = qa(:, i);
    dqa_cur = dqa(:, i);
    
    phip(i) = deltaphip_sca([0;0;0;qa_cur]);
    tau_cur = (phip(i) - phipinitial)/v_d;
    
%     if(abs(h_sca([0;0;0;qa_cur])) < 0.001&&tau > 0.1)   
    if(tau_cur > tf && forwardIndex > 20)
        disp('Guard reached, leg swaped!')
        Tincrease   = Tincrease + tf;
%         phipend     = deltaphip_sca([0;0;0;qa_cur]);
        xreset      = R1_map([0;0;0;qa_cur; 0;0;0;dqa_cur], 1);
        qa_cur      = xreset(4:nexdof);
        dqa_cur     = xreset(4+nexdof:end);   
        
        phipinitial = deltaphip_sca([0;0;0;qa_cur]);
        vhipinitial = deltaphip_dot_mat()*dqa_cur;  
        
        tf          = (phipend - phipinitial)/v_d;
        phip(i)     = deltaphip_sca([0;0;0;qa_cur]);
        tau_cur     = (phip(i) - phipinitial)/v_d;
        ts(i)       = tau_cur  + tf;
        forwardIndex = 0;
    else
        forwardIndex = forwardIndex + 1;
    end
    
    
    
    if(isODEapproximation)
        tau_next= tau_cur  + tf/40;
        
        z1      = v_d * tau_next + phipinitial + ...
                    1 / ep * (1 - exp(-ep * tau_next)) * (vhipinitial - v_d);
        dz1     = v_d + exp(-ep * tau_next) * (vhipinitial - v_d);   
    else
        z1      = deltaphip_sca([0;0;0;qa_cur]);
        dz1     = deltaphip_dot_mat()*dqa_cur;
    end
                  
    yd2     = yt_d2(tau_next*v_d/a(1,1), phipinitial, a);
    dyd2    = dyt_d2(tau_next*v_d/a(1,1), phipinitial, a)*dz1/a(1,1);
    qd_cur  = Phi_mat() \ [z1; yd2];
    dqd_cur = Phi_mat() \ [dz1; dyd2];

    ps(i)   = z1;
    dps(i)  = dz1;

    qd(:, i)    = qd_cur;
    dqd(:, i)   = dqd_cur;
    yd2s(:, i)  = yd2;
    dyd2s(:, i) = dyd2;

    qa(:, i+1)  = qd_cur;
    dqa(:, i+1) = dqd_cur;   
    
    ts(i) = Tincrease + tau_next;
end

%% Ensure final position corresponds to fixed point
xf      = [0; 0 ;0;  qa(:, switchIndex); 0; 0; 0; dqa(:, switchIndex)];
xf_plus = R1_map(xf, 1);
diff_err= xf - x_minus;

err     = max(abs(diff_err));
tol     = 1e-10;
if err > tol
    [xf_plus, x_plus]
    err
    warning('Condition is not on HZD surface!');
end

z = [ps; dps];

%% Generate test file
test = struct();
test.in  = struct();
test.in.qa = qa(:, 1:switchIndex)';
test.in.dqa = dqa(:, 1:switchIndex)';
test.in.xplus = x_plus([4:nexdof, nexdof+4:end]);
test.out = struct();
test.out.phipic = phipinitial;
test.out.phipfc = phipend;
test.out.vhipic = vhipinitial;
test.out.vhipfc = v_d;
test.out.phiptau= ts;
test.out.z      = z(:, 1:switchIndex)';
test.out.qd     = qd(:, 1:switchIndex)';
test.out.dqd    = dqd(:, 1:switchIndex)';
test.out.yd2s = yd2s(:, 1:switchIndex)';
test.out.dyd2s = dyd2s(:, 1:switchIndex)';

addpath('./utilities/yaml');
yaml_init();
dir = '~/catkin_ws/src/amber_prosthetic/ambpros_trajectory/share';
yaml_write_file([dir, '/test_Tau.yaml'], test);

%% Position of the hip
figure(22);
clf();
subplot(2, 1, 1);
plot(ts(1:switchIndex), qd(:, 1:switchIndex));
ylabel('q');
subplot(2, 1, 2);
plot(ts(1:switchIndex), dqd(:, 1:switchIndex));
ylabel('dq');

figure
dqd_actual = zeros(6, switchIndex-1);
for i=1:6
    dqd_actual(i, :) = diff(qd(i, 1:switchIndex))./diff(ts(1:switchIndex));
    plot(ts(1:switchIndex-1), dqd_actual(i,:)','--','Linewidth', 2)
    hold on
    plot(ts(1:switchIndex), dqd(i, 1:switchIndex)')
    hold on
end

%% plot the q and dq
figure
plot(ts(1:switchIndex), ps(1:switchIndex))

%% Aniplot
do_aniplot = true;
if do_aniplot
    figure(2);
    clf;
    ramp = plot([-1 10], [0 0], 'b:');
    hold on;
    legs = plot([0 0], 'k');
    hold off;
    axis([-1 5 -1 1.2]);%

    px = 0;
    py = 0;

    time_scale = 1;
    tic;
    for i = 1:count
        tnow = 0;
        while (tnow < time_scale*ts(i))
            tnow = toc;
        end
        % position
        q = qa(:, i);
        pos = jpos_mat([0;0;0;q]);
        phipz = pos(1,3);
        set(legs, 'XData', pos(1,:), 'YData', pos(2,:), ...
            'erasemode', 'normal');
        axis equal;
        hold on;
        drawnow;
    end
end
