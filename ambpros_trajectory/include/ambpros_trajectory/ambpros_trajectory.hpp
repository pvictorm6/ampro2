#ifndef TRAJECTORY_GENERATION_HPP
#define TRAJECTORY_GENERATION_HPP

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Eigen>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <yaml_eigen_utilities/yaml_eigen_utilities.hpp>
#include <roslib_utilities/ros_package_utilities.hpp>
#include "neuralNetwork.h"
#include <gmm_classifier.hpp>
#include <base_spline.hpp>

struct DomainData
{
    std::string name;
    Eigen::MatrixXd a;
    Eigen::MatrixXd impedparams_stance;
    Eigen::MatrixXd impedparams_swing;
    double impedswitch_stance;
    double impedswitch_swing;
    double phaseswitch;
    Eigen::MatrixXd invPhi;
    Eigen::MatrixXd invPhi2;

    int nOutputs;
    int nR2Outputs;
    int nParameters;
};

class Domain{
    DomainData data;
    std::string name;
 public:
    Domain(std::string domainname): name(domainname) {
    }

   std:: string getDomainName(){
        return name;
    }

    DomainData getDomainData(){
        return data;
    }

    void loaddata(){
            std::cout <<"Loading the data of " << name << "\n" <<std:: endl;
            data.name = name;

            if(data.name.compare("standing") != 0){
                // load the a_opt data
                std::string datapath_aopt = "package://ambpros_trajectory/share/a_opt_";
                datapath_aopt.append(name);
                datapath_aopt.append(".yaml");

                std::string filepath1 = roslib_utilities::resolve_local_url(datapath_aopt).string();
                YAML::Node node1;
                yaml_utilities::yaml_read_file(filepath1, node1);
                node1["a_opt"] >> data.a;
                node1["invPhi"] >> data.invPhi;
                //for temporary
//                data.nOutputs = 6;
//                data.nParameters = 5;
//                data.nR2Outputs = 5;

//                if(data.nParameters==7){
//                    //load the multi-contact walking
//                    data.invPhi2 = Eigen::MatrixXd::Zero(7,7);
//                    data.phaseswitch = 0;
//                }

                // load the impedance parameter
                std::string datapath_imped = "package://ambpros_controller/share/impedparameters_";
                datapath_imped.append(name);
                datapath_imped.append(".yaml");
                std::string filepath2 = roslib_utilities::resolve_local_url(datapath_imped).string();
                YAML::Node node2;
                yaml_utilities::yaml_read_file(filepath2, node2);
                node2["stance"] >>data.impedparams_stance;
                node2["swing"]  >> data.impedparams_swing;
                node2["stancetau_switch"] >> data.impedswitch_stance;
                node2["swingtau_switch"]  >> data.impedswitch_swing;
            }
            else{
                data.a = Eigen::MatrixXd::Zero(6, 5);
                data.invPhi = Eigen::MatrixXd::Zero(6, 6);
                data.impedparams_stance = Eigen::MatrixXd::Zero(4, 3);
                data.impedparams_swing  = Eigen::MatrixXd::Zero(4, 3);
                data.impedswitch_stance  = 0;
                data.impedswitch_swing  = 0;
//                data.nOutputs = 0;
//                data.nParameters = 0;
//                data.nR2Outputs = 0;
            }
    }
};

typedef boost::shared_ptr<Domain> DomainPtr;
typedef boost::shared_ptr<neuralNetwork> nnPtr;
typedef boost::shared_ptr<gmm_classifier> GmmPtr;
typedef boost::shared_ptr<SplineSupport> SplinePtr;

class TrajectoryGen
{
public:
	/* human-inspired outputs parameters */
	int nFullOutputs;
	int nFullR2Outputs;
	int nParameters;
    int ndomains;
    int track_mode;
    int initialdomain;
	/* parameterized time variables*/
	double Tau_max;
    double ep;
	double initialPhip;  /*initial hip position obtained from the optimization*/
	double phip_domain2;
	double phip_domain3;
	double phip_end;
	double initialVhip;
	double desiredVhip;
	double TimeParameter;
    double slowrate;
    double tauIncrement;

    double impedswitch_stance;
    double impedswitch_swing;

    Eigen::MatrixXd a;

    Eigen::MatrixXd invPhi;

    std::vector<DomainPtr> domains;
    std::vector<nnPtr> nnmodels;
    DomainData currDomain;
    GmmPtr gmm_models;
    SplinePtr connecting_spline;

    /* leg status */
    int legState; /* 1: prosthetic stance, 0: Undertermined -1: prosthetic swing */
    double phaseIndex;
    bool isWalking;
    bool isStanding;
    bool isStairclimb;
    bool isHumanSwing;
    bool isSwaped;
    bool heelcontact;
    bool toecontact;
    bool pauseimpedance;
    double forceThreshold;
    bool isUsingSpline;

    /* options to for different settings */
    bool isGeometry;
    bool isTimebased;
    bool isODEapproximation;
    bool isVelocityadaptable;
    bool isForcesensor;
    bool isSplineBuilt;

	TrajectoryGen();

    double compute_phip(double t);

    double compute_phip(Eigen::VectorXd &dqstance);

    double compute_vhip(double t);

    double compute_vhip(Eigen::VectorXd &qstance);

    void calcReference(Eigen::VectorXd &pos_ref, Eigen::VectorXd &vel_ref, Eigen:: VectorXd &pos_feed,  Eigen::VectorXd &vel_feed, Eigen::VectorXd &contactForces, Eigen::VectorXd &imu_data, double freq);

public:
	/* human parameters */
	double calflength;
	double thighlength;
	double anklelength;

    void setDomainData(DomainData domaindata);

    void compute_y2d(Eigen::VectorXd &y2d, double t);

    void compute_y2dDot(Eigen::VectorXd &y2dDot, double vhip, double t);

    void compute_sine(double &y2, double &y2d);

    double updateTimeParameters(double freq);

    double updateTimeParameters(Eigen::VectorXd &qstance);

    void checkSwitchEvent();

    void checkSwitchEvent(Eigen::VectorXd &contactForce);

    void checkStandToWalk(Eigen::VectorXd &dataset, double phip_curr);

    void checkStandToWalkStair(Eigen::VectorXd &dataset, double phip_curr);

    void checkWalkToStand(Eigen::VectorXd &dataset, double phip_curr);

    void checkStairToStand(Eigen::VectorXd &dataset, double phip_curr);

    void checkWalkToStand_imu(Eigen::VectorXd &dataset, double phip_curr);

    void checkStairToStand_imu(Eigen::VectorXd &dataset, double phip_curr);

    /* Gmm Functions */

    void CheckStandToNewDomain(Eigen::VectorXd &dataset, double phip_curr);

    void CheckWalkToNewDomain(Eigen::VectorXd &dataset, double phip_curr);

    /* Spline Functions */

    void BuildConnectingSpline(VectorXd &pos_ref, VectorXd &vel_ref, VectorXd &pos_feed, VectorXd &vel_feed, VectorXd &contactForces, VectorXd &imu_angle, double freq);

    void calcSplineReference(VectorXd &pos_ref, VectorXd &vel_ref, VectorXd &pos_feed, VectorXd &vel_feed, VectorXd &contactForces, VectorXd &imu_angle, double freq);


    void updateImpedPhase();

    void updatePhaseStatus();

    void updatePhaseStatus(int &phaseIndex, Eigen::VectorXd &qStates, double _TimeParameters);

    void updateInitialPhip(Eigen::VectorXd &qstance);

    void updateInitialVhip(Eigen::VectorXd &dqstance);

    void calcQfeedstate(Eigen::VectorXd &qstate, Eigen::VectorXd &dqstate, Eigen:: VectorXd &pos_feed,  Eigen::VectorXd &vel_feed, Eigen::VectorXd &imu_data);

    void calcQrefstate(Eigen::VectorXd &xref);

    void calcQrefstate(Eigen::VectorXd &xref, Eigen::VectorXd &qstance, Eigen::VectorXd &dqstance);

    void calcStancestate(Eigen::VectorXd &qstance, Eigen::VectorXd &dqstance, Eigen:: VectorXd &pos_feed,  Eigen::VectorXd &dqstate);

    void calcTimebasedReference(Eigen::VectorXd &pos_ref, Eigen::VectorXd &vel_ref, Eigen::VectorXd &contactForces, double freq);

    void calcStandReference(Eigen::VectorXd &pos_ref, Eigen::VectorXd &vel_ref, Eigen:: VectorXd &pos_feed, Eigen::VectorXd &vel_feed,  Eigen::VectorXd &contactForces, Eigen::VectorXd &imu_data);

    void calcStatebasedReference(Eigen::VectorXd &pos_ref, Eigen::VectorXd &vel_ref,Eigen:: VectorXd &pos_feed,
                                 Eigen::VectorXd &vel_feed, Eigen::VectorXd &contactForces, Eigen::VectorXd &imu_angle, double freq);

    void phzd_reconstruction(Eigen::VectorXd &xd, Eigen::VectorXd &z, Eigen::VectorXd &y2d, Eigen::VectorXd &y2dDot);

    void reference_setup(Eigen::VectorXd &pos_ref, Eigen::VectorXd &vel_ref, Eigen::VectorXd &xd);

    void reference_clamp(Eigen::VectorXd &pos_ref, Eigen::VectorXd &vel_ref, Eigen::VectorXd &_pos_ref, Eigen::VectorXd &_vel_ref);

    void geometry_reconstruction(Eigen::VectorXd &ic, Eigen::VectorXd &xd, double sf, double sfD);
};

#endif
