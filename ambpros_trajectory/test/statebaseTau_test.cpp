#include <gtest/gtest.h>
#include "ros/ros.h"
#include <ambpros_trajectory/ambpros_trajectory.hpp>
#include <roslib_utilities/ros_package_utilities.hpp>
#include <yaml_eigen_utilities/yaml_eigen_utilities.hpp>

using namespace YAML;
using namespace common;
using boost::filesystem::path;
using namespace std;
using namespace Eigen;
using namespace yaml_utilities;

/**
 * @brief Statebased for the Tau generation
 */
TEST(ambpros_trajectory, StateBased)
{
    TrajectoryGen AmbprosTraj;

    string filepath = roslib_utilities::resolve_local_url("package://ambpros_trajectory/share/test_Tau.yaml").string();
    Node node;

    yaml_utilities::yaml_read_file(filepath, node);
    const Node &in = node["in"];
    const Node &out = node["out"];

    const double tolerance = 1e-3;

    // Check the initial conditions
    double phipic;
    double phipfc;
    double vhipic;
    double vhipfc;
    out["phipic"] >> phipic;
    out["phipfc"] >> phipfc;
    out["vhipic"] >> vhipic;
    out["vhipfc"] >> vhipfc;

    EXPECT_LE(abs(phipic- AmbprosTraj.initialPhip), tolerance);
    EXPECT_LE(abs(phipfc- AmbprosTraj.phip_end), tolerance);
    EXPECT_LE(abs(vhipic- AmbprosTraj.initialVhip), tolerance);
    EXPECT_LE(abs(vhipfc- AmbprosTraj.desiredVhip), tolerance);


    const Node &qas = in["qa"];
    const Node &dqas = in["dqa"];
    const Node &qds = out["qd"];
    const Node &dqds = out["dqd"];
    const Node &yd2s = out["yd2s"];
    const Node &dyd2s = out["dyd2s"];
    const Node &zs = out["z"];
    const Node &phipTau = out["phiptau"];

    int count = zs.size();
    common_assert_msg(qas.size() == count, qas.size() << " != " << count);
    common_assert_msg(dqas.size() == count, dqas.size() << " != " << count);

    Eigen::VectorXd qd_expected(AmbprosTraj.nFullOutputs), qd_actual(AmbprosTraj.nFullOutputs);
    Eigen::VectorXd dqd_expected(AmbprosTraj.nFullOutputs), dqd_actual(AmbprosTraj.nFullOutputs);
    Eigen::VectorXd z_expected(2), z_actual(2);

    Eigen::VectorXd xref  = Eigen::VectorXd::Zero(2*AmbprosTraj.nFullOutputs);

    Eigen::VectorXd qa_actual(AmbprosTraj.nFullOutputs), dqa_actual(AmbprosTraj.nFullOutputs);
    Eigen::VectorXd qstance(3), dqstance(3);

    Eigen::VectorXd yd2_expected;
    Eigen::VectorXd dyd2_expected;
    Eigen::VectorXd yd2_actual = Eigen::VectorXd::Zero(AmbprosTraj.nFullR2Outputs);
    Eigen::VectorXd dyd2_actual = Eigen::VectorXd::Zero(AmbprosTraj.nFullR2Outputs);

    double phip_cur;
    double phip_cur_actual;
    //Test 1, use the qa and dqa from matlab
    for (int i = 0; i < count; ++i)
    {
        qds[i]  >> qd_expected;
        dqds[i] >> dqd_expected;
        zs[i]   >> z_expected;
        yd2s[i] >> yd2_expected;
        dyd2s[i] >> dyd2_expected;
        qas[i]  >> qa_actual;
        dqas[i] >> dqa_actual;

        phipTau[i] >> phip_cur;

        qstance << 0,
                    qa_actual.segment(0, 2);
        dqstance << 0,
                    dqa_actual.segment(0, 2);



        AmbprosTraj.updateTimeParameters(qstance);

        // Check hip position
        phip_cur_actual = AmbprosTraj.TimeParameter;
        EXPECT_LE(abs(phip_cur-phip_cur_actual), tolerance);

        z_actual << AmbprosTraj.compute_phip(AmbprosTraj.TimeParameter),
                    AmbprosTraj.compute_vhip(AmbprosTraj.TimeParameter);

        // Check z
        EXPECT_LE((z_actual - z_expected).norm(), tolerance);


        AmbprosTraj.compute_y2d(yd2_actual, AmbprosTraj.TimeParameter);
        AmbprosTraj.compute_y2dDot(dyd2_actual, z_actual(1), AmbprosTraj.TimeParameter);
        // Check outputs
        EXPECT_LE((yd2_actual - yd2_expected).norm(), tolerance);
        EXPECT_LE((dyd2_actual - dyd2_expected).norm(), tolerance);

        AmbprosTraj.phzd_reconstruction(xref, z_actual, yd2_actual, dyd2_actual);
        qd_actual = xref.head(AmbprosTraj.nFullOutputs);
        dqd_actual = xref.tail(AmbprosTraj.nFullOutputs);

        // Check reconstruction
        EXPECT_LE((qd_actual - qd_expected).norm(), tolerance);
        EXPECT_LE((dqd_actual - dqd_expected).norm(), tolerance);
    }

    //Test 2, generate qa and dqa within the code
    Eigen::VectorXd xplus(2*AmbprosTraj.nFullOutputs);

    in["xplus"] >> xplus;
    qa_actual = xplus.head(AmbprosTraj.nFullOutputs);
    dqa_actual = xplus.tail(AmbprosTraj.nFullOutputs);
    for (int i = 0; i< count; i++){

        qds[i]  >> qd_expected;
        dqds[i] >> dqd_expected;
        zs[i]   >> z_expected;
        qas[i]  >> qa_actual;
        dqas[i] >> dqa_actual;

        yd2s[i] >> yd2_expected;
        dyd2s[i] >> dyd2_expected;

        // Check actual
        EXPECT_LE((qa_actual - qa_actual).norm(), tolerance);
        EXPECT_LE((dqa_actual - dqa_actual).norm(), tolerance);

        qstance << 0,
                    qa_actual.segment(0, 2);
        dqstance << 0,
                    dqa_actual.segment(0, 2);


        AmbprosTraj.checkSwitchEvent();
        AmbprosTraj.updateTimeParameters(qstance);

        AmbprosTraj.calcQrefstate(xref);

        qd_actual = xref.head(AmbprosTraj.nFullOutputs);
        dqd_actual = xref.tail(AmbprosTraj.nFullOutputs);

        qa_actual = qd_actual;
        dqa_actual = dqd_actual;

        // Check desired
        EXPECT_LE((qd_actual - qd_expected).norm(), tolerance);
        EXPECT_LE((dqd_actual - dqd_expected).norm(), tolerance);
    }

}

int main(int argc, char **argv)
{
    ros::init(argc,argv,"ambpros_tests_12345");
    ros::NodeHandle n;
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
