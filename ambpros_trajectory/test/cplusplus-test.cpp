#include <gtest/gtest.h>
#include "ros/ros.h"
#include <boost/shared_ptr.hpp>
#include <vector>
#include <iostream>
#include <ambpros_trajectory/ambpros_trajectory.hpp>
#include <roslib_utilities/ros_package_utilities.hpp>
#include <yaml_eigen_utilities/yaml_eigen_utilities.hpp>

using namespace YAML;
using namespace common;
using boost::filesystem::path;
using namespace std;
using namespace Eigen;
using namespace yaml_utilities;

struct DomainData
{
    std::string name;
    Eigen::MatrixXd a;
    Eigen::MatrixXd impedparams_stance;
    Eigen::MatrixXd impedparams_swing;
    Eigen::MatrixXd Phi;
};

class Domain{
    DomainData data;
    std::string name;
 public:
    Domain(std::string domainname): name(domainname) {
    }

    string getDomainName(){
        return name;
    }

    DomainData getDomainData(){
        return data;
    }

    void loaddata(){
            cout <<"Load the data of " << name << "\n" << endl;
            data.name = name;

            // load the a_opt data
            string datapath_aopt = "package://ambpros_trajectory/share/a_opt_roll_smallknee_";
            datapath_aopt.append(name);
            datapath_aopt.append(".yaml");

            string filepath1 = roslib_utilities::resolve_local_url(datapath_aopt).string();
            Node node1;
            yaml_read_file(filepath1, node1);
            node1["a_opt"] >> data.a;
//            node1["a_opt"] >> data.Phi;

            // load the impedance parameter
            string datapath_imped = "package://ambpros_controller/share/impedparameters_";
            datapath_imped.append(name);
            datapath_imped.append(".yaml");
            string filepath2 = roslib_utilities::resolve_local_url(datapath_imped).string();
            Node node2;
            yaml_utilities::yaml_read_file(filepath2, node2);
            node2["stance"] >>data.impedparams_stance;
            node2["swing"]  >> data.impedparams_swing;
    }
};

typedef boost::shared_ptr<Domain> DomainPtr;

/**
 * @brief TimeBased Test HZD input (time) against expected output (desired position and velocity)
 */
TEST(ambpros_trajectory, TimeBased)
{
//    std::vector<DomainPtr> domains = {
//            new Domain,
//            new Domain
//        };

//    // vector test
//    std::vector<DomainPtr> domains;

//    domains.push_back(DomainPtr(new Domain("stairclimb")));
//    domains.push_back(DomainPtr(new Domain("downstair")));

//    DomainPtr cur1_domain =  domains[0];
//   DomainPtr cur2_domain =  domains[1];
//   cur1_domain->loaddata();
//   cur2_domain->loaddata();

////     single pointer test
//    DomainPtr pointer_domain(new Domain("levelwalk"));
//   pointer_domain->loaddata();

    Domain cur_domain("stairclimb");
   cur_domain.loaddata();

   string domainname = cur_domain.getDomainName();
   DomainData domaindata = cur_domain.getDomainData();
    cout << domainname << endl;
    cout << domaindata.a(0,0) << endl;
}

int main(int argc, char **argv)
{
    ros::init(argc,argv,"ambpros_tests_cpp");
    ros::NodeHandle n;
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
