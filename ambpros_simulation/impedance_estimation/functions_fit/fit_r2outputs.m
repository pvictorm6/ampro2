%% fit the stance-knee angle based on the motion primitive types. If it's 
%% flatground walking, then fit the data with canonical function; otherwise
%% fit with the extended canonical function

function ret = fit_r2outputs(t,data, nParameter)
addpath('./functions_fit')
%% fit with canonical function
if nParameter == 7
    xinitial    = [-0.7725    7.1861   -0.0623    1.7299    0.0964   15.5704    1.0069];
else
    xinitial    = [-0.1243   10.4883    0.1010    1.9458    0.2179];
end

%%setup the options of fmincon
A               = [];
b               = [];
Aeq             = [];
beq             = [];
lb              = -50*ones(1,length(xinitial));
ub              = 50*ones(1,length(xinitial));

errorbound      = 1e-9;
options         = optimset('Display','off','TolFun',errorbound,'TolCon',...
                        errorbound,'TolX',errorbound,'Algorithm',...
                        'interior-point','MaxIter',500,'MaxFunEvals',3000);
if nParameter == 7
    excanonfit      = fmincon(@cost_excanonical,xinitial,A,b,Aeq,beq,...
                            lb,ub,@mycon,options);
    ret             = excanonfit; 
else
    canonfit        = fmincon(@cost_canonical,xinitial,A,b,Aeq,beq,...
                            lb,ub,@mycon,options);
    ret             = canonfit; 
end


function error   = cost_canonical(coef)    
    yfit     = func_canonical(t,coef);
    error    = norm(yfit - data);
end

function error   = cost_excanonical(coef)
    yfit     = func_excanonical(t, coef);
    error    = norm(yfit - data);
end

function [c,ceq] = mycon(coef)
    c        = [];
    ceq      = [];
end

function [c,ceq] = mycon_ex(coef)
    c        = [];
    ceq      = [];
end
  
end