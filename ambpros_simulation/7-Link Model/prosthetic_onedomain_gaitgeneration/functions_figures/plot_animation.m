function plot_animation(robotwalk, time_scale)
%% animation
% SET THE TIME_SCALE TO HOW MANY TIMES YOU WANT TO SLOW THE ANIMATION DOWN,
% E. G., time_scale = 4 slows down the simulation to 1/4 speed.
px  = 0;
pz  = 0;
for i = 1:length(robotwalk.data)
    if i == 1
        % Initialize animation plot
        figure;
        plot([-1 5], [0 0], 'k-');
        hold on;
        legs   = plot(NaN, NaN, 'b', 'LineWidth', 2);
        prosth = plot(NaN, NaN, 'r', 'LineWidth', 2);
        xterrain = linspace(-1,5,1000);
        % zterrain = zeros(length(xterrain));
        zterrain = linspace(0,-1,1000);
        h = area(xterrain, 0*zterrain,-0.2);
        set(h,'FaceColor',[0 150 50]/255)
        hold off;
        axis equal;
        axis off
    end
   tic
   for k=1:length(robotwalk.data{i}(1,:));
        tnow = 0;
        while (tnow < time_scale*robotwalk.data{i}(1, k))
            tnow = toc;
        end
        % position
        pos = jpos_mat(robotwalk.data{i}(2:end, k));
        set(legs, ...
            'XData', pos(1,6:end) + px, ...
            'YData', pos(2,6:end) + pz, ...
            'erasemode', 'normal');
        set(prosth, ...
            'XData', pos(1,1:6) + px, ...
            'YData', pos(2,1:6) + pz, ...
            'erasemode', 'normal');
        if k == 1
            axis manual
        end
        drawnow; 
   end
   if robotwalk.nBaseDof == 0
        px = pos(1, end);
        pz = 0;
   else
        px = 0;
        pz = 0;
   end

end
end