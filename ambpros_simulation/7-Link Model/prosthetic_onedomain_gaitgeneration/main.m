addpath('./build_torso', './functions_figures', 'classdef')
addpath('./wrappers')

%% clear the data 
clear robotwalk logObj

robotwalk = amberwalk_configuration;
logObj    = amber_data_logger;

%%excute the steps
robotwalk.nSteps = 1;
robotwalk = hybrid_flow(logObj,robotwalk);

%% animation
% profile on
time_scale = 20;
plot_animation(robotwalk, time_scale);
% profile viewer

%% 
plot_simulation(logObj, robotwalk)
