function [UserChoice, OptSet] = ...
    optimization_configuration(robotwalk)

%% set up the user constraints
UserChoice                      = struct;
UserChoice.MU                   = 1000; % friction coefficient
UserChoice.MOMENT_LOWERBOUND    = 1; % lower bound on foot pitch moment balance
UserChoice.MOMENT_UPPERBOUND    = 1; % upper bound on foot pitch moment balance
UserChoice.MAX_VELOCITY         = 10;
UserChoice.MAX_TORQUE           = 300;
UserChoice.MAX_FOOTHEIGHT       = 0.01;
UserChoice.MAX_HEELHEIGHT       = 0.006;
UserChoice.MAX_TOEHEIGHT        = 0.01;
UserChoice.MAX_TORSO            = 0.1;
UserChoice.MIN_SKNEE            = 0.01;
UserChoice.MIN_NSKNEE           = 0.01;
UserChoice.MIN_STEPLENGTH       = 0.22;
UserChoice.SMOOTH_TORQUE        = [4 5];
UserChoice.ep                   = robotwalk.ep;

%% set up the optimal set of the fmincon optimization
OptSet                  = struct;
%%% set the properties
OptSet.ERRORBOUND       = 1e-6;
OptSet.MAX_F_EVALS      = 5000;
OptSet.OPT_ALGORITHM    = 'interior-point';  %'sqp'
OptSet.USE_PARALLEL     = 'always';  %'always'

nOutputs                = robotwalk.nOutputs;
nParameter              = robotwalk.nParameter;

%%%
OptSet.A                = [];
OptSet.b                = [];
OptSet.Aeq              = [];
OptSet.beq              = [];


%%% upper & lower bounds
OptSet.para_bound       = 20;
OptSet.ubmat            = OptSet.para_bound*ones(nOutputs,nParameter);
OptSet.lbmat            = -OptSet.para_bound*ones(nOutputs,nParameter);

%%% hip velocity constraints
% OptSet.ubmat(1,1)       = 0.3;
% OptSet.ubmat(1,2:end)   = 0;
% OptSet.lbmat(1,1)       = 0.1;
% OptSet.lbmat(1,2:end)   = 0;

% reshape bound matrices
OptSet.ub       = OptSet.ubmat(:);
OptSet.lb       = OptSet.lbmat(:);

% fmincon options
OptSet.options  = optimset( ...
                    'Algorithm', OptSet.OPT_ALGORITHM, ...
                    'Display', 'iter', ...
                    'TolX',OptSet.ERRORBOUND, ...
                    'MaxFunEvals', OptSet.MAX_F_EVALS, ...
                    'TolCon',OptSet.ERRORBOUND);

% OptSet.options.OutputFcn    = @stopOutFun;
% OptSet.options.PlotFcns     = {@optimplotx, @optimplotfval};
OptSet.options.UseParallel  = OptSet.USE_PARALLEL;

end