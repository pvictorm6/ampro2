%% read rosbag data logged from experiment
% function read_logdata()
% temp    = load('walking_2014-03-12_18-04.bag')
% end

MOTOR_REDUCTION     = 80;
TORQUE_SENS         = 0.0621;
SAMPLE_RATE         = 2000;

%%desired motor current and joint torque
ankCurrCommand      = resampledata(SAMPLE_RATE, rosbag.control_feedback.control_signal_ankle);
kneeCurrCommand     = resampledata(SAMPLE_RATE, rosbag.control_feedback.control_signal_knee);

ankTorqCommand      = resampledata(SAMPLE_RATE, rosbag.control_feedback.torque_desired_ankle);
kneeTorqCommand     = resampledata(SAMPLE_RATE, rosbag.control_feedback.torque_desired_knee);


%%actual motor current and joint torque
ankCurrFeedback     = resampledata(SAMPLE_RATE, rosbag.control_feedback.torque_actual_ankle/MOTOR_REDUCTION/TORQUE_SENS);
kneeCurrFeedback    = resampledata(SAMPLE_RATE, rosbag.control_feedback.torque_actual_knee/MOTOR_REDUCTION/TORQUE_SENS);

ankTorqFeedback     = resampledata(SAMPLE_RATE, rosbag.control_feedback.torque_actual_ankle);
kneeTorqFeedback    = resampledata(SAMPLE_RATE, rosbag.control_feedback.torque_actual_knee);

%%desired joint angle positions
ankPosReference     = resampledata(SAMPLE_RATE, rosbag.control_feedback.position_desired_ankle);
kneePosReference    = resampledata(SAMPLE_RATE, rosbag.control_feedback.position_desired_knee);

%%actual joint angle positions and velocities
ankPosFeedback      = resampledata(SAMPLE_RATE, rosbag.control_feedback.position_actual_ankle);
kneePosFeedback     = resampledata(SAMPLE_RATE, rosbag.control_feedback.position_actual_knee);

ankVelFeedback      = resampledata(SAMPLE_RATE, rosbag.control_feedback.velocity_actual_ankle);
kneeVelFeedback     = resampledata(SAMPLE_RATE, rosbag.control_feedback.velocity_actual_knee);

%%process the time
logtime_sec         = rosbag.control_feedback.t_mat;
dftime              = diff(logtime_sec);
logtime             = cumsum(dftime);
plottime            = resampledata(SAMPLE_RATE, logtime);
startPoint          = 1;
endPoint            = length(plottime);
plot_range          = startPoint:1:endPoint;



%%
figure(1); clf;
subplot(211)
plot(plottime(plot_range), ankTorqCommand(plot_range), 'b')
hold on
plot(plottime(plot_range), ankTorqFeedback(plot_range), 'r')
title('Ankle joint torque plots')
legend('Desired ankle torque', 'Actual ankle torque')
subplot(212)
plot(plottime(plot_range), kneeTorqCommand(plot_range), 'b')
hold on
plot(plottime(plot_range), kneeTorqFeedback(plot_range), 'r')
legend('Desired knee torque', 'Actual knee torque')
title('Knee joint torque plots')


figure(2); clf;
subplot(211)
plot(plottime(plot_range), ankCurrCommand(plot_range), 'b')
hold on
plot(plottime(plot_range), ankCurrFeedback(plot_range), 'r')
legend('Desired ankle current', 'Actual ankle current')
title('Ankle motor current plots')
subplot(212)
plot(plottime(plot_range), kneeCurrCommand(plot_range), 'b')
hold on
plot(plottime(plot_range), kneeCurrFeedback(plot_range), 'r')
legend('Desired knee current', 'Actual knee current')
title('Knee motor current plots')


figure(3); clf;
subplot(211)
plot(plottime(plot_range), ankPosReference(plot_range), 'b')
hold on
plot(plottime(plot_range), ankPosFeedback(plot_range), 'r')
legend('Desired ankle angle', 'Actual ankle angle')
title('Ankle angle Plots')
subplot(212)
plot(plottime(plot_range), kneePosReference(plot_range), 'b')
hold on
plot(plottime(plot_range), kneePosFeedback(plot_range), 'r')
legend('Desired knee angle', 'Actual knee angle')
title('Knee angle plots')



% %% plot the frequency 
% loopfrequency       = 0.005; %s
% actualfrequency     = mean(dftime);
% 
% figure(4); clf;
% plot(dftime(plot_range))
% hold on
% plot(plot_range, loopfrequency, '--r', 'LineWidth', 3)
% hold on
% plot(plot_range, actualfrequency, '--m', 'LineWidth', 3)



