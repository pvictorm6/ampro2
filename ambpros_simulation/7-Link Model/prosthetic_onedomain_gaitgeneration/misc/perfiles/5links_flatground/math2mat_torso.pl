## Mathematica output file list:
#
@filenames = ("G_vec","C_mat","D_mat","Ge_vec","De_mat","Ce_mat","Ee_mat","Edot_mat","h_sca","phip_sca","jpos_mat","phip_dot_mat","h_dot_mat","deltaphip_sca","deltaphip_dot_mat","vtoe");

## Parse the mathematica output files and write them to disk in a format usable by MATLAB:
#
for ($matnum = 0; $matnum < scalar(@filenames); $matnum++) {
    $filename = @filenames[$matnum];
    open(DAT, "< ./build_torso/$filename") || die("Could not open file: " . $filename);
    @raw_data = <DAT>;
    close(DAT);
    $mat_str = "function x@filenames[$matnum] = @filenames[$matnum](x)\n"; #remove p
    $mat_str .= "x@filenames[$matnum] = ";
     
    foreach $linevar (@raw_data) {
        $linevar =~ s/\},/\};/g;
        $linevar =~ s/\n/ ...\n/g;
        $linevar =~ s/Pi/pi/g;

	$linevar =~ s/Abs/abs/g;
	$linevar =~ s/Sign/sign/g;

        $linevar =~ s/Sec/sec/g;
        $linevar =~ s/Csc/csc/g;
        $linevar =~ s/Cot/cot/g;
        
        $linevar =~ s/Cos/cos/g;
        $linevar =~ s/Sin/sin/g;
        $linevar =~ s/Tan/tan/g;

	$linevar =~ s/E/exp(1)/g;
        
        $linevar =~ s/\[/\(/g;
        $linevar =~ s/\]/\)/g;
        $linevar =~ s/\{/\[/g;
        $linevar =~ s/\}/\]/g;
        
        $mat_str .= $linevar;
    }
    $mat_str .= ";";

    $newfilename = @filenames[$matnum].".m";
    open(DAT, "> ./build_torso/$newfilename") || die "Can't open $newfilename : $!";
    print DAT $mat_str;
    close(DAT);
}

## Mathematica output file list:
#
@filenames = ("ya1_sca", "yd1_sca", "Dya1_mat", "Dyd1_mat", "DLfyd1_mat", "DLfya1_mat","ya2_vec", "yd2_vec", "Dya2_mat", "Dyd2_mat", "DLfyd2_mat", "DLfya2_mat","pcom_sca","sigma_sca");

## Parse the mathematica output files and write them to disk in a format usable by MATLAB:
#
for ($matnum = 0; $matnum < scalar(@filenames); $matnum++) {
    $filename = @filenames[$matnum];
    open(DAT, "< ./build_torso/$filename") || die("Could not open file: " . $filename);
    @raw_data = <DAT>;
    close(DAT);
    $mat_str = "function x@filenames[$matnum] = @filenames[$matnum](x,  p, a)\n"; # remove p
    $mat_str .= "x@filenames[$matnum] = ";
     
    foreach $linevar (@raw_data) {
        $linevar =~ s/\},/\};/g;
        $linevar =~ s/\n/ ...\n/g;
        $linevar =~ s/Pi/pi/g;

	$linevar =~ s/Abs/abs/g;
	$linevar =~ s/Sign/sign/g;

        $linevar =~ s/Sec/sec/g;
        $linevar =~ s/Csc/csc/g;
        $linevar =~ s/Cot/cot/g;
        
        $linevar =~ s/Cos/cos/g;
        $linevar =~ s/Sin/sin/g;
        $linevar =~ s/Tan/tan/g;

	$linevar =~ s/E/exp(1)/g;
        
        $linevar =~ s/\[/\(/g;
        $linevar =~ s/\]/\)/g;
        $linevar =~ s/\{/\[/g;
        $linevar =~ s/\}/\]/g;
        
        $mat_str .= $linevar;
    }
    $mat_str .= ";";

    $newfilename = @filenames[$matnum].".m";
    open(DAT, "> ./build_torso/$newfilename") || die "Can't open $newfilename : $!";
    print DAT $mat_str;
    close(DAT);
}

