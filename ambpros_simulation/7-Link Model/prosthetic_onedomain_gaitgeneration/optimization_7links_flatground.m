function [x_opt,a_opt] = optimization_7links_flatground
%%%%%%%%%%%%%Add paths to mathematica compiled files:
addpath('./build_torso', './buildopt_torso', './functions_fit',...
    './wrappers', './human_data', './utilities', './classdef',...
    './functions_optimization')

%% configure the optimization
clear robotwalk
robotwalk   = amberwalk_configuration;
[USERCHOICE, OPTSET] = optimization_configuration(robotwalk);
humandata   = optimization_loaddata(robotwalk);

%% trying different initial guess
a0      = humandata.a_fit;
a0(end,:) = 0;

temp    = load('a_opt_roll_jake_goodclearance.mat');
a0      = temp.a_opt;

temp    = load('a_opt_roll_jake_goodclearance.mat');
humandata.a_reference   = temp.a_opt;

a_ic    = flatten(a0);
%%%%% Optimization

[a_out,~] = fmincon(@(a)cost(a,humandata,robotwalk),a_ic,OPTSET.A,OPTSET.b,...
                            OPTSET.Aeq,OPTSET.beq,OPTSET.lb,OPTSET.ub,...
                            @(a)mycon(a, USERCHOICE,robotwalk),...
                            OPTSET.options);

%%%%%%%%%%%%%% Output the parameter matrix and the fit matrix:
a_opt   = unflatten(a_out, robotwalk);
%%%%%%%%%%%%%% Output the initial condition:
q       = inverse_kin(a_opt);
qdot    = [0;0;0;H_minus(q,a_opt)\[a_opt(1,1); 0; 0; 0; 0; 0]];
x_opt   = [q; qdot];

% %%%%%%%%%%%%%% Save initial and final condition
save('x_opt_roll_jake2.mat','x_opt')
save('a_opt_roll_jake2.mat','a_opt')
end

function ret = cost(aflat,humandata, robotwalk)

typeofcost  = 1;
mean_time   = humandata.mean_time;
mean_hippos = humandata.mean_hippos;
mean_sknee  = humandata.mean_sknee;
mean_nsknee = humandata.mean_nsknee;
mean_nsslope= humandata.mean_nsslope;
mean_torso  = humandata.mean_torso;
mean_nsfoot = humandata.mean_nsfoot;
a_reference = humandata.a_reference;

%%%%%%%%%%%%%% Unflatten the a matrix
a = unflatten(aflat, robotwalk);

if typeofcost == 1
    
    time_scale = mean_time;

    cost_ns_slope   =  (yd_ns_slope(time_scale,a_reference)-yd_ns_slope(time_scale,a)).^2;
    cost_hip_pos    =  (yd_hip_pos(time_scale,a_reference)-yd_hip_pos(time_scale,a)).^2;
    cost_s_knee     =  (yd_s_knee(time_scale,a_reference)-yd_s_knee(time_scale,a)).^2;
    cost_ns_knee    =  (yd_ns_knee(time_scale,a_reference)-yd_ns_knee(time_scale,a)).^2;
    cost_ns_foot    =  (yd_ns_foot(time_scale,a_reference)-1*yd_ns_foot(time_scale,a)).^2;

    Ynorm           = cost_hip_pos + cost_ns_slope + cost_s_knee + ...
                        cost_ns_knee + cost_ns_foot;
    YnormInt        = trapz(time_scale, Ynorm);
    ret             =  YnormInt ;

elseif typeofcost == 2

%%%%%%%%%%%%%% Cost for each canonical function
    cost_hip_pos   = sum(abs(mean_hippos - yd_hip_pos(mean_time,a)));
    cost_s_knee    = sum(abs(mean_sknee - yd_s_knee(mean_time,a)));
    cost_ns_knee   = sum(abs(mean_nsknee - yd_ns_knee(mean_time,a)));
    cost_torso     = sum(abs(mean_torso - yd_torso(mean_time,a)));
    cost_ns_slope  = sum(abs(mean_nsslope - yd_ns_slope(mean_time,a)));
    cost_ns_foot   = sum(abs(mean_nsfoot - yd_ns_foot(mean_time,a)));
%%%%%%%%%%%%%% Wieghting for the fits
    beta_hip_pos   = 1/(max(mean_hippos)- min(mean_hippos));
    beta_s_knee    = 1/(max(mean_sknee)- min(mean_sknee));
    beta_ns_knee   = 1/(max(mean_nsknee)- min(mean_nsknee));
    beta_ns_slope  = 1/(max(mean_nsslope)- min(mean_nsslope));
    beta_ns_foot   = 1/(max(mean_nsfoot)- min(mean_nsfoot));
%     beta_ns_foot   = 0;
    
    if (max(mean_torso) - min(mean_torso))
        beta_torso  = 0.01/(max(mean_torso)- min(mean_torso));
    else
        beta_torso  = 1;
    end
    beta           = beta_ns_slope + beta_hip_pos + beta_s_knee + ...
                     beta_ns_knee + beta_ns_foot;% + beta_torso;

    cost_all = [
          beta_ns_slope*cost_ns_slope,...
          beta_hip_pos*cost_hip_pos,...
          beta_s_knee*cost_s_knee,...
          beta_ns_knee*cost_ns_knee,...
          beta_ns_foot*cost_ns_foot
          ];

    %%%%%%%%%%%%%% Return the cost
    ret = sum(cost_all)/beta;
end

end

%%%%%%%%%%%%%% Constraints to enforce HZD (or partial HZD)
function [c,ceq] = mycon(aflat, USERCHOICE,robotwalk)

%%%%%%%%%%%%%% Unflatten the a matrix
ndof        = robotwalk.nDof;
nbasedof    = robotwalk.nBaseDof;
nexdof      = ndof + nbasedof;
a           = unflatten(aflat,robotwalk);
ep          = USERCHOICE.ep;

%%%%%%%%%%%%%% Determine the initial condition (on the guard) for the
%%%%%%%%%%%%%% current parameters
q_minus     = inverse_kin(a);
qdot_minus  = [0;0;0;H_minus(q_minus,a)\[a(1,1); 0; 0; 0; 0; 0]];
x_minus     = [q_minus; qdot_minus];

x_plus      = R1_map(x_minus,1);
phip_ic     = deltaphip_sca(x_plus);
phip_fc     = deltaphip_sca(x_minus);

%%%%%%%%%%%%%% H is the post impact velocities. 
H           = H_plus(x_plus,a)*x_plus(nexdof + nbasedof + 1:end);
Hzd         = H(2:end,:) - [0; 0; 0; 0; 0];

%%%foot height condition
[~, tau]    = find_fixed(a, ep, x_plus, phip_ic, phip_fc); 
% tau         = (phip_fc - phip_ic)/a(1, 1);

if ~isreal(tau)||isempty(tau)|| tau <=0 || tau >1.5
    [c,ceq] = deal(100);
    return
end

[Umax,minsknee,maxtorso,minnsknee,maxnsknee, maxnsankle, maxfootroll,max_nsslope,...
        maxPower,hcondmin_foot, hcondmin_toe, hcondmin_heel,...
        flatMomentCond, velocityCond, endZ] = ...
                                minmaxcons(x_plus, tau, a, USERCHOICE);

%%to compute the strike length
joint_pos   = jpos_mat(x_plus);
stankle_pos = joint_pos(1,4);
swankle_pos = joint_pos(1,10);

foot_space  = stankle_pos - swankle_pos;

%% reduce the velocity jump between stance and swing
veljump_ankle = abs(x_plus(end) - x_minus(end));
veljump_knee  = abs(x_plus(end-1) - x_minus(end-1));
%% torque smooth constraints
% u_f     = torquesmooth(x_minus,phip_ic,a);
% u_0     = torquesmooth(x_plus,phip_ic,a);
% Relabel = fliplr(eye(length(x_plus)/2));
% tdiff   = u_0-Relabel*u_f;

    %%%%%%%%%%%%%% Nonlinear inequalities
    %%%%%%%%%%%%%%%%% gaurd must be negative
    %%%%%%%%%%%%%%%%% Time must be less than the human time
    c = [
           h_dot_minus(q_minus)*qdot_minus(4: end)+.01;
%            -minsknee+ USERCHOICE.MIN_SKNEE;
%            -minnsknee+ USERCHOICE.MIN_NSKNEE;
           -10*hcondmin_heel;
           -10*hcondmin_toe;
           10*(maxnsknee - 1.1);
           Umax-USERCHOICE.MAX_TORQUE;
           
%            abs(maxfootroll) - 0.1 
           10*(maxnsankle - 0.5);
%            maxtorso - USERCHOICE.MAX_TORSO;
%            flatMomentCond;
%            velocityCond - USERCHOICE.MAX_VELOCITY;
%            veljump_ankle - 5;
%            veljump_knee - 5; 
           
           -foot_space + USERCHOICE.MIN_STEPLENGTH;
        ];
    
%%%%%%%%%%%%%% Nonlinear equalities
%%%%%%%%%%%%%%%%% y pre impact must be zero
%%%%%%%%%%%%%%%%% y dot post impact must be zero
%%%%%%%%%%%%%%%%% guard must be satisfied
ceq = [
       10*yd_ns_foot(0,a);
       10*yd_ns_foot(tau,a);
       y_minus(q_minus,a);
       Hzd;     
%        tdiff(USERCHOICE.SMOOTH_TORQUE)
       ];

   
end

function [umaxcond,minsknee,maxtorso,minnsknee,maxnsknee, maxnsankle, maxfootroll,max_nsslope,...
    maxPower,hcondmin_foot, hcondmin_toe, hcondmin_heel, flatMomentCond, velocityCond, endZ] = ...
    minmaxcons(x_plus, tau, a, USERCHOICE)

    nexdof  = 9;
    ep      = USERCHOICE.ep;
    hmax    = USERCHOICE.MAX_FOOTHEIGHT;
    hmax_t  = USERCHOICE.MAX_TOEHEIGHT;
    hmax_h  = USERCHOICE.MAX_HEELHEIGHT;
    lh      = 0.07;
    lt      = 0.18;
   
    p0      = deltaphip_sca(x_plus);
    dx_plus = x_plus(10:end,1);    
    p_dot0  = deltaphip_dot_mat(x_plus)*dx_plus(4:end);
    
    t_list  = [0:0.01:tau, tau];
    N       = length(t_list);
    
    %%initialize the constriaints
    %%angle constraints
    sknee        = zeros(1,N);
    nsknee       = zeros(1,N);
    maxtorso     = zeros(1,N);
    maxfootroll  = zeros(1,N);
    max_nsslope  = zeros(1,N);
    
    %%height constraints
    hcond_foot   = zeros(1,N);
    hcond_toe    = zeros(1,N);
    hcond_heel   = zeros(1,N);
    
    %%torque and moment constraints
    U            = zeros(1,N);
    Pow          = zeros(1,N);
    stFootConds  = zeros(3,N);
    velocities   = zeros(1,N);
    
    foot_pos_x   = zeros(1,N);
    foot_pos_y   = zeros(1,N);
    nsfz         = zeros(1,N);
    nsfx         = zeros(1,N);
    qh           = zeros(1,N);

    x_recons     = zeros(2*nexdof, N);  
                   
        
    for i = 1:N
        t               = t_list(i);
        z               = [a(1,1)*t + p0 + ((1-exp(-t*ep))*...
                            p_dot0+(-1+exp(-t*ep))*a(1,1))/ep;...
                            a(1,1)+ exp(-t*ep)*(p_dot0-a(1,1))];       
        q               = [0; 0; 0; Phi1_inverse_mat(z,p0,a)];        
        qdot            = [0; 0; 0; Phi1_dot_inverse_mat(z,p0,a)];
        x_recons(:,i)   = [q; qdot];
                                           
        xtemp           = x_recons(:,i);

        posfoot         = footpos_mat(q);
        foot_pos_x(i)   = posfoot(1,end);
        foot_pos_y(i)   = posfoot(2,end);  
        
        posbody         = jpos_mat(q);
        toe_pos_y       = posbody(2,end - 2);
        heel_pos_y      = posbody(2,end - 1);
%         
%         %%compute the ZMP, torque and power constraints
        [Fv, utemp]     = Fv_u_calc(xtemp,a,p0,ep);
%         
%         YTorqueFront    = lt*Fv(2,:);
%         YTorqueBack     = -lh*Fv(2,:);
%         % friction
%         friction        = sqrt(Fv(1,:).^2) - USERCHOICE.MU*Fv(2,:);
%         % pitch
%         flat_pitch_cond = [ YTorqueBack+USERCHOICE.MOMENT_LOWERBOUND - Fv(3,:);
%                             Fv(3)+USERCHOICE.MOMENT_UPPERBOUND- YTorqueFront];
%         fconds          = [friction;
%                            flat_pitch_cond];
%         stFootConds(:,i)= fconds;
%         if sum(utemp) > 1000
%             utemp = 10000;
%             break
%         end
        U(i)            = max(abs(utemp));
        Pow(i)          = max(abs(xtemp(nexdof+4:end).*utemp));
        
        
        %%compute the outputs constraints
        maxfootroll(i)  = abs(yd_ns_foot(t,a));        
        sknee(i)        = yd_s_knee(t,a);
        nsknee(i)       = yd_ns_knee(t,a);
        maxtorso(i)     = abs(yd_torso(t,a));
        max_nsslope(i)  = abs(yd_ns_slope(t,a));      
        velocities(i)   = max(abs(xtemp(nexdof+1:end)));
        
        %%compute the height and position constraints
%         deltax          = foot_pos_x(i) - min(foot_pos_x);
%         qheight         = quad_heightDecay(deltax,hmax,max(foot_pos_x)- ...
%                             min(foot_pos_x));
%         qheight         = quad_heightDecay(t,hmax,tau);
        qheight         = quad_height(t,hmax,tau);
        hcond_foot(i)   = foot_pos_y(i) - qheight;     
        qheight_T       = quad_height(t,hmax_t,tau);
        hcond_toe(i)    = toe_pos_y - qheight_T;
        qheight_H       = quad_height(t, hmax_h, tau);
        hcond_heel(i)   = heel_pos_y - qheight_H;
         
        nsfz(i)         = foot_pos_y(i);
        nsfx(i)         = foot_pos_x(i);
        qh(i)           = qheight;        
    end
    
    
    
    maxfootroll         = max(maxfootroll);
    maxtorso            = max(maxtorso);       
    minsknee            = min(sknee);
    minnsknee           = min(nsknee);
    maxnsknee           = max(nsknee);
    maxnsankle          = max(abs(x_recons(9,:)));
    max_nsslope         = max(max_nsslope);
    
    
    umaxcond            = max(U);
    maxPower            = max(max(Pow));
%     flatMomentCond      = max(max(stFootConds));
%     velocityCond        = max(max(abs(velocities))); 
%     umaxcond            = 0;
%     maxPower            = 0;
    flatMomentCond      = 0;
    velocityCond        = 0; 
    
    hcondmin_foot       = min(hcond_foot);  
    hcondmin_toe        = min(hcond_toe);
    hcondmin_heel       = min(hcond_heel);
    endZ                = h_sca(x_recons(:,end));     
end

function h = quad_height(t,hmax,Ttau)

a = -4*hmax/(Ttau^2);
b = 4*hmax/Ttau;

h = a*t^2 + b*t;
end

%% %%%%%%%%%%%% Canonical human functions

function ret = yd_hip_pos(t,a)

ret = a(1,1)*t;

end

function ret = yd_s_knee(t,a)
    ret = func_canonical(t,a(2,:));
%     ret = func_excanonical(t,a(2,:));
end

function ret = yd_ns_knee(t,a)
    ret = func_canonical(t,a(3,:));
%     ret = func_excanonical(t,a(3,:));
end

function ret = yd_ns_slope(t,a)
    ret = func_canonical(t,a(4,:));
%     ret = func_excanonical(t,a(4,:));
end

function ret = yd_torso(t,a)
    ret = func_canonical(t,a(5,:));
%     ret = func_excanonical(t,a(5,:));
end

function ret = yd_ns_foot(t,a)
    ret = func_canonical(t,a(6,:));
%     ret = func_excanonical(t,a(6,:));
end


function aflat = flatten(a)
    aflat = a(:);
end

function aunflat = unflatten(aflat, robotwalk)
    nOutputs    = robotwalk.nOutputs;
    nParameter  = robotwalk.nParameter;
    aunflat     = reshape(aflat,nOutputs,nParameter);
end

%%%%%%%Computing the position at impact through inverse kinematics

function ret = inverse_kin(a)

    typeofsolve = 2;

    if typeofsolve == 1    
        %%%%%%%%%%%%%%%%%%%%% Numeric inverse kinematics
        q(2) = yd_ns_knee(0,a);
        q(5) = yd_s_knee(0,a);
        q(3) = yd_ns_slope(0,a) + yd_torso(0,a) - 0.39883*q(2);
        q(1) = - 2*yd_ns_slope(0,a) - yd_ns_foot(0,a) -yd_torso(0,a) ...
               - 0.20234*q(2) + q(3);

        qtemp = zeros(2,1);
        options = optimset('Display','off','Algorithm','Levenberg-Marquardt');
        q46   = fsolve(@myfun,qtemp,options);
        q(4)  = q46(1);
        q(6)  = q46(2);
        ret   = q;
    elseif typeofsolve == 2
            %%%%If using the old version (no torso) use this:
            q    = zeros(6,1);
            q(2) = theta_a2(q,a);
            q(5) = theta_a5(q,a);
            q(3) = theta_a3(q,a);
            q(1) = theta_a1(q,a);
            q(4) = theta_a4(q,a);
            q(6) = theta_a6(q,a);
            
            ret  = [0;0;0;q];
    end
    
    function ret = myfun(qtemp)
        ret1 = qtemp(1) + q(5) + qtemp(2) - yd_torso(0,a);
           q = [q(1);q(2);q(3);qtemp(1);q(5);qtemp(2)];
        ret2 = h_sca(q);        
%           rq = R(q);    
%         ret3 = h_sca(rq);
        ret  = [ret1;
                ret2;
%                 ret3
                ];
    end
end

function ret=torquesmooth(x,p,a)
    [~, ret]  =  Fv_u_calc(x, a, p, 0);
end
