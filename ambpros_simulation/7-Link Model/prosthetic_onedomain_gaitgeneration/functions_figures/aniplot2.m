%% aniplot(data)
%
%  Description:
%   Generates an animated plot showing the motion of the system with
%   additional plots showing the phase portrait and angle and angle
%   velocities measured over time.
%
%  Operational Description:
%   Plots the angle/angle velocity and phase portrait graphs.  Creates
%   markers as plot objects that match up with animation graph, updating
%   every time the animation graph is updated.  The animation is a plot
%   object containing points for the end of each link.  This is updated for
%   every data point to generate an animation.
%
%  Arguments:
%   data    - A struct containing a an 'integral_curves' member.  This
%             member holds the results of integration returned by
%             hybrid_flow().
%
%  Return Values:
%   None.
%
function aniplot2(data, varargin)
%% Fit plots to screen.
%
screenSize = get(0, 'ScreenSize');
width = screenSize(3);
height = screenSize(4);

% Set default options
%
opts.makeavi = false;
opts.extragraphs = true;
opts.moviefile = 'amber2d.avi';
opts.framerate = 60;

% Parse varargin
for k = 1:size(varargin, 2)/2
    switch lower(varargin{2*k - 1})
        case 'movie'
            if strcmpi(varargin{2*k},'avi')
                opts.makeavi = true;
            end

        case 'moviefile'
            opts.moviefile = varargin{2*k};
            
        case 'extragraphs'
            if ~varargin{2*k}
                opts.extragraphs = false;
            end
            
        case 'framerate'
            if ~(varargin{2*k} - floor(varargin{2*k})) && ...
                    isscalar(varargin{2*k}) %#ok<BDLOG>
                opts.framerate = varargin{2*k};
            end
            
        otherwise
            error('No such option.');
    end
end

if ~opts.extragraphs
    opts.phasemarkers = false;
    opts.anglemarkers = false;
end



% Parse input.
%
curves = cat(2, data{:});
% t = cat(2, data.time(:));
t = curves(1,:);
q = curves(2:end,:);

dim = size(q, 1);
ndof=dim/2;
posv = (1:floor(dim/2));
vel = (floor(dim/2)+1:dim);

%% Setup the legends.
%
names = cell(1, dim);

% domainnames = {'\mathit{sf}','\mathit{sc}','\mathit{st}', ...
%     '\mathit{nst}','\mathit{nsc}','\mathit{nsf}'};

% for n=1:length(posv)
%     names{posv(n)} = ['$\theta_' domainnames{n} '$'];
%     names{vel(n)} = ['$\dot{\theta}_' domainnames{n} '$'];
% end

for n=1:length(posv)
    names{posv(n)} = ['$q_' num2str(n) '$'];
    names{vel(n)} = ['${\dot q}_' num2str(n) '$'];
end
linespecs = {'k.', 'k.', 'k.', 'k.', 'k.', 'k.', 'k.'};

%% Plot the phase portraits.
%
fig = figure(1); clf; hold off;
if opts.makeavi
    set(fig,'DoubleBuffer','on');
end

set(fig,'MenuBar','none')
qa=q(1:ndof,:);
qd=q(ndof+1:end,:);
qmin=(min(qa(:))-0.1*abs(min(qa(:))));
qmax=(max(qa(:))+0.1*abs(max(qa(:))));
qdmin=  (min(qd(:))-0.1*abs(min(qd(:))));
qdmax=  (max(qd(:))+0.1*abs(max(qd(:))));
    
        
set(fig, 'Position', [0 50 1280 720]);
if opts.extragraphs
    subplot(221); plot(q([1 2 3 4 5], :)', q([6 7 8 9 10], :)');
    axis([qmin qmax qdmin qdmax])
    legend(strcat(names(posv)), 'Interpreter', 'LaTeX', ...
        'Location', 'BestOutside', 'Orientation', 'Horizontal')
    legend boxoff
    
     hold on;
    th = NaN(5, 1);
    for i = 1:floor(dim/2)
        th(i) = plot([0 0], linespecs{i}, 'MarkerSize', 12);
    end
end
%% Plot the angles over time.
%

if opts.extragraphs
    subplot(422); plot(t, q([1 2 3 4 5],:))
    legend(strcat(names(posv)), 'Interpreter', 'LaTeX', ...
    'Location', 'SouthOutside', 'Orientation', 'Horizontal')
    legend boxoff
    axis([0 t(end) qmin qmax]);
    hold on;
    tha = NaN(floor(dim/2), 1);
    for i = 1:floor(dim/2)
        tha(i) = plot([0 0], linespecs{i}, 'MarkerSize', 12);
    end
    
    subplot(424); plot(t, q([6 7 8 9 10],:))
    legend(strcat(names(vel)), 'Interpreter', 'LaTeX', ...
        'Location', 'SouthOutside', 'Orientation', 'Horizontal')
    legend boxoff
    axis([0 t(end) qdmin qdmax])
     hold on;
    thb = NaN(floor(dim/2), 1);
    for i = 1:floor(dim/2)
        thb(i) = plot([0 0], linespecs{i}, 'MarkerSize', 12);
    end
end

%% Setup the plot for the animation.
%
if opts.extragraphs
    subplot(212);
end
hold on;
xT=jpos_mat(qa(:,end));
axis([-0.5 (length(data)+2)*xT(1,end)+0.5 -.1 0.8]);
axis equal;axis manual;
origin = [0 0 ];
% linespecs = {'ro', 'bo', 'go', 'mo', 'yo', 'ko'};
ground = line([-100  100], [-100*tan(2*pi/180) 100*tan(2*pi/180)]);
set(ground,'Color', 'k');
hold on

%% Setup up the plot objects for the legs.
%
if opts.extragraphs
    subplot(212); 
end
hold on;
% plot3(-2:.01:5, -getrobotparameter('s') * (-2:.01:5),'b:');
grid on
% legs   = plot3(0, 0, 0);
torso  = plot(0, 0);
sfoot  = plot(0, 0);
nsfoot = plot( 0, 0);

% view(41, 20);
% view(0,0);
% axis([origin(1)+[-1 8] origin(3)+[-1.5 1.5]]);
% axis auto
% axis([-1 3 -.5 3.5]);
% %% Setup plot objects for the angle graphs.
% %
% subplot(221); hold on;
% th = NaN(floor(dim/2), 1);
% for i = 1:floor(dim/2)
%     th(i) = plot([0 0], linespecs{i}, 'MarkerSize', 8);
% end
% 

% subplot(422); hold on;
% tha = NaN(floor(dim/2), 1);
% for i = 1:floor(dim/2)
%     tha(i) = plot([0 0], linespecs{i}, 'MarkerSize',8);
% end
% 
% subplot(424); hold on;
% thb = NaN(floor(dim/2), 1);
% for i = 1:floor(dim/2)
%     thb(i) = plot([0 0], linespecs{i}, 'MarkerSize',8);
% end


if opts.makeavi
    set(gca, 'NextPlot', 'Replace', 'Visible', 'Off')
    mov = avifile(opts.moviefile, 'Compression', 'None', ...
        'fps', opts.framerate);
    nmod = 1;
end
px = 0;
py = 0;
k = 0;
tl = 0;
tic
pospos = [0 0 0 0 0 0; 0 0 0 0 0 0];
%% Precompute plot positions.
%

% data.integral_curves


for i=1:length(data)
%      pos = xpos{i};   
    
    %% Loop through all the data points in each discrete state.
    %
    for j = 1:size(data{i},2)
        
%         if (data.time{i}(j) < toc)
%             continue
%         end
%         l = 1;
%         while data.time{i}(j) > toc + .01 * l
%             pause(.01)
%             l = l + 1;
%         end
%         
        k = k + 1;
        if opts.makeavi
            if mod(k, nmod) ~= 0
                continue;
            end
        end
        
        if data{i}(1,j) < tl + 1/opts.framerate;
        continue;
        else
        tl = tl + 1/opts.framerate;
        end
        
        % Update the positions of the legs and feet.
        %

%         x_l = origin(1) + pos(1,  [2 4:5 8:9 11], j);
%         y_l = origin(2) + pos(2,  [2 4:5 8:9 11], j);
%         z_l = origin(3) + pos(3,  [2 4:5 8:9 11], j);
        
%         x_t = origin(1) + pos(1,  6:7, j);
%         y_t = origin(2) + pos(2,  6:7, j);
%         z_t = origin(3) + pos(3,  6:7, j);
%          pospos = Pos(0,[px;py;data.integral_curves{i}(:,j)]);

         
        xops=data{i};
        
        pos=jpos_mat(xops(2:end,j));
        x_st = origin(1) + pos(1,  1:4);
        y_st = origin(2) + pos(2,  1:4);
         
        x_t = origin(1) + pos(1,  4:5);
        y_t = origin(2) + pos(2,  4:5);

        x_sw = origin(1) + pos(1,  5:7);
        y_sw = origin(2) + pos(2,  5:7);
   
        
%         z_st = origin(3) + pos(3,  1:3,  j);

%         x_sw = origin(1) + pos(1, 10:12, j);
%         y_sw = origin(2) + pos(2, 10:12, j);
%         z_sw = origin(3) + pos(3, 10:12, j);
        
%         if k == 1
%             axis auto
%             axis equal
%         end
%         
        %% Replot the legs.
        %
            a1 = xops(2,j);
            a2 = xops(3,j);
            a3 = xops(4,j);
            a4 = xops(5,j);
            a5 = xops(6,j);
            a6 = xops(7,j);
            a7 = xops(8,j);
            a8 = xops(9,j);
            a9 = xops(10,j);
            a10 = xops(11,j);

%         for ell = 1:floor(dim/2)
           set(th(1:5), 'XData', [a1, a2, a3, a4, a5]   , ...
               'YData', [a6, a7, a8, a9, a10], ...
               'EraseMode', 'normal');
           set(tha(1:5), 'XData', [xops(1,j), xops(1,j), xops(1,j),xops(1,j),xops(1,j)], ...
               'YData', [a1, a2, a3, a4, a5], ...
               'EraseMode', 'normal');
           set(thb(1:5), 'XData', [xops(1,j), xops(1,j), xops(1,j),xops(1,j),xops(1,j)], ...
               'YData', [a6, a7, a8, a9, a10], ...
               'EraseMode', 'normal');
%        end

        if opts.extragraphs
            subplot(212);
        end
        
%         set(legs,   'XData', x_l,  'YData', y_l, 'EraseMode', 'normal');
        set(torso,  'XData', x_t,  'YData', y_t, 'EraseMode', 'normal','Color','BLACK','LineWidth',3);
        set(sfoot,  'XData', x_st, 'YData', y_st, 'EraseMode', 'normal','Color','BLACK','LineWidth',3);
        set(nsfoot, 'XData', x_sw, 'YData', y_sw, 'EraseMode', 'normal','Color','RED','LineWidth',3);

        drawnow;
        if opts.makeavi
            if mod(k, nmod) == 0
                F = getframe(gcf);
                mov = addframe(mov,F);
            end
        end
    end
%     pause
    
    %% Update the position of the stance foot if necessary, i.e., the event
    %  that occured is a foot impact.
    %
      px = pos(1,7,end);
        py = pos(2,7,end); 
%       end
    origin = origin + [px, py];
%     if data.events(i) == 2
%         origin = origin + pos(3,:)';
%     end
%     pause
end

if opts.makeavi
    
        mov=close(mov);
%         movie2avi(mov,opts.moviefile,'Compression','i420');
end

end