/*
 * Error.h
 *
 *  Created on: Oct 14, 2012
 *      Author: eacousineau
 */

#ifndef AMBER_CORE_ERROR_H_
#define AMBER_CORE_ERROR_H_

#include <string>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <sys/types.h>

namespace Amber
{
	namespace Core
	{

class assert_error : public std::runtime_error
{
        std::string expr;
        std::string file;
	unsigned int line;
	std::string func, msg;
	std::string what_arg;
public:
	assert_error(const std::string &expr, const std::string &file, unsigned int line, const std::string &func, const std::string &msg = "");
	~assert_error() throw();
	const char* what() const throw();
};

	}
}

// Assert with exception
	// Rename to amber_assert or something?
#define assert_ex_msg(expr, msg) if (!(expr)) throw Amber::Core::assert_error(#expr, __FILE__, __LINE__, __PRETTY_FUNCTION__, msg)
#define assert_ex(expr) assert_ex_msg(expr, "")


#endif /* AMBER_CORE_ERROR_H_ */
