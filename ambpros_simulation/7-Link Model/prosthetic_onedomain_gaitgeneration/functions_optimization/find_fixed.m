function [p_dot_end,T_end] = find_fixed(a, ep, x_0, p_0, p_f)
dimq    = length(x_0)/2;
vhip    = a(1,1);
z1p     = p_0;
z1m     = p_f;

dx_0    = x_0(dimq+1:end);
p_dot_0 = deltaphip_dot_mat(x_0)*dx_0(4:end);
z2p     = p_dot_0;

z2m_solve   = (1 + lambertw((exp(-1 + (z2p - z1m*ep + z1p*ep)/vhip)*(z2p...
                - vhip))/vhip))*vhip;
T_end       = 1/(ep*vhip)*(-z2p + (z1m - z1p)*ep+ z2m_solve);
p_dot_end   = z2m_solve;

end
