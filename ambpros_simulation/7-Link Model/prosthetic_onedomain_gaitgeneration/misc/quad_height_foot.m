function h = quad_height_foot(q,hmax,q0,qf)

a = -((4*hmax)/(q0 - qf)^2);
b = (4*hmax*(q0 + qf))/(q0 - qf)^2;
c = -((4*hmax*q0*qf)/(q0 - qf)^2);

h = a*q.^2 + b*q + c;
end
