function ret = f1_vector_voltage(t,x,tp0,logObj,robotwalk)

ep    = 1;
a     = robotwalk.a_opt{robotwalk.dr,1};
p     = robotwalk.p;
ndof  = robotwalk.nDof;

dx = x(ndof+1:end);
% x
% pause(1000)
M    = D_mat(x);
C    = C_mat(x);
G    = G_vec(x);

%%%Outputs
y_a1     = ya1_sca(x, p, a);
y_a2     = ya2_vec(x, p, a);
y_d1     = yd1_sca(x, p, a);
y_d2     = yd2_vec(x, p, a);

%%%Outputs
y1 = y_a1 - y_d1;
y2 = y_a2 - y_d2;

y_a = [y_a1; y_a2];
y_d = [y_d1; y_d2];

%%%Jacobians of Outputs
Dy_a1    = Dya1_mat(x, p, a);
Dy_d1    = Dyd1_mat(x, p, a);
Dy_a2    = Dya2_mat(x, p, a);
Dy_d2    = Dyd2_mat(x, p, a);

Dy_1 = Dy_a1 - Dy_d1;
Dy_2 = Dy_a2 - Dy_d2;

%%%Control Fields
vf    = [dx; M \ (-C*dx - G)];
B_IO  = eye(ndof);
gf    = [zeros(size(B_IO)); M \ B_IO];

%%%Lie Derivatives
Lgy1 = Dy_1*gf;
Lgy2 = Dy_2*gf;
Lfy1 = Dy_1*vf;
Lfy2 = Dy_2*vf;

ep2=1;
Vdo=[-ep*y1, 0; -ep*y2, -ep2*Dy_2*x];
Ra   = 1.27;
Kphi = 2.89;  
Kmg  = 2.89; 
    
dig2vol = 24/1000;
ap1     = -2200*dig2vol; 
ap2     = 2000*dig2vol; 
ap3     = 1200*dig2vol;
ap4     = 2000*dig2vol;  
ap5     = 1600*dig2vol; 
ap6     = -10000*dig2vol;

ad1     = 0; 
ad2     = -100*dig2vol; 
ad3     = 80*dig2vol;
ad4     = 120*dig2vol;  
ad5     = 60*dig2vol; 
ad6     = -20*dig2vol;

%%it gives stable walk but the foot will go below the ground
% ap1= -800*dig2vol; 
% ap2= 600*dig2vol; 
% ap3= 1600*dig2vol;
% ap4= 1600*dig2vol;  
% ap5= 2000*dig2vol; 
% ap6= -1600*dig2vol;
% 
% ad1= 0; 
% ad2= -80*dig2vol; 
% ad3= 80*dig2vol;
% ad4= 120*dig2vol;  
% ad5= -120*dig2vol; 
% ad6= 120*dig2vol;

%%back up
% ap1= -800*dig2vol; 
% ap2= 1200*dig2vol; 
% ap3= 1200*dig2vol;
% ap4= 1600*dig2vol;  
% ap5= 1600*dig2vol; 
% ap6= -2000*dig2vol;
% 
% ad1= 0; 
% ad2= 80*dig2vol; 
% ad3= 80*dig2vol;
% ad4= 120*dig2vol;  
% ad5= 120*dig2vol; 
% ad6= -20*dig2vol;


Tauin = Kphi*([ap1* Vdo(1,1) + ad1* Vdo(1,2);...
               ap2* Vdo(2,1) + ad2* Vdo(2,2);...
               ap3* Vdo(5,1) + ad3* Vdo(3,2);...
               ap4* Vdo(4,1) + ad4* Vdo(4,2);...
               ap5* Vdo(3,1) + ad5* Vdo(5,2);...
               ap6* Vdo(6,1) + ad6* Vdo(6,2)] - ...
               Kmg*x(ndof+1:end))/Ra; 


ret = vf + gf * Tauin;

tp  = t + tp0;
Pow = Tauin.*dx;
logObj.append(tp,tp0,x,robotwalk.dr,y_a,y_d,Tauin,Pow);
end