#ifndef AMBPROS_COMMON_HPP
#define AMBPROS_COMMON_HPP

#include <ambpros_controller/ambpros_controller.hpp>
#include <ambpros_can_driver/control_feedback.h>
#include <ambpros_trajectory/ambpros_trajectory.hpp>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Eigen>
#include "ambcap_pros.h"
#include "std_srvs/Empty.h"


class AmbprosCommon{
public:
    int control_type_flag; /* torque */

    int number_drivers;
    int init_flag;

    int track_mode;
    int control_method;

    Eigen::VectorXd control_signal;
    Eigen::VectorXd init_pos;
    Eigen::VectorXd vel_ref;
    Eigen::VectorXd tor_ref;
    Eigen::VectorXd pos_ref; //rad

    Eigen::VectorXd pos_feed;
    Eigen::VectorXd vel_feed;
    Eigen::VectorXd tor_feed;

    /* can bus related variables which can not set to be Eigen */
    double feedback_container[6];
    double control_output[2];
    int driver_ID[2];

    double PI_MATH;
    double TORQUE_SENS;
    double MOTOR_REDUCTION;
    int can_rate;

    ros::ServiceServer running_serv_;
    bool running;

    AmbprosCommon();

    double anglelimiter(float angle);

    void cushion_timer(int nsec);

    void feedback_setup();

    void command_setup(AmbprosControl &AmbprosController);

    void imudata_setup(Eigen::VectorXd &imu_data, ambcap_pros &imu);

    void message_setup(ambpros_can_driver::control_feedback &feedback_msg, Eigen::VectorXd &contactForces, Eigen::VectorXd &imu_data, Eigen::VectorXd &imped_torque, double tau, bool ishumanswing);

    void print_status(AmbprosControl &AmbprosController, TrajectoryGen &AmbprosTraj);

    void get_feedback_fake();

    void set_reference_fake();

    void setup_servs(ros::NodeHandle& nh);

    bool running_callback(std_srvs::Empty::Request  &req,
                          std_srvs::Empty::Response &resp);

};

#endif
