function ret = parsePoint(q,v,distance)
    k = 1;
    m = floor(length(q)/(distance*10));
    ret = zeros(2,m);
    for i = 1:distance*10:length(q)
        ret(1,k) = q(i);
        ret(2,k) = v(i);
        k = k+1;
    end
end