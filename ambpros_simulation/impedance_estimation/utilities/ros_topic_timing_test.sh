#!/bin/bash

publish-topic()
{
	rostopic pub -r 1000 /stuff std_msgs/Float64 "data: 20.0"
}

topic-rate()
{
	rostopic hz /stuff
}

bag-topic()
{
	rosbag record -o test.bag -j /stuff
}

topic-subscribe()
{
	rostopic echo /stuff
}

example-run()
{
	publish-topic &
	bag-topic > /tmp/bag.txt &
	n_subs=5
	for i in $(seq $n_subs)
	do
		echo "Sub $i"
		topic-subscribe > /dev/null &
	done
	topic-rate
}

example-run
