#ifndef AMBPROS_CONTROLLER_HPP
#define AMBRROS_CONTROLLER_HPP

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Eigen>

class AmbprosControl
{
public:
	/* PD controller gain */
    double pGainStance, pGainSwing; //100
    double dGainStance, dGainSwing; //4

    /* Swing Impedance gains */
    double pGainSwingImp, dGainSwingImp;

	/* MIQP controller gain and bounds*/
	double e;
	double qpPenalty;

	double frequency;
	double TORQUE_BOUND;
    double CURRENT_BOUND;

    Eigen::MatrixXd impedparams_stance;
    Eigen::MatrixXd impedparams_swing;

    Eigen::VectorXd imped_torque;
    
    bool isfullbodyMIQP;
    bool isimpedance;
    bool ispd;
    bool isGainsmooth;
    bool isStanding;

	AmbprosControl();

    void calc_controller(int control_method, Eigen::VectorXd &control_signal, Eigen::VectorXd &pos_feed, Eigen::VectorXd &vel_feed,
                         Eigen::VectorXd &torque_feed, Eigen::VectorXd &pos_ref, Eigen::VectorXd &vel_ref,
                         Eigen::VectorXd &torque_ref, int n_act, double timeParameters, int phaseIndex, bool isHumanSwing, bool pauseimpedance);

    void Stand_controller(Eigen::VectorXd &control_signal, Eigen::VectorXd &pos_feed, Eigen::VectorXd &vel_feed,
                          Eigen::VectorXd &torque_feed, Eigen::VectorXd &pos_ref, Eigen::VectorXd &vel_ref,
                          Eigen::VectorXd &torque_ref);

    void MIQP_controller(Eigen::VectorXd &control_signal, Eigen::VectorXd &pos_feed, Eigen::VectorXd &vel_feed,
                           Eigen::VectorXd &torque_feed, Eigen::VectorXd &pos_ref, Eigen::VectorXd &vel_ref,
                           Eigen::VectorXd &torque_ref, int n_act, double timeParameters,  int phaseIndex, bool isHumanSwing, bool pauseimpedance);

    void PD_controller(Eigen::VectorXd &control_signal, Eigen::VectorXd &pos_feed, Eigen::VectorXd &vel_feed,
                           Eigen::VectorXd &torque_feed, Eigen::VectorXd &pos_ref, Eigen::VectorXd &vel_ref,
                           Eigen::VectorXd &torque_ref, int n_act, double timeParameters, int phaseIndex, bool isHumanSwing, bool pauseimpedance);

    void Imped_controller(Eigen::VectorXd &control_signal, Eigen::VectorXd &pos_feed, Eigen::VectorXd &vel_feed,
                           Eigen::VectorXd &torque_feed, Eigen::VectorXd &pos_ref, Eigen::VectorXd &vel_ref,
                           Eigen::VectorXd &torque_ref, int n_act, double timeParameters, int phaseIndex, bool isHumanSwing);

    void MIQPimpedSwing_controller(Eigen::VectorXd &control_signal, Eigen::VectorXd &pos_feed, Eigen::VectorXd &vel_feed,
                           Eigen::VectorXd &torque_feed, Eigen::VectorXd &pos_ref, Eigen::VectorXd &vel_ref,
                           Eigen::VectorXd &torque_ref, int n_act, double timeParameters, int phaseIndex, bool isHumanSwing, bool pauseimpedance);

    double calc_MIQP_fullbody(Eigen::VectorXd &control_sig, Eigen::VectorXd &eta, Eigen::VectorXd &imped_ff, int nOutputs, int phaseIndex, bool isHumanSwing);

    double calc_MIQP(Eigen::VectorXd &control_sig, Eigen::VectorXd &eta, double imped_ff, int indexjoint, int phaseIndex, bool isHumanSwing);

    double calc_feedforward(Eigen::VectorXd &imped_ff, Eigen::VectorXd &pos_feed, Eigen::VectorXd &vel_feed,
                            int a_act, int phaseIndex, bool isHumanSwing);


    double gain_smooth(double desiredGain, double initialGain, double convertEp, double t);

private:
	double d_diagelem;
	double C_num;  //eigMin / eigMax;
};


#endif
