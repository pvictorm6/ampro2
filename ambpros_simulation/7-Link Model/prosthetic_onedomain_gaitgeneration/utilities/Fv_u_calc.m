function [Fv, u] = Fv_u_calc(x,a,p,ep)
nexdof  = length(x)/2;
nbasedof= 3;
dq      = x(nexdof+1:end);

M       = De_mat(x);
C       = Ce_mat(x);
G       = Ge_vec(x);
H       = C*dq + G;

Jsf     = J_sf(x);
Jdotsf  = Jdot_sf(x);
J       = Jsf;
Jdot    = Jdotsf;

invM    = inv(M);
Xi      = inv(J*invM *J');
vf      = [dq; M \ ((J'*Xi*J*invM  - eye(nexdof))*H...
                -J'*Xi*Jdot*dq)];
B_IO    = [zeros(nbasedof, nexdof - nbasedof); eye(nexdof - nbasedof)];
gf      = [zeros(size(B_IO)); invM*(eye(nexdof) - ...
                J'*Xi*J*invM )*B_IO];
            
% Outputs
y_a1    = ya1_sca(x, p, a);
y_a2    = ya2_vec(x, p, a);
y_d1    = yd1_sca(x, p, a);
y_d2    = yd2_vec(x, p, a);
y1      = y_a1 - y_d1;
y2      = y_a2 - y_d2;

% Jacobians of Outputs
Dy_a1   = Dya1_mat(x, p, a);
Dy_d1   = Dyd1_mat(x, p, a);
Dy_a2   = Dya2_mat(x, p, a);
Dy_d2   = Dyd2_mat(x, p, a);
Dy_1    = Dy_a1 - Dy_d1;
Dy_2    = Dy_a2 - Dy_d2;

% Lie Derivatives
Lgy1    = Dy_1*gf;
Lgy2    = Dy_2*gf;
Lfy1    = Dy_1*vf;
Lfy2    = Dy_2*vf;

% Second Order Jacobians
DLfy_a2 = DLfya2_mat(x, p, a);
DLfy_d2 = DLfyd2_mat(x, p, a);
DLfy2   = DLfy_a2-DLfy_d2;

% Second Lie Derivatives
LfLfy2  = DLfy2*vf;
LgLfy2  = DLfy2*gf;

% Vector field
A   = [Lgy1; LgLfy2];
u   = -A \ ([0; LfLfy2]+[Lfy1; 2*ep*Lfy2]+ [ep*y1; ep^2*y2]);

% Control Fields
Fv  = -inv((J*invM *J')) * (Jdot * dq + J*invM *(B_IO*u - H));

end