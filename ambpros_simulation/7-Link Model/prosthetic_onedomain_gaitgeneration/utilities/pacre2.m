function y=pacre2(x,a)
addpath('./build_torso')
global p
global UseImpedanceCopy triger_time_copy
global post_swing_trigger stance_switch_trigger  leg_phase leg_phase_on




%Control Gain
ep = 20;

%%%compute q5 based on the guard. Since we are solving the sinusoidal
%%%functions, there are multiple solutions. We have to check which one is
%%%correct by repluging it back to the guard equation.
%%%In our situation, we solve \theta_5, which is quite straight forward,
%%%from the guard equation(the non-stance foot height). Therefore, we know 
%%%which solution is right. 
h1 = @(x1,x2,x3,x4)(-1).*acos(cos(x1)+(113/95).*(cos(x1+ ...
                x2)+(-1).*cos(x1+x2+x3+(-1).*x4)))+ x1 + x2+ x3-x4;

% h2= @(x1,x2,x3,x4)(+1).*acos(cos(x1)+(113/95).*(cos(x1+ ...
%                 x2)+(-1).*cos(x1+x2+x3+(-1).*x4)))+ x1 + x2+ x3-x4;;
% 
% xtest=[x(1:4);h1(x(1),x(2),x(3),x(4))];
% if abs(h_sca(xtest))<=0.01
%     x1=h1;
% end
% xtest=[x(1:4);h2(x(1),x(2),x(3),x(4))];
% if abs(h_sca(xtest))<=0.01
%     x1=h2;
% end

% Comment these out
x       = [x(1:4);h1(x(1),x(2),x(3),x(4));x(5:end)];
ic      = resetFunc(x);
% load('ic_after_40_3_phase_io_ankle_no_cons_LLS.mat')

% load the ic after 40 steps


%%%compute the hip position and hip velocity
p       = deltaphip_sca(ic);



%%%%%%%%%%%%%%%%% NAVID's SETUP %%%%%%%%%%%%%%%%%%%%%%%%%%%
% setting it up right
UseImpedanceCopy =  1;
% Impedance_Params_Copy = Impedance_Params;
% chosen_joint_copy =  chosen_joint;
triger_time_copy = Inf;

post_swing_trigger =  0;
stance_switch_trigger =  0;



leg_phase = 1;  % must always be one here!!!! don't change
% leg_phase_on = -1 turns leg phasing on... i.e. only one knee does
% impedance control
% leg_phase_on = 1 turns leg phasing off...i.e. both knees will have
% impedance controllers
leg_phase_on = -1; 
%%%%%%%%%%%%%%%%% NAVID's SETUP %%%%%%%%%%%%%%%%%%%%%%%%%%%


% Need to do this before every step
    leg_phase = leg_phase * leg_phase_on;
    
    
    post_swing_trigger =  0;
    stance_switch_trigger =  0;



%%%take one step

sol     = ode45(@(t,x) My_f1_vector(t,x,a,ep), [0 4], ic, ...
        odeset('Events', @(t,x)eventfcn(t,x,a), 'MaxStep', 1e-2));


% Need to do this before every step
    leg_phase = leg_phase * leg_phase_on;
    
    
    post_swing_trigger =  0;
    stance_switch_trigger =  0;

    
    
% sol     = ode45(@(t,x) My_f1_vector(t,x,a,ep), [0 4], ic, ...
%         odeset('Events', @(t,x)eventfcn(t,x,a), 'MaxStep', 1e-2));

% data    = sol.y;

%%%%here is the part that has been changed for the two steps poincare map
%%reset Function to get the initial condition for second step


ic_secondstep = resetFunc(sol.y(1:end,end));
p       = deltaphip_sca(ic_secondstep);  %%initial the hip position

sol2    = ode45(@(t,x) My_f1_vector(t,x,a,ep), [0 4], ic_secondstep, ...
        odeset('Events', @(t,x)eventfcn(t,x,a), 'MaxStep', 1e-2));

% sol2    = ode45(@(t,x) My_f2_vector(t,x,a,ep), [0 4], ic_secondstep, ...
%         odeset('Events', @(t,x)eventfcn(t,x,a), 'MaxStep', 1e-2));
data    = sol2.y;        

y=data([1:4,6:end],end);

