#include <gtest/gtest.h>
#include "ros/ros.h"
#include <boost/shared_ptr.hpp>
#include <vector>
#include <iostream>
#include <ambpros_trajectory/ambpros_trajectory.hpp>
#include <roslib_utilities/ros_package_utilities.hpp>
#include <yaml_eigen_utilities/yaml_eigen_utilities.hpp>

using namespace YAML;
using namespace common;
using boost::filesystem::path;
using namespace std;
using namespace Eigen;
using namespace yaml_utilities;

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

/**
 * @brief TimeBased Test HZD input (time) against expected output (desired position and velocity)
 */
TEST(ambpros_trajectory, TimeBased)
{
    TrajectoryGen AmbprosTraj;
    string filepath = roslib_utilities::resolve_local_url("package://ambpros_trajectory/share/test_nnmodel.yaml").string();
    Node node;

    yaml_utilities::yaml_read_file(filepath, node);
    const Node &in = node["in"];
    const Node &out = node["out"];

    const Node &testset = in["testset"];
    const Node &labels = out["labels"];
    const Node &labels2 = out["labels2"];

    int count;
    in["datalength"] >> count;
    neuralNetwork standtowalk(8, 10, 2, "walk2stand");

    double tolerance = 1e-4;
    double phip_curr = 0;
    VectorXd testdata = VectorXd::Zero(8);
    int predict_result;
    int real_label;


    cout << "sign of 10 is " << sgn(10) << endl;

    cout << "sign of -10 is " << sgn(-10) << endl;

    cout << "sign of 0 is " << sgn(0) << endl;

    for (int i = 0; i < count; ++i)
    {
        testset[i] >> testdata;
        labels2[i] >> real_label;
        AmbprosTraj.checkWalkToStand(testdata, phip_curr);

        predict_result = standtowalk.predict(testdata);

        cout << "Label:" << real_label <<". Prediction:" << predict_result << endl;

        EXPECT_LE(abs(predict_result - real_label), tolerance);
    }
//    VectorXd testdata2(8);
//    testset[2] >> testdata2;
//    cout << testdata << endl;
//    cout << testdata2 << endl;
//    cout << testdata.cwiseProduct(testdata2) << endl;
}

int main(int argc, char **argv)
{
    ros::init(argc,argv,"ambpros_tests_nnmodel");
    ros::NodeHandle n;
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
