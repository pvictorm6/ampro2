function newrobotwalk = estimate_ImpedanceParams_robot2(logdata, robotwalk)
    newrobotwalk        = robotwalk;
    ndof                = robotwalk.nDof;
    knee_stance         = robotwalk.Chosen_Joints{2}(2);
%     ankle_stance        = robotwalk.Chosen_Joints{2}(1);
    knee_swing          = robotwalk.Chosen_Joints{1}(2);
%     ankle_swing         = robotwalk.Chosen_Joints{1}(1);
    T                   = logdata.tp;
    Utorque             = logdata.u;                   
    q                   = logdata.q;
    dx                  = logdata.p;
    length_of_step      = length(T);
    
    estimation_method_stance   = @dynamic_estimation_stancephase;
    estimation_method_swing    = @dynamic_estimation_swingphase;

    
    %% determine the trigger time for each domain
    %swing
    [~, trigger_index_swing]    = max(q(knee_swing, :));
    post_swing_trigger          = trigger_index_swing(1);

    %stance
%     post_stance_trigger         = robotwalk.Post_Stance_Trigger;
%     [~, trigger_index_stance]   = find(q(knee_stance, :) > post_stance_trigger);  
    [~, trigger_index_stance]   = max(q(knee_stance, :));  
    stance_switch_trigger       = trigger_index_stance(1);

    
    %% range of each small phase
    range1 = 1: stance_switch_trigger - 1;
    range2 = stance_switch_trigger : length_of_step;
    range3 = 1: post_swing_trigger - 1;
    range4 = post_swing_trigger: length_of_step;

    % post-swing knee impedance estimation
    pre_impact_trigger = newrobotwalk.Pre_Impact_Trigger;
    if pre_impact_trigger < -1
        range4_old  = range4;
        [~, trigger_index_impact]...
                    = find(q(knee_swing, range4_old) > pre_impact_trigger);
        range4      = range4_old(1:trigger_index_impact(end));
        range5      = range4_old(trigger_index_impact(end) + 1: end);
    else
        range5      = [];
    end
    
    %% estimate the impedance parameters for each phase
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% estimate the impedance parameters for domain1
    %%%stance phase    
    impedparams_d1 = estimation_method_stance(q(:, range1)', T(range1), ...
                                        Utorque(:,range1), robotwalk);
    gcf; title('dynamic estimation of domain1')
    
    %% estimate the impedance parameters for domain2
    if stance_switch_trigger < length_of_step
        impedparams_d2 = estimation_method_stance(q(:, range2)', T(range2), ...
                                        Utorque(:,range2), robotwalk);
        gcf; title('dynamic estimation of domain2')
    else
        impedparams_d2 = zeros(1, 3);
    end
    
    %% estimate the impedance parameter for domain3                                           
    %%%swing phase of Domain3 has been separated into two phases    
    impedparams_d3 = estimation_method_swing(q(:, range3)', T(range3), ...
                                        Utorque(:,range3), robotwalk);
    gcf; title('dynamic estimation of domain3')

    impedparams_d4 = estimation_method_swing(q(:, range4)', T(range4), ...
                                        Utorque(:,range4), robotwalk);
    gcf; title('dynamic estimation of domain4')

    if isempty(range5)
        impedparams_d5 = zeros(1, 3);
    else
        impedparams_d5 = estimation_method_swing(q(:, range5)', T(range5), ...
                                        Utorque(:,range5), robotwalk);
        gcf; title('dynamic estimation of domain5')        
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%swing phase impedance parameters 
    ImpedanceParams_nsknee  = [impedparams_d3; impedparams_d4;...
                               impedparams_d5];
    %%%%stance phase impedance parameters                           
    ImpedanceParams_sknee   = [impedparams_d1; impedparams_d2];


    %% save the multi phase range of the observed data                        
    range_multiphase    = cell(5,1);
    range_multiphase{1} = range1;
    range_multiphase{2} = range2;
    range_multiphase{3} = range3;
    range_multiphase{4} = range4;
    range_multiphase{5} = range5;

    %% update the robotwalk
    newrobotwalk.Impedance_Params.sknee     = ImpedanceParams_sknee;
    newrobotwalk.Impedance_Params.nsknee    = ImpedanceParams_nsknee;
    newrobotwalk.range_multiphase           = range_multiphase;
    %% plot the predicted vs observed trajectory
%     plot_predicted
end