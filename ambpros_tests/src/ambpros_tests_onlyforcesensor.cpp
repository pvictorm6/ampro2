#include "ros/ros.h"
#include "ambpros_common.hpp"
#include <ambpros_trajectory/ambpros_trajectory.hpp>
#include "ambcap_pros.h"
#include "flexforce_adapter.h"

//#define TIME_MODE_DEBUG
#define MESSG_MODE_DEBUG

#ifdef TIME_MODE_DEBUG
#include <time.h>
  int counter = 0;
#endif
 using namespace std;
int main(int argc, char **argv)
{
  ros::init(argc,argv,"ambpros_tests_noCAN");
  ros::NodeHandle n;

  /* initialize three basic classes */
  AmbprosCommon  AmbprosMethods;
  AmbprosControl AmbprosController;
  TrajectoryGen AmbprosTraj;
//  ambcap_pros imu(n);
  flexforce force_adapter(n, AmbprosController.frequency);

//  /* start the can bus */
//  can_base CanDriver(AmbprosMethods.can_rate); // 1000 Kbps
//  CanDriver.open_can();

#ifdef MESSG_MODE_DEBUG
  ros::Publisher data_feedback = n.advertise<ambpros_can_driver::control_feedback>
      ("control_feedback",10);
  ambpros_can_driver::control_feedback feedback_msg;
#endif

#ifdef TIME_MODE_DEBUG
  struct timespec start_r, stop_r;
  double realtime;
#endif

  AmbprosMethods.print_status(AmbprosController, AmbprosTraj);

  ros::Rate loop_rate(AmbprosController.frequency);
  while(ros::ok()){
    #ifdef TIME_MODE_DEBUG
        if(clock_gettime(CLOCK_REALTIME,&start_r) == -1){
         cout << "clock realtime error!" << endl;
        }
    #endif

    ros::spinOnce();
//    imu.spinOnce();
    force_adapter.spinOnce();

    if(AmbprosMethods.init_flag == 1){
//      CanDriver.can_information(AmbprosMethods.number_drivers, AmbprosMethods.driver_ID);
//      CanDriver.configure_mode(AmbprosMethods.control_type_flag);
//      CanDriver.get_feedback(AmbprosMethods.feedback_container, AmbprosMethods.control_type_flag);
        //set up the initial positions
        AmbprosMethods.init_pos(0) = AmbprosMethods.anglelimiter(AmbprosMethods.feedback_container[0]) * AmbprosMethods.PI_MATH / 180.0; //ankle
        AmbprosMethods.init_pos(1) = AmbprosMethods.anglelimiter(AmbprosMethods.feedback_container[3]) * AmbprosMethods.PI_MATH / 180.0; //knee

        // preparation time: 5s
//        AmbprosMethods.cushion_timer(5);

      if(AmbprosMethods.track_mode==0){
        AmbprosMethods.pos_ref(0) = AmbprosMethods.anglelimiter(AmbprosMethods.feedback_container[0]) * AmbprosMethods.PI_MATH / 180.0 - AmbprosMethods.init_pos(0); //ankle
        AmbprosMethods.pos_ref(1) = AmbprosMethods.anglelimiter(AmbprosMethods.feedback_container[3]) * AmbprosMethods.PI_MATH / 180.0 - AmbprosMethods.init_pos(1); //knee
      }
      AmbprosMethods.init_flag = 0;
    }
    else{
        /* get the feedback signal */
//        CanDriver.get_feedback(AmbprosMethods.feedback_container, AmbprosMethods.control_type_flag);
        AmbprosMethods.get_feedback_fake();
    }

    /* setup the feedback values */
    AmbprosMethods.feedback_setup();

    Eigen::VectorXd imu_data = Eigen::VectorXd::Zero(8);
//    if(!AmbprosTraj.isTimebased)
//        AmbprosMethods.imudata_setup(imu_data, imu);

    Eigen::VectorXd contactForces = Eigen::VectorXd::Zero(2);
//    if(AmbprosTraj.isForcesensor){
        contactForces << force_adapter.flexforce_msg.vector.x,
                         force_adapter.flexforce_msg.vector.y;
//    }


    //ROS_INFO("l_thigh: %f, r_knee: %f, r_angle: %f, r_foot: %f", imu.hLte[2], imu.hRst[2], imu.hRfs[2], imu.hRfe[2]);
    /* get the position reference using the TrajectoryGen class */
    AmbprosTraj.calcReference(AmbprosMethods.pos_ref, AmbprosMethods.vel_ref,
                             AmbprosMethods.pos_feed, AmbprosMethods.vel_feed,
                             contactForces, imu_data, AmbprosController.frequency);

//    cout << AmbprosMethods.vel_ref[0] << endl << AmbprosMethods.vel_ref[1] << endl;

    /* compute the command torque on the joint side  AmbprosControl class */
    AmbprosController.calc_controller(AmbprosMethods.control_method, AmbprosMethods.control_signal,
                                      AmbprosMethods.pos_feed, AmbprosMethods.vel_feed, AmbprosMethods.tor_feed,
                                      AmbprosMethods.pos_ref, AmbprosMethods.vel_ref, AmbprosMethods.tor_ref,
                                      AmbprosMethods.number_drivers, AmbprosTraj.TimeParameter, AmbprosTraj.phaseIndex,
                                      AmbprosTraj.isHumanSwing, AmbprosTraj.pauseimpedance);
    /* setup the command currents */
    AmbprosMethods.command_setup(AmbprosController);


    /* send the command to the motion controllers */
//    CanDriver.set_references(AmbprosMethods.control_output, AmbprosMethods.control_type_flag);
    AmbprosMethods.set_reference_fake();

    /* publish the messages */
    #ifdef MESSG_MODE_DEBUG
        Eigen::VectorXd imped_torque(2);
        imped_torque << AmbprosController.imped_torque(0),
                          AmbprosController.imped_torque(1);
        AmbprosMethods.message_setup(feedback_msg, contactForces, imu_data, imped_torque,  AmbprosTraj.TimeParameter, AmbprosTraj.isHumanSwing);
        data_feedback.publish(feedback_msg);
    #endif

    loop_rate.sleep();

    #ifdef TIME_MODE_DEBUG
        if(clock_gettime(CLOCK_REALTIME,&stop_r) == -1){
         cout << "clock realtime error!" << endl;
        }else{
          realtime = (stop_r.tv_sec - start_r.tv_sec) + (double)(stop_r.tv_nsec - start_r.tv_nsec) / (double)1e9;
          std::cout<< "Time :"<<realtime<<std::endl;
        }
    #endif
  }

  AmbprosMethods.control_output[0] = 0;
  AmbprosMethods.control_output[1] = 0;
//  CanDriver.set_references(AmbprosMethods.control_output, AmbprosMethods.control_type_flag);
//  sleep(1);
//  CanDriver.stop_devices();
//  CanDriver.close_can();
  return 0;
}
