classdef amberwalk_configuration < handle
    % This class holds information about a specific robotic model including
    % some details about the form of control applied.
    
    properties        
        %%%%model information
        nBaseDof;
        nDof;
        nR1Dof;
        nR2Dof;
        nDomain;
        nOutputs;
        nParameter
        motiontype;
        
        %%%%user defined parameters
        ep;
        dr;
        nSteps;  
        
        %%%%optimized parameter
        a_fit;
        a_opt;
        x_opt;
        ic;
        p;
        data;

        %% functions
        resetFunc;
        guardFunc;
        odeFunc;
        phip;
    end
    
    properties
        %% CLF parameters
        nCon;
        Penalty;
        Umax
        F;
        G;
        Q;
        P;
        C;
        e;
        Pe;     
    end
    
    methods
        function this = amberwalk_configuration()
            initializeProperties(this);
            
            updateModel(this);
            
            loadData(this);
            
            setSimulationInitial(this);
            
            setCLFInitial(this); 
                       
        end
        
        function initializeProperties(this)  
            this.nBaseDof   = 3;
            this.nDof       = 6;
            this.nR1Dof     = 1;
            this.nR2Dof     = 5;
            this.nOutputs   = 6;
            this.nParameter = 5; %5 is canonical function, 7 is ex-canonical
            this.nDomain    = 2;
            this.motiontype = MotionType.FLATGROUND;
            
            this.data       = cell(this.nSteps,1);
            this.a_fit      = cell(this.nDomain,1);
            this.a_opt      = cell(this.nDomain,1);
            this.x_opt      = cell(this.nDomain,1);
            this.ic         = cell(this.nDomain,1);
            
            this.resetFunc  = cell(this.nDomain,1);
            this.guardFunc  = cell(this.nDomain,1);
            this.odeFunc    = cell(this.nDomain,1);         
        end
        
        function updateModel(this)           
            this.resetFunc{1} = str2func('R1_map');
            this.resetFunc{2} = str2func('R1_map');
            this.guardFunc{1} = str2func('eventfcn1');
            this.guardFunc{2} = str2func('eventfcn1');
            this.odeFunc{1}   = str2func('f1_vector');
            this.odeFunc{2}   = str2func('f1_vector');
%             this.odeFunc{1}   = str2func('f1_vector_voltage');
%             this.odeFunc{2}   = str2func('f1_vector_voltage');
            this.phip         = str2func('deltaphip_sca');
        end
        
        function loadData(this)            
%             temp            = load('a_opt_temp.mat');
            temp            = load('a_opt_roll_jake2.mat');
            this.a_opt{1,1} = temp.a_opt;
            this.a_opt{2,1} = temp.a_opt;
            
%             temp            = load('x_opt_extended.mat');
            temp            = load('x_opt_roll_jake2.mat');
            this.x_opt{1,1} = temp.x_opt;
            this.x_opt{2,1} = temp.x_opt;
        end
        
        function setSimulationInitial(this)
            this.ep         = 20;
            this.nSteps     = 2;
            this.dr         = 1; 
            this.ic{1,1}    = this.resetFunc{this.dr}(this.x_opt{2,1},2);
            this.ic{2,1}    = this.resetFunc{this.dr}(this.x_opt{1,1},1);
            this.p          = this.phip(this.ic{this.dr,1});           
        end
  
        %% initial the CLF and QP problem for each domain
        function setCLFInitial(this)
            %%for domain 1                                   
            this.Umax    = 150;
            this.Penalty = 10E2;
            this.nCon    = 6;
            nc1          = this.nR1Dof;
            nc2          = this.nR2Dof;
            
            this.F   = [zeros(nc1, nc1), zeros(nc1, 2*nc2);
                           zeros(nc2, nc1), zeros(nc2, nc2), eye(nc2);
                           zeros(nc2, nc1), zeros(nc2, 2*nc2)];
            this.G   = [eye(nc1), zeros(nc1, nc2);
                           zeros(nc2, nc1), zeros(nc2, nc2);
                           zeros(nc2, nc1), eye(nc2)];
            this.Q   = eye(nc1 + 2*nc2);
            this.P   = care(this.F, this.G, this.Q);
            this.C   = min(eig(eye(nc1 + nc2*2))) / max(eig(this.P));
            this.e   = blkdiag(eye(nc1),this.ep.*eye(nc2),eye(nc2));
            this.Pe  = this.e*this.P*this.e;                 
        end
    end
end