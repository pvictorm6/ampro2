classdef MotionType < uint32
    enumeration
        FLATGROUND (1)
        STAIRCLIMB (2)
        DOWNSTAIRS (3)
        RUNNING    (4)
    end
end