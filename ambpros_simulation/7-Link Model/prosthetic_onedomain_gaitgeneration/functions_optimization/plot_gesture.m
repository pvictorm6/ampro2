function plot_gesture(xe, color)
    figure(212);
    plot([-0.7 2.4], [0 0], '-b');

    hold on;
    legs = plot([0 0], color);
    axis([-1 4 -1 1.2]);
    
    pos     = jpos_mat(xe);
    set(legs, 'XData', pos(1,:), 'YData', pos(2,:), ...
        'erasemode', 'normal');
    axis equal;
    hold on;
    drawnow;   
    
end