#include <gtest/gtest.h>
#include "ros/ros.h"
#include <ambpros_trajectory/ambpros_trajectory.hpp>
#include <roslib_utilities/ros_package_utilities.hpp>
#include <yaml_eigen_utilities/yaml_eigen_utilities.hpp>

using namespace YAML;
using namespace common;
using boost::filesystem::path;
using namespace std;
using namespace Eigen;
using namespace yaml_utilities;

/**
 * @brief TimeBased Test HZD input (time) against expected output (desired position and velocity)
 */
TEST(ambpros_trajectory, TimeBased)
{
    TrajectoryGen AmbprosTraj;

    string filepath = roslib_utilities::resolve_local_url("package://ambpros_trajectory/share/test_hzd_recon.yaml").string();
    Node node;

    yaml_utilities::yaml_read_file(filepath, node);
    const Node &in = node["in"];

    const double tolerance = 1e-3;

    // Check the basics
    double pPlus;
    Eigen::MatrixXd Phi_expected;
    in["p_plus"] >> pPlus;
    in["Phi"] >> Phi_expected;
    EXPECT_LE(abs(pPlus- AmbprosTraj.initialPhip), tolerance);


    const Node &ts = in["ts"];
    const Node &out = node["out"];
    const Node &qs = out["qs"];
    const Node &dqs = out["dqs"];
    const Node &yd2s = out["yd2s"];
    const Node &dyd2s = out["dyd2s"];
    const Node &zs = out["z"];

    int count = ts.size();
    common_assert_msg(qs.size() == count, qs.size() << " != " << count);
    common_assert_msg(dqs.size() == count, dqs.size() << " != " << count);

    double t;
    Eigen::VectorXd yd2_expected;
    Eigen::VectorXd dyd2_expected;
    Eigen::VectorXd q_expected;
    Eigen::VectorXd dq_expected;
    Eigen::VectorXd z_expected;

    Eigen::VectorXd yd2_actual = Eigen::VectorXd::Zero(AmbprosTraj.nFullR2Outputs);
    Eigen::VectorXd dyd2_actual = Eigen::VectorXd::Zero(AmbprosTraj.nFullR2Outputs);
    Eigen::VectorXd q_actual = Eigen::VectorXd::Zero(AmbprosTraj.nFullOutputs);
    Eigen::VectorXd dq_actual = Eigen::VectorXd::Zero(AmbprosTraj.nFullOutputs);

    Eigen::VectorXd z = Eigen::VectorXd::Zero(2);
    Eigen::VectorXd xref  = Eigen::VectorXd::Zero(2*AmbprosTraj.nFullOutputs);

    for (int i = 0; i < count; ++i)
    {
        ts[i] >> t;
        yd2s[i] >> yd2_expected;
        dyd2s[i] >> dyd2_expected;
        qs[i] >> q_expected;
        dqs[i] >> dq_expected;
        zs[i] >> z_expected;

        // Check z
        z << AmbprosTraj.compute_phip(t),
             AmbprosTraj.compute_vhip(t);
        EXPECT_LE((z - z_expected).norm(), tolerance);

        // Check outputs
        AmbprosTraj.compute_y2d(yd2_actual, t);
        AmbprosTraj.compute_y2dDot(dyd2_actual, z(1), t);
        EXPECT_LE((yd2_actual - yd2_expected).norm(), tolerance);
        EXPECT_LE((dyd2_actual - dyd2_expected).norm(), tolerance);

        AmbprosTraj.phzd_reconstruction(xref, z, yd2_actual, dyd2_actual);

        q_actual  = xref.head(AmbprosTraj.nFullOutputs);
        dq_actual = xref.tail(AmbprosTraj.nFullOutputs);
        // Check reconstruction
        EXPECT_LE((q_actual - q_expected).norm(), tolerance);
        EXPECT_LE((dq_actual - dq_expected).norm(), tolerance);
    }

//    cout << Phi_expected << endl;
//    cout << AmbprosTraj.Phi << endl;
}

int main(int argc, char **argv)
{
    ros::init(argc,argv,"ambpros_tests_12346");
    ros::NodeHandle n;
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
