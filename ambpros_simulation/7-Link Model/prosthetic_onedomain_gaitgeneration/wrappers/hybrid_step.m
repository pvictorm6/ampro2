function execution = hybrid_step(tp0,logObj,robotwalk)
    %% Perform integration over the default time interval.  Use the
    %  provided ODE and pass it the initial conditions.
    
    sol = ode45(@(t,x)robotwalk.odeFunc{robotwalk.dr}(t,x,tp0,logObj,robotwalk),...
                 [0 1.618], robotwalk.ic{robotwalk.dr,1},...
                 odeset('Events', robotwalk.guardFunc{robotwalk.dr},...
                 'MaxStep', 1e-2));
        
    execution.time           = sol.x;
    execution.integral_curve = sol.y;
    
    %% reset for next step
    if ~isempty(sol.x)
        [execution.next_qi, execution.next_di] = ...
            robotwalk.resetFunc{robotwalk.dr}(sol.y(:, end), robotwalk.dr);
    else
        execution.next_qi = sol.y(:, end);
        execution.next_di = robotwalk.dr;
    end
end

