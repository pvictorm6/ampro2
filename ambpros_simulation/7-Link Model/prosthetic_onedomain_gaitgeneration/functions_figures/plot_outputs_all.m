%Hip position constraint for 4 subjects
function plot_outputs_all

addpath('./dataforfigure')

temp    = load('log_data_IO.mat');
logObj  = temp.logObj;

legendarray     = {'v_{hip}','\theta_{sk}','\theta_{nsk}','m_{nsl}',...
                'torso','\theta_{nsf}'};
colorarray      = {'blue','red',[32 153 41]/255,[80 30 20]/255,'black','m'};
sampleRate      = 100;
outputs         = logObj.yact;
[nOutputs, ~]   = size(outputs);
op              = zeros(nOutputs, sampleRate);  
%reduce number of data points for readability
for i = 1: nOutputs
    op(i,:) = resampledata(sampleRate, outputs(i,:)');   
end

%plots
h               = figure;
mark_size       = 8;

set(h, 'position', [100 100 1*500 1*350]);

%%%%patch the one standard deviation

hold on
time  = resampledata(sampleRate,logObj.tp');
for i = [1,3,5]
    hk(i) = plot(time,op(i,:),'color',colorarray{i});
end


set(hk(:),'MarkerSize',mark_size,'LineWidth',2);

% axis([0 .42 0.40 1.15])
xlbound = 0;
xubound = time(end) + 0.01;
ylbound = min(min(op)) - 0.02;
yubound = max(max(op)) + 0.1;
axis([xlbound xubound ylbound yubound])

%Legend
%Font Size
FS       = 15;
%Top offset
top_offset = 0.80;
%Bottom offset
bott_offset = 0.88;
%
xlabel('Scaled Time','Interpreter','LaTex','FontSize',15);
ylabel('Angle (rad)','Interpreter','LaTex','FontSize',15);
set(gca,'FontSize',12);
apc = [0.130000000000000   0.2800000000000   0.775000000000000   0.690000000000000];
set(gca,'position',apc);

ah1 = gca;
l = legend(ah1,hk(1),legendarray{1},'Location','SouthOutside',...
          'Box','off','FontSize',FS,'Interpreter','LaTeX');
p = get(l,'position');
set(l, 'position', [0.1 p(2)- top_offset .3 p(4)+.06],'Box','off');


ah2=axes('position',get(gca,'position'), 'visible','off');
set(ah2,'FontSize',FS);
l = legend(ah2,hk(2),legendarray{2},'Location','SouthOutside',...
    'Box','off','FontSize',FS,'Interpreter','LaTeX');
p = get(l,'position');
set(l, 'position', [0.1 p(2)-bott_offset .35 p(4)+.06],'Box','off');

ah3=axes('position',get(gca,'position'), 'visible','off');
set(ah3,'FontSize',FS);
l = legend(ah3,hk(3),legendarray{3},'Location','SouthOutside',...
    'Box','off','FontSize',FS,'Interpreter','LaTeX');
p = get(l,'position');
set(l, 'position', [0.4 p(2)-top_offset .3 p(4)+.06],'Box','off');





ah4=axes('position',get(gca,'position'), 'visible','off');
set(ah4,'FontSize',FS);
l = legend(ah4,hk(4),legendarray{4},'Location','SouthOutside',...
    'Box','off','FontSize',FS,'Interpreter','LaTeX');
p = get(l,'position');
set(l, 'position', [0.7 p(2)-bott_offset .35 p(4)+.06],'Box','off');

ah5=axes('position',get(gca,'position'), 'visible','off');
set(ah5,'FontSize',FS);
l = legend(ah5,hk(5),legendarray{5},'Location','SouthOutside',...
    'Box','off','FontSize',FS,'Interpreter','LaTeX');
p = get(l,'position');
set(l, 'position', [0.7 p(2)-top_offset .3 p(4)+.06],'Box','off');

ah6=axes('position',get(gca,'position'), 'visible','off');
set(ah6,'FontSize',FS);
l = legend(ah6,hk(6),legendarray{6},'Location','SouthOutside',...
    'Box','off','FontSize',FS,'Interpreter','LaTeX');
p = get(l,'position');
set(l, 'position', [0.4 p(2)-bott_offset .35 p(4)+.06],'Box','off');


end