    #include <iostream>
    #include "ros/ros.h"

    #include <ambpros_controller/ambpros_controller.hpp>
    #include <yaml_eigen_utilities/yaml_eigen_utilities.hpp>
    #include <roslib_utilities/ros_package_utilities.hpp>
    #include <control_utilities/limits.hpp>

    #include "Error.h"
    #include "EigenUtil.h"
    #include "eiquadprog.h"

    using namespace YAML;
    using namespace common;
    using boost::filesystem::path;
    using namespace std;
    using namespace Eigen;

    static bool isprt = true;

    AmbprosControl::AmbprosControl()
    {
        // controller gain
        ros::param::get("/ambpros/control/pGainStance", this->pGainStance);
        ros::param::get("/ambpros/control/dGainStance", this->dGainStance);
        ros::param::get("/ambpros/control/pGainSwing", this->pGainSwing);
        ros::param::get("/ambpros/control/dGainSwing", this->dGainSwing);
        ros::param::get("/ambpros/control/e", this->e);
        ros::param::get("/ambpros/control/qpPenalty", this->qpPenalty);
        ros::param::get("/ambpros/control/pGainSwingImp", this->pGainSwingImp);
        ros::param::get("/ambpros/control/dGainSwingImp", this->dGainSwingImp);

        // torque bound and current bound
        ros::param::get("/ambpros/control/TORQUE_BOUND", this->TORQUE_BOUND);
        ros::param::get("/ambpros/control/CURRENT_BOUND", this->CURRENT_BOUND);

        // operating frequency
        ros::param::get("/ambpros/control/frequency", this->frequency);

        // choose which MIQP method to use
        ros::param::get("/ambpros/control/isfullbodyMIQP", this->isfullbodyMIQP);

        // load impedance parameters
        ros::param::get("/ambpros/control/isimpedance", this->isimpedance);
        ros::param::get("/ambpros/control/ispd", this->ispd);
        ros::param::get("/ambpros/control/isgainsmooth", this->isGainsmooth);
        ros::param::get("/ambpros/trajectory/isStanding", this->isStanding);

        string filepath = roslib_utilities::resolve_local_url("package://ambpros_controller/share/impedparameters.yaml").string();
        Node node;
        yaml_utilities::yaml_read_file(filepath, node);
        node["stance"] >> impedparams_stance;
        node["swing"]  >> impedparams_swing;

        imped_torque = VectorXd::Zero(2);

        d_diagelem = 1.7321;
        C_num      = 0.3660;  //eigMin / eigMax;
    }


    void AmbprosControl::calc_controller(int control_method, VectorXd &control_signal, VectorXd &pos_feed, VectorXd &vel_feed,
                                         VectorXd &torque_feed, VectorXd &pos_ref, VectorXd &vel_ref,
                                         VectorXd &torque_ref, int n_act, double timeParameter, int phaseIndex, bool isHumanSwing, bool pauseimpedance)
    {
        if(isStanding){
            Stand_controller(control_signal, pos_feed, vel_feed, torque_feed, pos_ref,
                             vel_ref, torque_ref);
        }
        else{
            switch(control_method){
                case 0:
                {
                    PD_controller(control_signal, pos_feed, vel_feed, torque_feed, pos_ref,
                                  vel_ref, torque_ref,  n_act, timeParameter, phaseIndex, isHumanSwing, pauseimpedance);
                    if(isprt){
                        cout << "PD controller is used. Impedance is used: " << isimpedance << endl;
                        isprt = false;
                    }
                    break;
                }
                case 1:
                {
                    MIQP_controller(control_signal, pos_feed, vel_feed, torque_feed, pos_ref,
                                    vel_ref, torque_ref, n_act, timeParameter, phaseIndex, isHumanSwing, pauseimpedance);
                    if(isprt){
                        cout << "MIQP controller is used. Impedance is used: " << isimpedance << endl;
                        isprt = false;
                    }
                    break;
                }
                case 2:
                {
                    Imped_controller(control_signal, pos_feed, vel_feed, torque_feed, pos_ref,
                                     vel_ref, torque_ref, n_act, timeParameter, phaseIndex, isHumanSwing);
                    break;
                }
                case 3:
                {
                    MIQPimpedSwing_controller(control_signal, pos_feed, vel_feed, torque_feed, pos_ref,
                                     vel_ref, torque_ref, n_act, timeParameter, phaseIndex, isHumanSwing, pauseimpedance);
                    if(isprt){
                        cout << "MIQPimpedSwing controller is used. Low impedance is used at Swing phase: " << isimpedance << endl;
                        isprt = false;
                    }
                    break;
                }
                default:
                {
                    cout << "The chosen control method is not available !!" << endl;
                    cout << "all control signal are set to 0." << endl;
                    control_signal << 0, 0;
                }
            }
        }
    }


    void AmbprosControl::Stand_controller(Eigen::VectorXd &control_signal, Eigen::VectorXd &pos_feed, Eigen::VectorXd &vel_feed,
                          Eigen::VectorXd &torque_feed, Eigen::VectorXd &pos_ref, Eigen::VectorXd &vel_ref,
                          Eigen::VectorXd &torque_ref)
    {
        double anklerange_lb = -0.1;
        double anklerange_ub = 0.1;
        double kneerange_lb  = 0;
        double kneerange_ub  = 0.2;
        double tauMax_ankle  = abs(anklerange_ub - anklerange_lb);
        double tauMax_knee   = abs(kneerange_ub - kneerange_lb);
        double tauAnkle;
        double tauKnee;
        tauAnkle = abs(pos_feed(0))/tauMax_ankle;
        tauAnkle = control_utilities::clamp(tauAnkle, 0, 1);

        tauKnee  = abs(pos_feed(1))/tauMax_knee;
        tauAnkle = control_utilities::clamp(tauKnee, 0, 1);

        double desiredPGain = 1.0*pGainStance;
        double initialPGain = 0.1*pGainStance;
        double desiredDGain = 1.0*dGainStance;
        double initialDGain = 0.1*dGainStance;
        double convertEp    = 20;
        double Kp_ankle = gain_smooth(desiredPGain, initialPGain, convertEp, tauAnkle);
        double Kp_knee  = gain_smooth(desiredPGain, 0.2*initialPGain, convertEp, tauKnee);
        double Kd_ankle = gain_smooth(desiredDGain, initialDGain, convertEp, tauAnkle);
        double Kd_knee  = gain_smooth(desiredDGain, 0.2*initialDGain, convertEp, tauKnee);

        control_signal(0) = -(Kp_ankle*(pos_feed(0) - pos_ref(0)) + Kd_ankle*(vel_feed(0)- vel_ref(0)));
        control_signal(1) = -(Kp_knee*(pos_feed(1) - pos_ref(1)) + Kd_knee*(vel_feed(1)- vel_ref(1)));
    }

    void AmbprosControl::MIQP_controller(VectorXd &control_signal, VectorXd &pos_feed, VectorXd &vel_feed,
                           VectorXd &torque_feed, VectorXd &pos_ref, VectorXd &vel_ref,
                           VectorXd &torque_ref, int n_act, double timeParameters, int phaseIndex, bool isHumanSwing, bool pauseimpedance)
    {
        VectorXd y2   = VectorXd::Zero(n_act);
        VectorXd dy2  = VectorXd::Zero(n_act);

        if(isimpedance && !pauseimpedance){
            calc_feedforward(imped_torque, pos_feed, vel_feed, n_act, phaseIndex, isHumanSwing);
        }else{
            imped_torque << 0, 0;
        }

        //compute the MIQP for each joint
        for(int i=0; i< n_act; i++){
            y2(i)  = pos_feed(i) - pos_ref(i);
            dy2(i) = vel_feed(i) - vel_ref(i);
        }

        /* use different types of MIQP controller */
        if(isfullbodyMIQP){
            VectorXd eta  = VectorXd::Zero(2*n_act);
            eta <<  y2,
                    dy2;
            calc_MIQP_fullbody(control_signal, eta, imped_torque, n_act, phaseIndex, isHumanSwing);
        }
        else{
            for(int index_joint = 0; index_joint < n_act; index_joint++){
                VectorXd eta  = VectorXd::Zero(2);
                eta <<  y2(index_joint),
                        dy2(index_joint);

                calc_MIQP(control_signal, eta, imped_torque(index_joint), index_joint, phaseIndex, isHumanSwing);
            }
        }
    }

    void AmbprosControl::PD_controller(VectorXd &control_signal, VectorXd &pos_feed, VectorXd &vel_feed,
                           VectorXd &torque_feed, VectorXd &pos_ref, VectorXd &vel_ref,
                           VectorXd &torque_ref, int n_act, double timeParameter, int phaseIndex, bool isHumanSwing, bool pauseimpedance)
    {
      double Kp, Kd;
      if(isHumanSwing){
          Kp = pGainStance;
          Kd = dGainStance;
      }else
      {
          Kp = pGainSwing;
          Kd = dGainSwing;
      }

      if(isGainsmooth){
          double desiredGain = 1.5*Kp;
          double initialGain = Kp;
          double convertEp  = 20;
          Kp = gain_smooth(desiredGain, initialGain, convertEp, timeParameter);
      }

      if(isimpedance && !pauseimpedance)
      {
          calc_feedforward(imped_torque, pos_feed, vel_feed, n_act, phaseIndex, isHumanSwing);
      }else
      {
          imped_torque << 0, 0;
      }
      for(int i = 0; i < n_act; i++){
          control_signal(i) = -(Kp*(pos_feed(i) - pos_ref(i)) + Kd*(vel_feed(i)- vel_ref(i))) + imped_torque(i);
    //      // check safety tracking. If the position is too off the desired trajectory, add an extra control force try to drag it back
    //      if(abs(pos_feed(i)-pos_ref(i))>0.2 && timeParameter>0.1)
    //      {
    //        double Kdrag = 200;
    //        control_signal(i)  = control_signal(i) - Kdrag*(pos_feed(i) - pos_ref(i));
    //      }
      }
    }

    void AmbprosControl::Imped_controller(VectorXd &control_signal, VectorXd &pos_feed, VectorXd &vel_feed,
                                          VectorXd &torque_feed, VectorXd &pos_ref, VectorXd &vel_ref,
                                          VectorXd &torque_ref, int n_act, double timeParameters, int phaseIndex, bool isHumanSwing)
    {
        VectorXd pd_control = VectorXd::Zero(n_act);
        calc_feedforward(imped_torque, pos_feed, vel_feed, n_act, phaseIndex, isHumanSwing);

        if(ispd)
        {
            double Kp, Kd;
            if(isHumanSwing){
                Kp = pGainStance;
                Kd = dGainStance;
            }else
            {
                Kp = pGainSwing;
                Kd = dGainSwing;
            }
            for(int i = 0; i < n_act; i++){
                pd_control(i) = -(Kp*(pos_feed(i) - pos_ref(i)) + Kd*(vel_feed(i)- vel_ref(i)));
            }
        }else
        {
            pd_control << 0, 0;
        }
        control_signal = imped_torque + pd_control;
    }

    void AmbprosControl::MIQPimpedSwing_controller(VectorXd &control_signal, VectorXd &pos_feed, VectorXd &vel_feed,
                                          VectorXd &torque_feed, VectorXd &pos_ref, VectorXd &vel_ref,
                                          VectorXd &torque_ref, int n_act, double timeParameters, int phaseIndex, bool isHumanSwing, bool pauseimpedance)
    {
      if(isHumanSwing) // prosthetic support
      {
        MIQP_controller(control_signal, pos_feed, vel_feed, torque_feed, pos_ref, vel_ref, torque_ref, n_act, timeParameters, phaseIndex, isHumanSwing, pauseimpedance);
      }else // prosthetic swing
      {
        MIQP_controller(control_signal, pos_feed, vel_feed, torque_feed, pos_ref, vel_ref, torque_ref, n_act, timeParameters, phaseIndex, isHumanSwing, pauseimpedance);
        double Kp, Kd;

        Kp = pGainSwingImp;
        Kd = dGainSwingImp;

        // Pure Impedance in the ankle
        control_signal(0) = Kp * (pos_ref(0) - pos_feed(0)) + Kd * (vel_ref(0) - vel_feed(0)); 

      }
    }

    double AmbprosControl::calc_MIQP_fullbody(VectorXd &control_sig, VectorXd &eta, VectorXd &imped_ff, int nOutputs, int phaseIndex, bool isHumanSwing)
    {
        int nDegreeOne = 0,
            nDegreeTwo = nOutputs;
        int etaSize = nDegreeOne + nDegreeTwo * 2;
        int nVar = 1 + nDegreeOne + nDegreeTwo;

        MatrixXd P_mat(etaSize, etaSize);
        P_mat.setIdentity();
        P_mat.block(nDegreeOne, nDegreeOne, 2*nDegreeTwo, 2*nDegreeTwo) *= d_diagelem;
        P_mat.block(nDegreeOne, nDegreeOne + nDegreeTwo, nDegreeTwo, nDegreeTwo).setIdentity();
        P_mat.block(nDegreeOne + nDegreeTwo, nDegreeOne, nDegreeTwo, nDegreeTwo).setIdentity();

        MatrixXd e_mat(MatrixXd::Identity(etaSize, etaSize));
        e_mat.block(nDegreeOne, nDegreeOne, nDegreeTwo, nDegreeTwo).setIdentity() *= e;

        MatrixXd F_mat(MatrixXd::Zero(etaSize, etaSize));
        F_mat.block(nDegreeOne, nDegreeOne + nDegreeTwo, nDegreeTwo, nDegreeTwo).setIdentity();

        MatrixXd G_mat(MatrixXd::Zero(etaSize, nOutputs));
        G_mat.block(0, 0, nDegreeOne, nDegreeOne).setIdentity();
        G_mat.block(nDegreeOne + nDegreeTwo, nDegreeOne,
            nDegreeTwo, nDegreeTwo).setIdentity();

        MatrixXd Pe_mat = e_mat * P_mat * e_mat;
        VectorXd Ve_num = eta.transpose() * Pe_mat * eta;
        VectorXd LfVe_num = eta.transpose() * (F_mat.transpose() * Pe_mat + Pe_mat * F_mat) * eta;
        VectorXd Psi0 = LfVe_num + C_num * e * Ve_num;
        MatrixXd Psi1 = 2 * eta.transpose() * Pe_mat * G_mat;

        // set up the constraints
        int nCon = 0,
            nEq  = nCon,
            nIneq = 1 + 2*nOutputs;

        bool useQPTorqueMax = true;
        bool usemuTorqueMax = true;
        bool useTorqueSmooth = false;

        VectorXd muTorqueMax, qpTorqueMax, TorqueSmooth, TorquePrev;

        muTorqueMax = VectorXd::Ones(nOutputs)*30;
        qpTorqueMax = VectorXd::Ones(nOutputs)*30;
        TorqueSmooth = VectorXd::Ones(nOutputs)*2;
        TorquePrev = VectorXd::Ones(nOutputs)*0;

        // check how many constraints we have
        if (useQPTorqueMax && useTorqueSmooth)  //constraint the rate limiter and the QP max torque
            nIneq += 4*nOutputs;
        if (useQPTorqueMax && !useTorqueSmooth) //constraint the rate limiter
            nIneq += 2*nOutputs;
        if (!useQPTorqueMax &&  useTorqueSmooth) //constraint the QP max torque
            nIneq += 2*nOutputs;


        MatrixXd Hqp, Aiq, Aeq;
        VectorXd muqp, biq, beq, fqp, feedforward;
        Hqp = MatrixXd::Identity(nVar, nVar);
        Aiq = MatrixXd::Zero(nIneq, nVar);
        Aeq = MatrixXd::Zero(nEq, nVar);
        muqp = VectorXd::Zero(nVar);
        fqp = VectorXd::Zero(nVar);
        biq = VectorXd::Zero(nIneq);
        beq = VectorXd::Zero(nEq);
        feedforward = VectorXd::Zero(nOutputs);

        Hqp(0, 0) = qpPenalty;
        feedforward = imped_ff;


        //CLF constraints
        int iqRow = 0;
        int nRelax = 1;

        Aiq.block(iqRow, 0, nRelax, nRelax) << -1;
         Aiq.block(iqRow, nRelax, nRelax, nOutputs) = Psi1;
         biq.segment(iqRow, nRelax) = -Psi0;  //- Psi1*feedforward;     //ff outside QP
         iqRow += nRelax;

         // Torque bound constrain
         if (useQPTorqueMax)
         {
             Aiq.block(iqRow, nRelax, nOutputs, nOutputs).setIdentity();    //[0,   1,   1]
             biq.segment(iqRow, nOutputs) = qpTorqueMax;  // Aiq * mu_QP + ff < u_max     //ff outside QP
             iqRow += nOutputs;

             Aiq.block(iqRow, nRelax, nOutputs, nOutputs).setIdentity() *=-1;   //[0,   -1,  -1]
             biq.segment(iqRow, nOutputs) = qpTorqueMax;
             iqRow += nOutputs;
         }

         // mu bound constrain
         if (usemuTorqueMax)
         {
             Aiq.block(iqRow, nRelax, nOutputs, nOutputs).setIdentity();    //[0,   1,  1]
             biq.segment(iqRow, nOutputs) = muTorqueMax;
             iqRow += nOutputs;

             Aiq.block(iqRow, nRelax, nOutputs, nOutputs).setIdentity() *= -1;  //[0,   -1, -1]
             biq.segment(iqRow, nOutputs) = muTorqueMax;
             iqRow += nOutputs;
         }

         // Torque rate limitation constrain
         if (useTorqueSmooth)
         {
             Aiq.block(iqRow, nRelax, nOutputs, nOutputs).setIdentity();  //[0, 1,  1]
             biq.segment(iqRow, nOutputs) = TorquePrev + TorqueSmooth;
             iqRow += nOutputs;

             Aiq.block(iqRow, nRelax, nOutputs, nOutputs).setIdentity() *= -1;  //[0,    -1, -1]
             biq.segment(iqRow, nOutputs) = TorquePrev - TorqueSmooth;
             iqRow += nOutputs;
         }

         // QP solving
     //    double qpEqualityTolerance = 0.01;
         MatrixXd HqpCopy = Hqp * 2;
         muqp = VectorXd::Zero(1+nOutputs);

         double obj;
         obj = Eigen::solve_quadprog(HqpCopy, fqp, -Aeq.transpose(), beq, -Aiq.transpose(), biq, muqp);

         if(muqp(0) < 10*qpTorqueMax(0)){
             for(int iOutputs=0; iOutputs<nOutputs; iOutputs++){
                control_sig[iOutputs] = muqp(iOutputs+1) + feedforward(iOutputs);
    //             control_sig(iOutputs) = muqp(iOutputs+1);
             }
             //cout << "QP is excuted successfully. The control torque is: " << control_sig[indexjoint] << endl;
         }
         else{
    //         cout << "QP is failed! PD+Imped control is used instead." << endl;
             double Kp, Kd;
             if(isHumanSwing){
                 Kp = pGainStance;
                 Kd = dGainStance;
             }else
             {
                 Kp = pGainSwing;
                 Kd = dGainSwing;
             }
             for(int iOutputs=0; iOutputs<nOutputs; iOutputs++){
                 control_sig[iOutputs] = -(Kp*eta(iOutputs) + Kd*eta(2*iOutputs+1)) + feedforward(iOutputs);
             }
         }
         return obj;
    }

    double AmbprosControl::calc_MIQP(VectorXd &control_sig, VectorXd &eta, double imped_ff, int indexjoint, int phaseIndex, bool isHumanSwing)
    {
        MatrixXd F_mat(2, 2), G_mat(2, 1), Q_mat(2, 2), P_mat(2, 2), e_mat(2, 2);
        MatrixXd Hqp, Aiq, Aeq;
        VectorXd muqp(2), fqp(2), biq, beq;

        // Initilized the constraints of QP
        double muqp_prev = control_sig[indexjoint];

        F_mat << 0, 1,
                0, 0;
        G_mat << 0, 1;
        Q_mat << 1, 0,
                0, 1;
        P_mat << 1.7321, 1,
                1,     1.7321;

        e_mat << e, 0,
                 0,  1;

        //for QP initialization
        Hqp  = MatrixXd::Identity(2, 2);
        Hqp(0, 0) = qpPenalty;

        fqp  = VectorXd::Zero(2);

        // QP equality equations (empty)
        Aeq = MatrixXd::Zero(0, 2);
        beq = VectorXd::Zero(0);

        MatrixXd Pe_mat = e_mat * P_mat * e_mat;
        VectorXd Ve_num = eta.transpose() * Pe_mat * eta;
        VectorXd LfVe_num = eta.transpose() * (F_mat.transpose() * Pe_mat + Pe_mat * F_mat) * eta;
        VectorXd Psi0 = LfVe_num + C_num * e * Ve_num;
        MatrixXd Psi1 = 2 * eta.transpose() * Pe_mat * G_mat;

        // QP inequality equations
        int    nIneq = 3;
        double qpTorqueMax = 30;
        double muMax = 30;
        double T_smooth = -1;


        // check how many constraints we have
        if (qpTorqueMax != -1 &&  T_smooth != -1)  //constraint the rate limiter and the QP max torque
            nIneq = 3 + 4;
        if (qpTorqueMax == -1 &&  T_smooth != -1 ) //constraint the rate limiter
            nIneq = 3 + 2;
        if (qpTorqueMax != -1 &&  T_smooth == -1 ) //constraint the QP max torque
            nIneq = 3 + 2;

        Aiq = MatrixXd::Zero(nIneq, 2);
        biq = VectorXd::Zero(nIneq);

        //CLF constraints
        int iqRow = 0;
        int nRelax = 1;
        int nOutputs = 1;

        double feedforward = imped_ff;

        Aiq.block(iqRow, 0, 1, 1) << -1;
        Aiq.block(iqRow, 1, 1, 1) = Psi1.transpose();
        biq.segment(iqRow, 1)     = -Psi0; //- Psi1*feedforward;        //ff outside QP
        iqRow += 1;

        // Torque bound constrain
        if (qpTorqueMax != -1)
        {
            Aiq.block(iqRow, nRelax, 1, 1) << 1;    //[0,   1]
            biq(iqRow) = qpTorqueMax - feedforward; // Aiq * mu_QP + ff < u_max     //ff outside QP
            iqRow += nOutputs;

            Aiq.block(iqRow, nRelax, 1, 1) << -1;   //[0,   -1]
            biq(iqRow) = qpTorqueMax + feedforward; // Aiq * mu_QP + ff > -u_max    //ff outside QP
            iqRow += nOutputs;
        }

        // mu bound constrain
        if (muMax != -1)
        {
            Aiq.block(iqRow, nRelax, 1, 1) << 1;    //[0,   1]
            biq(iqRow) = muMax;
            iqRow += nOutputs;

            Aiq.block(iqRow, nRelax, 1, 1) << -1;   //[0,   -1]
            biq(iqRow) = muMax;
            iqRow += nOutputs;
        }

        // Torque rate limitation constrain
        if (T_smooth != -1)
        {
            Aiq.block(iqRow, nRelax, 1, 1) << 1;
            biq(iqRow) = muqp_prev + T_smooth;
            iqRow += nOutputs;

            Aiq.block(iqRow, nRelax, 1, 1) << -1;
            biq(iqRow) = T_smooth - muqp_prev ;
            iqRow += nOutputs;
        }

        // QP solving
        MatrixXd HqpCopy = Hqp * 2;
        muqp = VectorXd::Zero(2);

        double obj;

        obj = Eigen::solve_quadprog(HqpCopy, fqp, -Aeq.transpose(), beq, -Aiq.transpose(), biq, muqp);

        if(muqp(0) < 10*qpTorqueMax){
            control_sig(indexjoint) = muqp(1) + feedforward;
    //        cout << "QP is excuted successfully. The control torque is: " << control_sig[indexjoint] << " . The QP torque is: "<< muqp(1) << endl;
        }
        else{
    //        cout << "QP is failed! PD+Imped control is used instead." << endl;
            double Kp, Kd;
            if(isHumanSwing){
                Kp = pGainStance;
                Kd = dGainStance;
            }else
            {
                Kp = pGainSwing;
                Kd = dGainSwing;
            }
    //        control_sig(indexjoint) = control_utilities::clamp((-(Kp*eta(0) + Kd*eta(1)) + feedforward), -qpTorqueMax, qpTorqueMax);
            control_sig(indexjoint) = control_utilities::clamp(-(Kp*eta(0) + Kd*eta(1)), -qpTorqueMax, qpTorqueMax);
        }
        return obj;
    }


    double AmbprosControl::calc_feedforward(VectorXd &imped_ff, VectorXd &pos_feed, VectorXd &vel_feed,
                                            int n_act, int phaseIndex, bool isHumanSwing)
    {
        int paramIndex;
        MatrixXd impedparams;
        if(isHumanSwing)
        {
            impedparams = impedparams_stance;
        }else
            {
            impedparams = impedparams_swing;
        }

        for(int i = 0; i < n_act; i++){
            paramIndex  = 2*(phaseIndex-1) + i;
            imped_ff(i) = impedparams(paramIndex, 0)*(pos_feed(i) - impedparams(paramIndex, 2)) +
                        impedparams(paramIndex, 1)*vel_feed(i);
            imped_ff(i) = control_utilities::clamp(imped_ff(i), -40, 40);
        }
        return 1;
    }

    double AmbprosControl::gain_smooth(double desiredGain, double initialGain, double convertEp, double t)
    {
       return desiredGain+ exp(-t*convertEp)*(initialGain-desiredGain);
    }
