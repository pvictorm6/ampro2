%% configure the a_opt matrix for the prosthetic C++ program
addpath('./build_torso','./buildopt_torso', './functions_figures', 'classdef')
addpath('./wrappers')
addpath('./utilities');
addpath('./utilities/yaml');

test = load('a_opt_roll_jake2.mat');
temp = load('x_opt_roll_jake2.mat');

xopt = temp.x_opt;
xic  = R1_map(xopt, 1);
xlen = length(xopt);
phipinitial = deltaphip_sca(xic);
phipend     = deltaphip_sca(xopt);
vhipinitial = deltaphip_dot_mat(xic)*xic(xlen/2+4:end);
% vhipend     = deltaphip_dot_mat(xopt)*xic(xlen/2+1:end);



test.a_opt(1,2) = phipinitial;
test.a_opt(1,3) = phipend;
test.a_opt(1,4) = vhipinitial;
yaml_init();
% dir = '~/catkin_ws/src/amber_prosthetic/ambpros_trajectory_generation/launch';
dir = '/tmp';
yaml_write_file([dir, '/a_opt_roll_smallknee.yaml'], test);