function optimization_plotoutputs(humandata, a_fit, a_opt)
%% parse the data
mean_time   = humandata.mean_time;
mean_hippos = humandata.mean_hippos;
mean_sknee  = humandata.mean_sknee;
mean_nsknee = humandata.mean_nsknee;
mean_nsslope= humandata.mean_nsslope;
mean_torso  = humandata.mean_torso;
mean_nsfoot = humandata.mean_nsfoot;

%% decide which function to use to fit
nParameter = size(a_opt,2);
if nParameter == 5
    humanfunc = @func_canonical;
else
    humanfunc = @func_excanonical;
end

%% plot the figures
figure(12); clf;
subplot(2,3,1); 
plot(mean_time,mean_time*a_opt(1,1),'b',mean_time,...
     mean_time*a_fit(1,1),'r',mean_time,mean_hippos,'k.')
title('Hip Position')
xlabel('time (s)');

subplot(2,3,2); 
plot(mean_time,humanfunc(mean_time,a_opt(2,:)),'b',mean_time,...
    humanfunc(mean_time,a_fit(2,:)),'r',mean_time,mean_sknee,'k.')
title('Stance Knee')
xlabel('time (s)');


subplot(2,3,3); 
plot(mean_time,humanfunc(mean_time,a_opt(3,:)),'b',mean_time,...
    humanfunc(mean_time,a_fit(3,:)),'r',mean_time,mean_nsknee,'k.')
title('Non-Stance Knee')
xlabel('time (s)');

subplot(2,3,4); 
plot(mean_time,humanfunc(mean_time,a_opt(4,:)),'b',mean_time,...
    humanfunc(mean_time,a_fit(4,:)),'r',mean_time,mean_nsslope,'k.')
title('Non-Stance Slope')
xlabel('time (s)');

subplot(2,3,5); 
plot(mean_time,humanfunc(mean_time,a_opt(5,:)),'b',mean_time,...
    humanfunc(mean_time,a_fit(5,:)),'r',mean_time,mean_torso,'k.')
title('Torso')
xlabel('time (s)');
legend({'Desired','Fit','Data'});

subplot(2,3,6); 
plot(mean_time,humanfunc(mean_time,a_opt(6,:)),'b',mean_time,...
    humanfunc(mean_time,a_fit(6,:)),'r',mean_time,mean_nsfoot,'k.')
title('Non-Stance Foot Angle')
xlabel('time (s)');

end