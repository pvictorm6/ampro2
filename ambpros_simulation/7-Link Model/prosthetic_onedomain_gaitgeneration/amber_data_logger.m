classdef amber_data_logger < handle
    % Logs simulation results for amber_sim.
    
    properties
        tp;
        tp0;
        q;
        p;
        y_a;
        y_d;
        u;
        Pow;
    end
    
    methods
        % default constructor
        function obj = amber_data_logger()
        end
        
        % append the passed data to the log
        function append(this, tp, tp0, q, p, y_a, y_d, u, Pow)
            this.tp(:, end+1) = tp;
            this.tp0(:, end+1) = tp0;
            this.q(:, end+1) = q;
            this.p(:, end+1) = p;
            this.y_a(:, end+1) = y_a;
            this.y_d(:, end+1) = y_d;
            this.u(:, end+1) = u;
            this.Pow(:, end+1) = Pow;
        end
        
        % save the data to the specified file
        function saveLog(this, filename)
            datalog = this; %#ok<NASGU>
            save(['./logs/' filename], 'datalog');
        end
    end    
end
