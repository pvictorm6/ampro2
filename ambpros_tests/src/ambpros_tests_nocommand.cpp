#include "ros/ros.h"
#include "ambpros_common.hpp"
#include <ambpros_trajectory/ambpros_trajectory.hpp>
#include <ambpros_can_driver/control_configuration.h>
#include <ambpros_can_driver/ControlService.h>
#include <ambpros_can_driver/can_base.hpp>
#include "flexforce_adapter.h"

//#define TIME_MODE_DEBUG
#define MESSG_MODE_DEBUG

using namespace std;
#ifdef TIME_MODE_DEBUG
#include <time.h>
  int counter = 0;
#endif

int main(int argc, char **argv)
{
  ros::init(argc,argv,"ambpros_tests");
  ros::NodeHandle n;

  /* initialize three basic classes */
  AmbprosCommon  AmbprosMethods;
  AmbprosControl AmbprosController;
  TrajectoryGen AmbprosTraj;
  ambcap_pros imu(n);
  flexforce force_adapter(n, AmbprosController.frequency);

  // Setup server for running flag
  AmbprosMethods.setup_servs(n);

  /* start the can bus */
  can_base CanDriver(AmbprosMethods.can_rate); // 1000 Kbps
  CanDriver.open_can();

#ifdef MESSG_MODE_DEBUG
  ros::Publisher data_feedback = n.advertise<ambpros_can_driver::control_feedback>
      ("control_feedback",10);
  ambpros_can_driver::control_feedback feedback_msg;
#endif

#ifdef TIME_MODE_DEBUG
  struct timespec start_r, stop_r;
  double realtime;
#endif

  AmbprosMethods.print_status(AmbprosController, AmbprosTraj);

  ros::Rate loop_rate(AmbprosController.frequency);
  while(ros::ok()){
    #ifdef TIME_MODE_DEBUG
        if(clock_gettime(CLOCK_REALTIME,&start_r) == -1){
         cout << "clock realtime error!" << endl;
        }
    #endif

    ros::spinOnce();
    imu.spinOnce();
    force_adapter.spinOnce();

    if(AmbprosMethods.init_flag == 1){
      CanDriver.can_information(AmbprosMethods.number_drivers, AmbprosMethods.driver_ID);
      CanDriver.configure_mode(AmbprosMethods.control_type_flag);
//      CanDriver.get_feedback(AmbprosMethods.feedback_container, AmbprosMethods.control_type_flag);
      CanDriver.map_ambpro();
      CanDriver.get_feedback_pdo(AmbprosMethods.feedback_container);

      //set up the initial positions
      AmbprosMethods.init_pos(0) = AmbprosMethods.anglelimiter(AmbprosMethods.feedback_container[0]) * AmbprosMethods.PI_MATH / 180.0; //ankle
      AmbprosMethods.init_pos(1) = AmbprosMethods.anglelimiter(AmbprosMethods.feedback_container[3]) * AmbprosMethods.PI_MATH / 180.0; //knee
      cout << "Initial Position: Ankle-" << AmbprosMethods.init_pos(0) << ", Knee-" << AmbprosMethods.init_pos(1) << endl;

      if(AmbprosMethods.track_mode==0){
        AmbprosMethods.pos_ref(0) = AmbprosMethods.anglelimiter(AmbprosMethods.feedback_container[0]) * AmbprosMethods.PI_MATH / 180.0 - AmbprosMethods.init_pos(0); //ankle
        AmbprosMethods.pos_ref(1) = AmbprosMethods.anglelimiter(AmbprosMethods.feedback_container[3]) * AmbprosMethods.PI_MATH / 180.0 - AmbprosMethods.init_pos(1); //knee
      }
      AmbprosMethods.init_flag = 0;

//      // preparation time: 5s
//      AmbprosMethods.cushion_timer(5);
    }
    else{
        /* get the feedback signal */
//        CanDriver.get_feedback(AmbprosMethods.feedback_container, AmbprosMethods.control_type_flag);
        CanDriver.get_feedback_pdo(AmbprosMethods.feedback_container);
    }

    /* setup the feedback values */
    AmbprosMethods.feedback_setup();

    /* get the position reference using the TrajectoryGen class */
    Eigen::VectorXd imu_data = Eigen::VectorXd::Zero(8);
    if(!AmbprosTraj.isTimebased)
        AmbprosMethods.imudata_setup(imu_data, imu);

    /* get the contact forces from the force sensors */
    Eigen::VectorXd contactForces(2);
    if(AmbprosTraj.isForcesensor){
        contactForces << force_adapter.flexforce_msg.vector.x,
                         force_adapter.flexforce_msg.vector.y;
    }

    AmbprosTraj.calcReference(AmbprosMethods.pos_ref, AmbprosMethods.vel_ref,
                             AmbprosMethods.pos_feed, AmbprosMethods.vel_feed,
                             contactForces, imu_data, AmbprosController.frequency);

    /* compute the command torque on the joint side  AmbprosControl class */
    AmbprosController.calc_controller(AmbprosMethods.control_method, AmbprosMethods.control_signal,
                                      AmbprosMethods.pos_feed, AmbprosMethods.vel_feed, AmbprosMethods.tor_feed,
                                      AmbprosMethods.pos_ref, AmbprosMethods.vel_ref, AmbprosMethods.tor_ref,
                                      AmbprosMethods.number_drivers, AmbprosTraj.TimeParameter, AmbprosTraj.phaseIndex,
                                      AmbprosTraj.isHumanSwing, AmbprosTraj.pauseimpedance);

    // Only send motor command if running flag has been called by server!
    if ( AmbprosMethods.running ) {
        /* setup the command currents */
        AmbprosMethods.command_setup(AmbprosController);

        /* send the command to the motion controllers */
        AmbprosMethods.control_output[0] = 0;
        AmbprosMethods.control_output[1] = 0;
        CanDriver.set_references(AmbprosMethods.control_output, AmbprosMethods.control_type_flag);
    }
    else{
        AmbprosMethods.control_output[0] = 0;
        AmbprosMethods.control_output[1] = 0;
        CanDriver.set_references(AmbprosMethods.control_output, AmbprosMethods.control_type_flag);
    }

    /* publish the messages */
    #ifdef MESSG_MODE_DEBUG
        Eigen::VectorXd imped_torque(2);
        imped_torque << AmbprosController.imped_torque(0),
                          AmbprosController.imped_torque(1);
        AmbprosMethods.message_setup(feedback_msg, contactForces, imu_data, imped_torque,  AmbprosTraj.TimeParameter, AmbprosTraj.isHumanSwing);
        data_feedback.publish(feedback_msg);
    #endif


    loop_rate.sleep();

    #ifdef TIME_MODE_DEBUG
        if(clock_gettime(CLOCK_REALTIME,&stop_r) == -1){
         cout << "clock realtime error!" << endl;
        }else{
          realtime = (stop_r.tv_sec - start_r.tv_sec) + (double)(stop_r.tv_nsec - start_r.tv_nsec) / (double)1e9;
          std::cout<< "Time :"<<realtime<<std::endl;
        }
    #endif
  }

  AmbprosMethods.control_output[0] = 0;
  AmbprosMethods.control_output[1] = 0;
  CanDriver.set_references(AmbprosMethods.control_output, AmbprosMethods.control_type_flag);
  sleep(1);
  CanDriver.stop_devices();
  CanDriver.close_can();
  return 0;
}
