function h = quad_heightDecay(t,hmax,Ttau)
    a = -4*hmax/(Ttau^2);
	b = 4*hmax/Ttau;	
    h = (a*t.^2 + b*t);
%     h = (a*t.^2 + b*t)*2.25*exp(-5*t); 
end
