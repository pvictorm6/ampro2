function plot_estimatedImpedParams(impedparams, t, xdata, ydata, udata)
    njoints         = size(ydata, 2);    
    Y_estimated     = zeros(size(ydata));
    for i = 1:njoints
        Y_estimated(:, i) = func_impedance(impedparams(i, :),...
                        [xdata(:, i), xdata(:, i+njoints)]);
    end

    figure
    subplot(211)
    plot(t, Y_estimated(:,1), 'b')
    hold on
    plot(t, ydata(:, 1), 'k')
    hold on
    plot(t, udata(:, 1), 'r')
    title('ankle')
    subplot(212)
    plot(t, Y_estimated(:,2), 'b')
    hold on
    plot(t, ydata(:, 2), 'k')
    hold on
    plot(t, udata(:, 2), 'r')
    title('knee')
%     subplot(224)
%     plot(t, Y_estimated(:,4), 'b')
%     hold on
%     plot(t, ydata(:, 4), 'k')
%     hold on
%     plot(t, udata(:, 4), 'r')
%     title('non-stance ankle')
    legend('estimated', 'computed', 'actual')
end