// ************* Wen-Loong Ma ******************************
// ************* www.ma-wenloong.com ***********************
#include "geometry_reconstruction.hpp"
#include <iostream>

#define PI 3.1415926;

using namespace std;

geoReconstruction::geoReconstruction(){
	
}

void geoReconstruction::duidufun(double x[18], double r, double q[2], double qdot[2])
	{
		double phi, B, theta6, theta7, thetadot6;
		double DBeta, DPhi;
		phi = atan2( abs( duiduNUM(x,r) ) , abs( duiduDEN(x,r) ) );
		B   = Beta6(x,r);


		theta6  = B - phi;
		theta7  = theta7DS(x,r);
		
		if( abs(theta7) > 1 )
			theta7 = theta7 - PI;

		double	*newIC = new double[9];
		for(int i=0; i<7; i++)
			newIC[i] = x[i];
		newIC[7] = theta6;		
		newIC[8] = theta7;  
	//	jpos_mat( pos, newIC );

		if( abs( nst_height(newIC) ) > -0.00001 )
			theta6 = B + phi;

		DBeta 	  = DBeta6(x, r);
		DPhi      = DPhi6(x,  r);
		thetadot6 = DBeta + DPhi;
		
		q[0]	= theta6;
		q[1]	= theta7;
		qdot[0] = thetadot6;
		qdot[1] = thetadot7DS(x, r);

	}

// main function=====================================================
void geoReconstruction::geometry_reconstruction(double ic[9],
			  double xd[12],
			  double sf, 
			  double sfD
			 )
{
	//local variables
	const double lf = 889.0/5000.0;
	double	*vtoe = new double[2];
	double nst0, steplen;
	double px, pz, pxdot, pzdot;

	vstoe(xd, vtoe);
	pxdot = vtoe[0];
	pzdot = vtoe[1];

	nst0 = nst0_func( ic );
	steplen = abs( nst0 + lf * cos(ic[2]) );

	pz = lf * sin( sf );
	px = lf * cos( sf ) + steplen;

	double	*tmp = new double[18];
	tmp[0] = px;	tmp[1] = pz;	 tmp[2] = sf; 
	tmp[9] = pxdot; tmp[10]= pzdot;  tmp[11]= sfD; 
	for( int i=0; i<6; i++ )
	{	tmp[i+ 3] = xd[i];
		tmp[i+12] = xd[i+6];
	}

	double	*q    = new double[2],
			*qdot = new double[2];
	duidufun( tmp, steplen, q, qdot );

	xd[4]  = q[0];		xd[5]  = q[1];
	xd[10] = qdot[0];	xd[11] = qdot[1];
}
