#!/bin/bash

label=walking
stamp=$(date '+%Y-%m-%d_%H-%M')
prefix="${label}_${stamp}"
bag="${prefix}.bag"
param="${prefix}.rosparam.yaml"

rosbag record -j -O $bag \
	/control_feedback \
	/control_time \
	# &
	
# rosparam dump $param &

# wait
