%%set up the configuration
tol     = 1e-24;
opts    = optimset('display', 'iter', 'tolfun', tol);


% IMPORTANT! The desired simulation should first be run to find correct
% impedance values and choose whether or not to use impedance at the ankle!
% this is because this file relies on some global variables that are set
% depending on the simulation that is run prior to the poincare analysis!

% bad programming I know! :D



%%load parameters
load('a_opt');
load('x_opt');
% ic      = x_opt;
a       = a_opt;
%load('sol_y_after_40_3_phase_io_ankle_no_cons_LLS.mat')
% load('sol_y_after_80_3_phase_imp_ankle_no_cons_LLS.mat')
load('sol_y_after_80_3_phase_imp_ankle_no_cons_LLS_ep_20.mat')
ic = soly_end;

x_guess = ic([1:4,6:end]);

%%solve for the eigenvalues of the poincare map
[xp, temp1, temp2, temp3, DH] = fsolve(@(x) x-pacre2(x,a), x_guess, opts);
DP = eye(9)-DH;
eig(DP)

